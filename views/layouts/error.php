<?
	use yii\helpers\Html;
?>

<?php $this->beginPage() ?>

	<!DOCTYPE html>
	<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<?= Html::csrfMetaTags() ?>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>DragonFlute</title>
		<meta content="DragonFlute" name="description" />
		<meta content="DragonFlute" name="keywords" />
		<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">
		<link href="<?= Yii::getAlias('@web') ?>/css/main.css?<?= \app\controllers\MainController::VERSION ?>" rel="stylesheet" type="text/css" />
	</head>
	<body class="page page_error">
		<?php $this->beginBody(); ?>

		<main class="main-container" role="main">
			<?= $content ?>
		</main>

		<?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>