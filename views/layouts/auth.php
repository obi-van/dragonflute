<?
	use yii\helpers\Html;
	use app\components\KSeoHeaders;
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<?= Html::csrfMetaTags() ?>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title><?= KSeoHeaders::get('title') ?></title>
		<meta content="<?= KSeoHeaders::get('description') ?>" name="description" />
		<meta content="<?= KSeoHeaders::get('keywords') ?>" name="keywords" />
		<link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@web') ?>/imgs/apple-touch-icon.png" />
		<link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@web') ?>/imgs/favicon-32x32.png" />
		<link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@web') ?>/imgs/favicon-16x16.png" />
		<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">
		<link href="<?= Yii::getAlias('@web') ?>/css/main.css?<?= \app\controllers\MainController::VERSION ?>" rel="stylesheet" type="text/css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	</head>
	<body class="page-auth">
		<div class="page-auth__container">
			<?= $content ?>
		</div>
		<script src="<?= Yii::getAlias('@web') ?>/js/authApp.js?<?= \app\controllers\MainController::VERSION ?>"></script>
	</body>
</html>