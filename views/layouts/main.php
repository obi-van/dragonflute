<?php
	use yii\helpers\Html;
	use app\helpers\Debug;
	use app\helpers\KUseful;
	use app\components\KSeoHeaders;
	use yii\helpers\Url;
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="UTF-8">
		<?= Html::csrfMetaTags() ?>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title><?= KSeoHeaders::get('title') ?></title>
		<meta content="<?= KSeoHeaders::get('description') ?>" name="description" />
		<meta content="<?= KSeoHeaders::get('keywords') ?>" name="keywords" />
		<link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@web') ?>/imgs/apple-touch-icon.png" />
		<link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@web') ?>/imgs/favicon-32x32.png" />
		<link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@web') ?>/imgs/favicon-16x16.png" />

		<meta property="og:locale" content="ru_RU" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?= Url::canonical() ?>" />
		<meta property="og:title" content="<?= KSeoHeaders::get('title') ?>" />
		<meta property="og:description" content="<?= KSeoHeaders::get('description') ?>" />
		<meta property="og:image" content="<?= Url::to('/imgs/dragonflute.png', true) ?>" />

		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="Dragon Flute"/>
		<meta name="twitter:title" content="<?= KSeoHeaders::get('title') ?>">
		<meta name="twitter:description" content="<?= KSeoHeaders::get('description') ?>"/>
		<meta name="twitter:image:src" content="<?= Url::to('/imgs/dragonflute.png', true) ?>"/>
		<meta name="twitter:domain" content="<?= Url::canonical() ?>"/>
		<meta name="yandex-verification" content="183c7286cde442a4" />

		<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">
		<link href="<?= Yii::getAlias('@web') ?>/css/main.css?<?= \app\controllers\MainController::VERSION ?>" rel="stylesheet" type="text/css" />
		<link href="<?= Yii::getAlias('@web') ?>/css/dev.css?<?= \app\controllers\MainController::VERSION ?>" rel="stylesheet" type="text/css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script type="text/javascript">
			var serverDate = '<?= date("Y-m-d H:i:s"); ?>'
			var dateStart = '<?= date(KUseful::cnf('date_server_start')); ?>'
		</script>
	</head>
	<body class="index-page">
		<?php $this->beginBody(); ?>

			<?/* Yandex metrika */?>
			<?= KUseful::cnf('ya_metrika') ?>

			<header class="header-1">
				<div class="header-1__wrap">
					<nav id="nav" class="header-1__container">
						<div class="header-1__container-top">
							<div class="header-1__container-top-left-block">
								<a class="header-1__burger-menu js--burger-menu">
									<span class="header-1__burger-btn-line header-1__burger-btn-line--top"></span>
									<span class="header-1__burger-btn-line header-1__burger-btn-line--middle"></span>
									<span class="header-1__burger-btn-line header-1__burger-btn-line--bottom"></span>
								</a>
								<a href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>" class="header-1__logo">
									<img src="<?= Yii::getAlias('@web') ?>/imgs/logo.svg" alt="Dragon Flute">
								</a>
							</div>
							<?/* TO DO
								<div class="header-1__container-top-right-block">
									<div class="header-1__language">
										<a href="#" class="header-1__language-item active">RU</a>
										<span class="header-1__language-item-line"></span>
										<a href="#" class="header-1__language-item">EN</a>
									</div>
								</div>
							*/?>
						</div>

						<div id="navbar" class="header-1__container-bottom">
							<div class="header-1__menu">
								<ul class="header-1__menu-list">
									<li class="header-1__menu-item">
										<a class="header-1__menu-link" href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>about">О СЕРВЕРЕ</a>
									</li>
									<li class="header-1__menu-item">
										<a class="header-1__menu-link" href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>files">ФАЙЛЫ</a>
									</li>

									<? if(KUseful::cnf('registration') === 'true'): ?>
										<li class="header-1__menu-item">
											<a class="header-1__menu-link js--registration-btn" href="#">РЕГИСТРАЦИЯ</a>
											<ul class="header-1__sub-menu-list">
												<li class="header-1__sub-menu-item">
													<span class="header-1__sub-menu-item-bord"></span>
													<a class="header-1__sub-menu-link js--amnesia-btn" href="#">Смена пароля</a>
												</li>
											</ul>
										</li>
									<? endif ?>

									<li class="header-1__menu-item">
										<a class="header-1__menu-link" href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>forum">Форум</a>
									</li>
								</ul>
							</div>
							<div class="header-1__action-btn-container">
								<? if(Yii::$app->user->isGuest): ?>
									<a href="<?= Yii::$app->getUrlManager()->createUrl('/auth/login') ?>" class="header-1__login-link">
										<div class="header-1__login-action-name">Войти</div>
									</a>
								<? else: ?>
									<a href="<?= Yii::$app->getUrlManager()->createUrl('/auth/logout') ?>" class="header-1__login-link">
										<div class="header-1__login-user-name"><?= KUseful::getUserAccount(Yii::$app->user->id) ?></div>
										<div class="header-1__login-action-name">Выйти</div>
									</a>
								<? endif ?>
								<a href="<?= Yii::$app->getUrlManager()->createUrl('/files') ?>" class="header-1__play-btn">ИГРАТЬ</a>
							</div>

						</div>
					</nav>
				</div>

				<div class="header-1__left-panel js--left-panel">
					<div class="header-1__left-panel-wrap">
						<div class="header-1__left-panel-header">
							<a href="/" class="header-1__left-panel-header-cross js--left-panel-cross">
								<svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg>
							</a>
							<a href="/" class="header-1__left-panel-header-logo">
								<img src="<?= Yii::getAlias('@web') ?>/imgs/logo.svg" alt="Dragon Flute">
							</a>
						</div>
						<div class="header-1__left-panel-header-menu">
							<ul class="header-1__left-panel-list">
								<li class="header-1__left-panel-item">
									<a class="header-1__left-panel-link" href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>about">О сервере</a>
								</li>
								<li class="header-1__left-panel-item">
									<a class="header-1__left-panel-link" href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>files">Файлы</a>
								</li>
								<li class="header-1__left-panel-item">
									<? if(Yii::$app->user->isGuest): ?>
										<a class="header-1__left-panel-link" href="<?= Yii::$app->getUrlManager()->createUrl('/auth/login') ?>">
											<div class="header-1__left-panel-login-action-name">войти</div>
										</a>
									<? else: ?>
										<a class="header-1__left-panel-link" href="<?= Yii::$app->getUrlManager()->createUrl('/auth/logout') ?>">
											<div class="header-1__left-panel-login-action-name">выйти</div>
											<div class="header-1__left-panel-login-user-name"><?= KUseful::getUserAccount(Yii::$app->user->id) ?></div>
										</a>
									<? endif ?>
								</li>

								<? if(KUseful::cnf('registration') === 'true'): ?>
									<li class="header-1__left-panel-item">
										<a class="header-1__left-panel-link js--registration-btn" href="/#">Регистрация</a>
										<ul class="header-1__left-panel-sub-list">
											<li class="header-1__left-panel-sub-item">
												<a class="header-1__left-panel-sub-link js--amnesia-btn" href="/#">Смена пароля</a>
											</li>
										</ul>
									</li>
								<? endif ?>

								<li class="header-1__left-panel-item">
									<a class="header-1__left-panel-link" href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>forum">Форум</a>
								</li>

							</ul>
						</div>
					</div>
				</div>
			</header>

			<div class="content <?= app\widgets\BodyClass::widget() ?>">
				<div class="content__spacer-full-header"></div>
				<div class="content__wrap">
					<div class="content__main-logo">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/main_logo.svg" alt="Dragon Flute">
					</div>
				</div>

				<?= $content ?>

			</div>

			<footer class="footer-1">
				<div class="footer-1__logo">
					<img src="<?= Yii::getAlias('@web') ?>/imgs/main_logo.svg" alt="Dragon Flute">
				</div>
				<div class="footer-1__copy">
					<?= KUseful::cnf('footer_copy') ?>
				</div>
			</footer>

			<div style="display: none">
				<div id="registration-form" class="registration-form-1__block">
					<div class="registration-form-1__step-1">
						<h1>Регистрация аккаунта</h1>
						<fieldset class="form-group">
							<input type="text" name="rf_account" placeholder="Аккаунт" maxlength="60" id="rf_account" class="form-control" autocomplete="off">
							<div id="rf-error-account" class="registration-form-1__error hidden"></div>
						</fieldset>
						<fieldset class="form-group">
							<input type="email" id="rf_email" name="rf_email" placeholder="E-mail" class="form-control" autocomplete="off">
							<div id="rf-error-email" class="registration-form-1__error hidden"></div>
						</fieldset>
						<div class="registration-form-1__step-1-fieldset-group">
							<fieldset class="form-group">
								<input id="rf_password" name="rf_password" type="password" placeholder="Пароль" class="form-control" autocomplete="off">
								<div id="rf-error-password" class="registration-form-1__error hidden">Error Password</div>
							</fieldset>
							<fieldset class="form-group">
								<input id="rf_repassword" name="rf_repassword" type="password" placeholder="Повторите пароль" class="form-control" autocomplete="off">
								<div id="rf-error-repassword" class="registration-form-1__error hidden">Error repassword</div>
							</fieldset>
						</div>
						<div class="registration-form-1__step-1-registration-btn-wrap">
							<a href="#" class="registration-form-1__step-1-registration-btn js--reg-form-submit">
								<span class="js--reg-form-submit-text">Зарегистрироваться</span>
								<span class="js--reg-form-submit-loader hidden">
									<img src="<?= Yii::getAlias('@web') ?>/imgs/ajax_loader.svg" alt="">
								</span>
							</a>
						</div>
					</div>
					<div class="registration-form-1__step-2 hidden">
						<h1>Спасибо за регистрацию</h1>
						<div class="registration-form-1__step-2-description">
							Для <strong>входа</strong> в игру вы должны подтвердить аккаунт, перейдя по ссылке из письма, отправленного вам на почту. Если в течение 24 часов, с момента
							регистрации, вы не подтвердите аккаунт - он будет удален из системы.
						</div>
					</div>
				</div>
			</div>

			<div style="display: none">
				<div id="amnesia-form" class="amnesia-form-1__block">
					<div class="amnesia-form-1__step-1">
						<h1>Забыли или хотите<br/> изменить пароль?</h1>
						<div class="amnesia-form-1__description">
							Введите <strong>E-mail</strong>, указанный при регистрации аккаунта, для получения дальнейших инструкций
						</div>
						<fieldset class="form-group">
							<input type="email" id="af_email" name="af_email" placeholder="E-mail" class="form-control" autocomplete="off">
							<div id="af-error-email" class="amnesia-form-1__error hidden"></div>
						</fieldset>
						<div class="amnesia-form-1__step-1-amnesia-btn-wrap">
							<a href="#" class="amnesia-form-1__step-1-amnesia-btn js--amn-form-submit">
								<span class="js--amn-form-submit-text">Отправить</span>
								<span class="js--amn-form-submit-loader hidden">
									<img src="<?= Yii::getAlias('@web') ?>/imgs/ajax_loader.svg" alt="">
								</span>
							</a>
						</div>
					</div>

					<div class="amnesia-form-1__step-2 hidden">
						<div class="amnesia-form-1__step-2-description">
							На почту <span class="js-player-email"></span> было отправлено письмо с дальнейшими инструкциями.
						</div>
					</div>
				</div>
			</div>

			<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
			<script src="<?= Yii::getAlias('@web') ?>/js/jquery.colorbox-min.js"></script>
			<script src="<?= Yii::getAlias('@web') ?>/js/app.js?<?= \app\controllers\MainController::VERSION ?>"></script>

		<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>