<div class="error-page">
	<div class="error-page__block">
		<div class="error-page__number"><?= $code ?></div>
		<div class="error-page__text">Извините, но страница не найдена</div>
		<div class="error-page__note">Страница, которую вы ищете, не существует или URL-адрес неверен</div>
		<div class="error-page__btn">
			<a class="btn btn_back" href="<?= yii\helpers\BaseUrl::home() ?>">вернуться на главную страницу</a>
		</div>
	</div>
</div>