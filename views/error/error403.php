<div class="error-page">
	<div class="error-page__block">
		<div class="error-page__number"><?= $code ?></div>
		<div class="error-page__text">У вас недостаточно прав для данного раздела сайта.</div>
		<div class="error-page__note">Попробуйте <a class="btn btn_back" href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>/logout">выйти</a> и зайти под другой учётной записью.</div>
	</div>
</div>
