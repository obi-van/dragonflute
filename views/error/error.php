<?
	use yii\helpers\Html;
?>

<div class="error-page">
	<div class="error-page__block">
		<div class="error-page__text"><?= $code ?></div>
		<div class="error-page__note"><?= Html::decode($message); ?></div>
	</div>
</div>