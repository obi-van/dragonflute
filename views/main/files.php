<?php
	use app\helpers\KUseful;
?>

<section class="files-1">
	<div class="files-1__wrap">
		<? if(KUseful::cnf('registration') === 'true'): ?>
			<div class="files-1__registration-btn-wrap">
				<a href="/" class="files-1__registration-btn js--registration-btn">Регистрация аккаунта</a>
			</div>
		<? endif ?>
		<div class="files-1__title-container">
			<h1>Варианты загрузки</h1>
			<? if(!KUseful::cnf('registration')): ?>
				<div class="files-1__title-description">Файлы будут доступны к загрузке после запуска сервера</div>
			<? endif ?>
		</div>
		<div class="files-1__patch-variant-wrap">

			<a href="<?= KUseful::cnf('download_archive') ?>" target="_blank" class="files-1__client-download-link-full-client<? if(empty(KUseful::cnf('download_archive'))): ?> disabled<? endif ?>" target="_blank">
				<?/*
				<span class="files-1__client-download-link-icon-google-drive">
					<img src="<?= Yii::getAlias('@web') ?>/imgs/google_drive_icon.svg">
				</span>
				*/?>
				<span class="files-1__client-download-link-icon-torrent">
					<img src="<?= Yii::getAlias('@web') ?>/imgs/torrent-icon.svg">
				</span>
				<span class="files-1__client-download-link-text">Клиент</span>
				<span class="files-1__client-download-icon">
					<img src="<?= Yii::getAlias('@web') ?>/imgs/download_arrow.svg">
				</span>
			</a>
			<a href="<?= KUseful::cnf('download_client') ?>" class="files-1__client-download-link-client<? if(empty(KUseful::cnf('download_client'))): ?> disabled<? endif ?>" target="_blank">
				<?/*
					<span class="files-1__client-download-link-icon-torrent">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/torrent-icon.svg">
					</span>
					*/?>
				<span class="files-1__client-download-link-text">Клиент</span>
				<span class="files-1__client-download-icon">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/download_arrow.svg">
					</span>
			</a>

			<ul class="files-1__client-download-instruction">
				<li>Скачать <strong>клиент</strong> и <strong>патч</strong></li>
				<li>Запустить установку <strong>клиента</strong> игры</li>
				<li>Распаковать архив <strong>патча</strong> и скопировать содержимое в папку игры <strong>с заменой</strong></li>
				<li>Запустить игру файлом <strong>l2.exe</strong> в папке <strong>system</strong></li>
			</ul>
		</div>

		<div class="files-1__full-variant-wrap">
			<a href="<?= KUseful::cnf('download_patch') ?>" class="files-1__client-download-link-patch<? if(empty(KUseful::cnf('download_patch'))): ?> disabled<? endif ?>" target="_blank">
				<span class="files-1__client-download-link-text">Патч</span>
				<span class="files-1__client-download-icon">
					<img src="<?= Yii::getAlias('@web') ?>/imgs/download_arrow.svg">
				</span>
			</a>
			<div class="files-1__client-download-instruction-full-client">
				<?= \app\helpers\KUseful::textblock('df_files_patch') ?>
			</div>
		</div>
	</div>
</section>