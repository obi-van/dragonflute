<section class="index-1">
	<div class="index-1__wrap">
		<div class="index-1__info">
			<div id="index-slider" class="index-1__swiper-container swiper-container">
				<div class="index-1__swiper-wrapper swiper-wrapper">
					<? if(!empty($events)): ?>
						<? foreach($events as $event): ?>
							<div class="index-1__swiper-slide swiper-slide">
								<? if(!empty($event->link)): ?>
									<a href="" class="index-1__swiper-slide-text">
										<span class="index-1__swiper-slide-title"><?= $event->title ?></span>
										<span class="index-1__swiper-slide-description"><?= !empty($event->description) ? $event->description : '' ?></span>
									</a>
								<? else: ?>
									<span class="index-1__swiper-slide-text">
										<span class="index-1__swiper-slide-title"><?= $event->title ?></span>
										<span class="index-1__swiper-slide-description"><?= !empty($event->description) ? $event->description : '' ?></span>
									</span>
								<? endif ?>

								<?
									$imgs = json_decode($event->img);
									$img = isset($imgs->thumb_600x644) ? $imgs->thumb_600x644 : '';
								?>

								<? if($img): ?>
									<img src="<?= \app\helpers\KUseful::imgpatch($img) ?>" alt="<?= !empty($event->title) ? $event->title : '' ?>">
								<? else: ?>
									<img src="<?= Yii::getAlias('@web') ?>/imgs/default_big_slider_img.png" alt="<?= !empty($event->title) ? $event->title : '' ?>">
								<? endif ?>
							</div>
						<? endforeach ?>
					<? else: ?>
						<div class="index-1__swiper-slide swiper-slide">
							<span class="index-1__swiper-slide-text">
								<span class="index-1__swiper-slide-title">Dragon Flute</span>
								<span class="index-1__swiper-slide-description"></span>
							</span>
							<img src="<?= Yii::getAlias('@web') ?>/imgs/default_big_slider_img.png" alt="<?= !empty($event->title) ? $event->title : '' ?>">
						</div>
					<? endif ?>
				</div>
				<? if(count($events) > 1): ?>
					<div class="index-1__swiper-pagination swiper-pagination"></div>
					<div class="index-1__swiper-button-next swiper-button-next">
						<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="268.832px" height="268.832px" viewBox="0 0 268.832 268.832" style="enable-background:new 0 0 268.832 268.832;"
							 xml:space="preserve">
								<g>
									<path d="M265.171,125.577l-80-80c-4.881-4.881-12.797-4.881-17.678,0c-4.882,4.882-4.882,12.796,0,17.678l58.661,58.661H12.5
										c-6.903,0-12.5,5.597-12.5,12.5c0,6.902,5.597,12.5,12.5,12.5h213.654l-58.659,58.661c-4.882,4.882-4.882,12.796,0,17.678
										c2.44,2.439,5.64,3.661,8.839,3.661s6.398-1.222,8.839-3.661l79.998-80C270.053,138.373,270.053,130.459,265.171,125.577z"/>
								</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
								</svg>
					</div>
					<div class="index-1__swiper-button-prev swiper-button-prev">
						<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="268.832px" height="268.832px" viewBox="0 0 268.832 268.832" style="enable-background:new 0 0 268.832 268.832;"
							 xml:space="preserve">
								<g>
									<path d="M265.171,125.577l-80-80c-4.881-4.881-12.797-4.881-17.678,0c-4.882,4.882-4.882,12.796,0,17.678l58.661,58.661H12.5
										c-6.903,0-12.5,5.597-12.5,12.5c0,6.902,5.597,12.5,12.5,12.5h213.654l-58.659,58.661c-4.882,4.882-4.882,12.796,0,17.678
										c2.44,2.439,5.64,3.661,8.839,3.661s6.398-1.222,8.839-3.661l79.998-80C270.053,138.373,270.053,130.459,265.171,125.577z"/>
								</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
							<g>
							</g>
								</svg>
					</div>
				<? endif ?>
			</div>
			<div class="index-1__info-statistics">
				<div class="index-1__info-statistics-counters">
					<div class="index-1__rates-wrap">
						<div class="index-1__rates-top-section">
							<div class="index-1__rates-title">Hard Five PTS</div>
							<div class="index-1__rates-subtitle">beta testing</div>
							<div class="index-1__rates-container">
								<div class="index-1__rates-text">Rates:</div>
								<div class="index-1__rates-param">HARD</div>
							</div>
						</div>

						<div class="index-1__rates-icons-container">
							<div class="index-1__rates-icon js--not-shit" data-id="shit">
								<img src="<?= Yii::getAlias('@web') ?>/imgs/not_shit.png" alt="No shit">
							</div>
							<div class="index-1__rates-icon js--not-shit" data-id="money">
								<img src="<?= Yii::getAlias('@web') ?>/imgs/not_money.png" alt="No donates">
							</div>
							<div class="index-1__rates-icon js--not-shit" data-id="automatization">
								<img src="<?= Yii::getAlias('@web') ?>/imgs/not_automatization.png" alt="No automatization">
							</div>
							<div class="index-1__rates-icon js--not-shit" data-id="windows">
								<img src="<?= Yii::getAlias('@web') ?>/imgs/not_windows.png" alt="No nyashnosti">
							</div>
						</div>

						<?= app\widgets\ServerWorkingTimeTemplate::widget() ?>

					</div>

					<?= app\widgets\ServerStatusTemplate::widget() ?>

				</div>
				<div class="index-1__info-statistics-slider">
					<? if(!empty($bosses)): ?>
						<div id="rb-slider" class="index-1__boss-statistics-swiper swiper-container">
							<div class="index-1__boss-swiper-wrapper swiper-wrapper">
								<? foreach ($bosses as $boss): ?>
									<div class="index-1__boss-statistics-slide swiper-slide">

										<?
											$imgs = json_decode($boss->img);
											$img = isset($imgs->thumb_300x300) ? $imgs->thumb_300x300 : '';
										?>

										<? if($img): ?>
											<img src="<?= \app\helpers\KUseful::imgpatch($img) ?>" alt="<?= $boss->name ?>">
										<? else: ?>
											<img src="<?= Yii::getAlias('@web') ?>/imgs/default_boss_slider_img.png" alt="<?= $boss->name ?>">
										<? endif ?>

										<div class="index-1__boss-statistics-slide-info">
											<div class="index-1__boss-statistics-slide-info-rb-name"><?= $boss->name ?></div>
											<div class="index-1__boss-statistics-slide-info-rb-count"><?= $boss->deaths ?></div>
										</div>
									</div>
								<? endforeach ?>
							</div>
							<div class="index-1__boss-statistics-slider-pagination swiper-pagination"></div>
						</div>

						<div class="index-1__boss-statistics-slider-btn-next boss-swiper-button-next">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 490.787 490.787" style="enable-background:new 0 0 490.787 490.787;" xml:space="preserve">
											<path d="M362.671,490.787c-2.831,0.005-5.548-1.115-7.552-3.115L120.452,253.006
												c-4.164-4.165-4.164-10.917,0-15.083L355.119,3.256c4.093-4.237,10.845-4.354,15.083-0.262c4.237,4.093,4.354,10.845,0.262,15.083
												c-0.086,0.089-0.173,0.176-0.262,0.262L143.087,245.454l227.136,227.115c4.171,4.16,4.179,10.914,0.019,15.085
												C368.236,489.664,365.511,490.792,362.671,490.787z"/>
								<path d="M362.671,490.787c-2.831,0.005-5.548-1.115-7.552-3.115L120.452,253.006c-4.164-4.165-4.164-10.917,0-15.083L355.119,3.256
												c4.093-4.237,10.845-4.354,15.083-0.262c4.237,4.093,4.354,10.845,0.262,15.083c-0.086,0.089-0.173,0.176-0.262,0.262
												L143.087,245.454l227.136,227.115c4.171,4.16,4.179,10.914,0.019,15.085C368.236,489.664,365.511,490.792,362.671,490.787z"/>

											</svg>
						</div>
						<div class="index-1__boss-statistics-slider-btn-prev boss-swiper-button-prev">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 490.787 490.787" style="enable-background:new 0 0 490.787 490.787;" xml:space="preserve">
											<path d="M362.671,490.787c-2.831,0.005-5.548-1.115-7.552-3.115L120.452,253.006
												c-4.164-4.165-4.164-10.917,0-15.083L355.119,3.256c4.093-4.237,10.845-4.354,15.083-0.262c4.237,4.093,4.354,10.845,0.262,15.083
												c-0.086,0.089-0.173,0.176-0.262,0.262L143.087,245.454l227.136,227.115c4.171,4.16,4.179,10.914,0.019,15.085
												C368.236,489.664,365.511,490.792,362.671,490.787z"/>
								<path d="M362.671,490.787c-2.831,0.005-5.548-1.115-7.552-3.115L120.452,253.006c-4.164-4.165-4.164-10.917,0-15.083L355.119,3.256
												c4.093-4.237,10.845-4.354,15.083-0.262c4.237,4.093,4.354,10.845,0.262,15.083c-0.086,0.089-0.173,0.176-0.262,0.262
												L143.087,245.454l227.136,227.115c4.171,4.16,4.179,10.914,0.019,15.085C368.236,489.664,365.511,490.792,362.671,490.787z"/>

											</svg>
						</div>
					<? endif ?>
				</div>
				<div class="index-1__info-stocials">
					<? if(!empty(app\helpers\KUseful::cnf('vk_link'))): ?>
						<a href="<?= app\helpers\KUseful::cnf('vk_link') ?>" target="_blank" class="index-1__info-stocial-item index-1__info-stocial-vk">
							<svg id="Bold" enable-background="new 0 0 24 24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m19.915 13.028c-.388-.49-.277-.708 0-1.146.005-.005 3.208-4.431 3.538-5.932l.002-.001c.164-.547 0-.949-.793-.949h-2.624c-.668 0-.976.345-1.141.731 0 0-1.336 3.198-3.226 5.271-.61.599-.892.791-1.225.791-.164 0-.419-.192-.419-.739v-5.105c0-.656-.187-.949-.74-.949h-4.126c-.419 0-.668.306-.668.591 0 .622.945.765 1.043 2.515v3.797c0 .832-.151.985-.486.985-.892 0-3.057-3.211-4.34-6.886-.259-.713-.512-1.001-1.185-1.001h-2.625c-.749 0-.9.345-.9.731 0 .682.892 4.073 4.148 8.553 2.17 3.058 5.226 4.715 8.006 4.715 1.671 0 1.875-.368 1.875-1.001 0-2.922-.151-3.198.686-3.198.388 0 1.056.192 2.616 1.667 1.783 1.749 2.076 2.532 3.074 2.532h2.624c.748 0 1.127-.368.909-1.094-.499-1.527-3.871-4.668-4.023-4.878z"/></svg>
						</a>
					<? else: ?>
						<span class="index-1__info-stocial-item index-1__info-stocial-vk no-pointer">
							<svg id="Bold" enable-background="new 0 0 24 24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m19.915 13.028c-.388-.49-.277-.708 0-1.146.005-.005 3.208-4.431 3.538-5.932l.002-.001c.164-.547 0-.949-.793-.949h-2.624c-.668 0-.976.345-1.141.731 0 0-1.336 3.198-3.226 5.271-.61.599-.892.791-1.225.791-.164 0-.419-.192-.419-.739v-5.105c0-.656-.187-.949-.74-.949h-4.126c-.419 0-.668.306-.668.591 0 .622.945.765 1.043 2.515v3.797c0 .832-.151.985-.486.985-.892 0-3.057-3.211-4.34-6.886-.259-.713-.512-1.001-1.185-1.001h-2.625c-.749 0-.9.345-.9.731 0 .682.892 4.073 4.148 8.553 2.17 3.058 5.226 4.715 8.006 4.715 1.671 0 1.875-.368 1.875-1.001 0-2.922-.151-3.198.686-3.198.388 0 1.056.192 2.616 1.667 1.783 1.749 2.076 2.532 3.074 2.532h2.624c.748 0 1.127-.368.909-1.094-.499-1.527-3.871-4.668-4.023-4.878z"/></svg>
						</span>
					<? endif ?>

					<? if(!empty(app\helpers\KUseful::cnf('facebook_link'))): ?>
						<a href="<?= app\helpers\KUseful::cnf('facebook_link') ?>" target="_blank" class="index-1__info-stocial-item index-1__info-stocial-facebook">
							<svg id="Bold" enable-background="new 0 0 24 24"  viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m15.997 3.985h2.191v-3.816c-.378-.052-1.678-.169-3.192-.169-3.159 0-5.323 1.987-5.323 5.639v3.361h-3.486v4.266h3.486v10.734h4.274v-10.733h3.345l.531-4.266h-3.877v-2.939c.001-1.233.333-2.077 2.051-2.077z"/></svg>
						</a>
					<? else: ?>
						<span class="index-1__info-stocial-item index-1__info-stocial-facebook no-pointer">
							<svg id="Bold" enable-background="new 0 0 24 24"  viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m15.997 3.985h2.191v-3.816c-.378-.052-1.678-.169-3.192-.169-3.159 0-5.323 1.987-5.323 5.639v3.361h-3.486v4.266h3.486v10.734h4.274v-10.733h3.345l.531-4.266h-3.877v-2.939c.001-1.233.333-2.077 2.051-2.077z"/></svg>
						</span>
					<? endif ?>

					<? if(!empty(app\helpers\KUseful::cnf('twich_link'))): ?>
						<a href="<?= app\helpers\KUseful::cnf('twich_link') ?>" target="_blank" class="index-1__info-stocial-item index-1__info-stocial-twich">
							<svg id="Bold" enable-background="new 0 0 24 24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m.975 4.175v16.694h5.749v3.131h3.139l3.134-3.132h4.705l6.274-6.258v-14.61h-21.434zm3.658-2.09h17.252v11.479l-3.66 3.652h-5.751l-3.134 3.127v-3.127h-4.707z"/><path d="m10.385 6.262h2.09v6.26h-2.09z"/><path d="m16.133 6.262h2.091v6.26h-2.091z"/></svg>
						</a>
					<? else: ?>
						<span class="index-1__info-stocial-item index-1__info-stocial-twich no-pointer">
							<svg id="Bold" enable-background="new 0 0 24 24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m.975 4.175v16.694h5.749v3.131h3.139l3.134-3.132h4.705l6.274-6.258v-14.61h-21.434zm3.658-2.09h17.252v11.479l-3.66 3.652h-5.751l-3.134 3.127v-3.127h-4.707z"/><path d="m10.385 6.262h2.09v6.26h-2.09z"/><path d="m16.133 6.262h2.091v6.26h-2.091z"/></svg>
						</span>
					<? endif ?>

					<? if(!empty(app\helpers\KUseful::cnf('youtube_link'))): ?>
						<a href="<?= app\helpers\KUseful::cnf('youtube_link') ?>" target="_blank" class="index-1__info-stocial-item index-1__info-stocial-youtube">
							<svg viewBox="0 -77 512.00213 512" xmlns="http://www.w3.org/2000/svg"><path d="m501.453125 56.09375c-5.902344-21.933594-23.195313-39.222656-45.125-45.128906-40.066406-10.964844-200.332031-10.964844-200.332031-10.964844s-160.261719 0-200.328125 10.546875c-21.507813 5.902344-39.222657 23.617187-45.125 45.546875-10.542969 40.0625-10.542969 123.148438-10.542969 123.148438s0 83.503906 10.542969 123.148437c5.90625 21.929687 23.195312 39.222656 45.128906 45.128906 40.484375 10.964844 200.328125 10.964844 200.328125 10.964844s160.261719 0 200.328125-10.546875c21.933594-5.902344 39.222656-23.195312 45.128906-45.125 10.542969-40.066406 10.542969-123.148438 10.542969-123.148438s.421875-83.507812-10.546875-123.570312zm0 0" fill="#fff"/><path d="m204.96875 256 133.269531-76.757812-133.269531-76.757813zm0 0" fill="#f00"/></svg>
						</a>
					<? else: ?>
						<span class="index-1__info-stocial-item index-1__info-stocial-youtube no-pointer">
							<svg viewBox="0 -77 512.00213 512" xmlns="http://www.w3.org/2000/svg"><path d="m501.453125 56.09375c-5.902344-21.933594-23.195313-39.222656-45.125-45.128906-40.066406-10.964844-200.332031-10.964844-200.332031-10.964844s-160.261719 0-200.328125 10.546875c-21.507813 5.902344-39.222657 23.617187-45.125 45.546875-10.542969 40.0625-10.542969 123.148438-10.542969 123.148438s0 83.503906 10.542969 123.148437c5.90625 21.929687 23.195312 39.222656 45.128906 45.128906 40.484375 10.964844 200.328125 10.964844 200.328125 10.964844s160.261719 0 200.328125-10.546875c21.933594-5.902344 39.222656-23.195312 45.128906-45.125 10.542969-40.066406 10.542969-123.148438 10.542969-123.148438s.421875-83.507812-10.546875-123.570312zm0 0" fill="#fff"/><path d="m204.96875 256 133.269531-76.757812-133.269531-76.757813zm0 0" fill="#f00"/></svg>
						</span>
					<? endif ?>
				</div>
			</div>
		</div>

		<? if(!empty($topNews)): ?>
			<div class="index-1__top-news">
				<? if(!empty($topNews)): ?>
					<? foreach ($topNews as $news): ?>
						<div class="index-1__top-news-item-wrap">
							<a href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>news/<?= $news->alias ?>" class="index-1__top-news-item">
								<div class="index-1__top-news-img">
									<?
										$imgs = json_decode($news->img);
										$img = isset($imgs->thumb_368x196) ? $imgs->thumb_368x196 : '';
									?>
									<img src="<?= \app\helpers\KUseful::imgpatch($img, Yii::getAlias('@web').'/imgs/default_news.jpg') ?>" alt="<?= $news->short_title ?>">
								</div>
								<div class="index-1__top-news-title"><?= $news->short_title ?></div>
								<div class="index-1__top-news-item-date-block-wrap">
									<div class="index-1__top-news-item-date-block">
										<div class="index-1__top-news-item-date">
											<?= \app\helpers\KUseful::date_for_human($news->date) ?>
										</div>
										<div class="index-1__top-news-item-arrow">
											<svg width="24" height="17" viewBox="0 0 24 17" fill="none" xmlns="http://www.w3.org/2000/svg">
												<path d="M23.6731 7.68775L16.5311 0.336394C16.0954 -0.112131 15.3887 -0.112131 14.9529 0.336394C14.5171 0.78501 14.5171 1.51224 14.9529 1.96086L20.1899 7.35134H1.11594C0.499672 7.35134 0 7.86566 0 8.49999C0 9.13423 0.499672 9.64864 1.11594 9.64864H20.1899L14.9531 15.0391C14.5173 15.4877 14.5173 16.215 14.9531 16.6636C15.1709 16.8877 15.4566 17 15.7422 17C16.0278 17 16.3134 16.8877 16.5313 16.6636L23.6731 9.31222C24.109 8.86361 24.109 8.13637 23.6731 7.68775Z" fill="#16666F"/>
											</svg>
										</div>
									</div>
								</div>
							</a>
						</div>
					<? endforeach ?>
				<? endif ?>
			</div>
		<? endif ?>

		<? if(!empty($allNews)): ?>
			<div class="index-1__news">

				<? if(!empty($allNews)): ?>
					<? foreach ($allNews as $news): ?>
						<div class="index-1__news-item-wrap">
							<a href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>news/<?= $news->alias ?>" class="index-1__news-item">
								<div class="index-1__news-item-title"><?= $news->short_title ?></div>
								<div class="index-1__news-item-image">
									<?
										$imgs = json_decode($news->img);
										$img = isset($imgs->thumb_368x196) ? $imgs->thumb_368x196 : '';
									?>
									<img src="<?= \app\helpers\KUseful::imgpatch($img, Yii::getAlias('@web').'/imgs/default_news.jpg') ?>" alt="<?= $news->short_title ?>">
								</div>
								<div class="index-1__news-item-content">
									<div class="index-1__news-item-description">
										<?= $news->short_description ?>
									</div>
									<div class="index-1__news-item-date-block-wrap">
										<div class="index-1__news-item-date-block">
											<div class="index-1__news-item-date">
												<?= \app\helpers\KUseful::date_for_human($news->date) ?>
											</div>
											<div class="index-1__news-item-arrow">
												<svg width="24" height="17" viewBox="0 0 24 17" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M23.6731 7.68775L16.5311 0.336394C16.0954 -0.112131 15.3887 -0.112131 14.9529 0.336394C14.5171 0.78501 14.5171 1.51224 14.9529 1.96086L20.1899 7.35134H1.11594C0.499672 7.35134 0 7.86566 0 8.49999C0 9.13423 0.499672 9.64864 1.11594 9.64864H20.1899L14.9531 15.0391C14.5173 15.4877 14.5173 16.215 14.9531 16.6636C15.1709 16.8877 15.4566 17 15.7422 17C16.0278 17 16.3134 16.8877 16.5313 16.6636L23.6731 9.31222C24.109 8.86361 24.109 8.13637 23.6731 7.68775Z" fill="#16666F"/>
												</svg>
											</div>
										</div>
									</div>
								</div>
							</a>
						</div>
					<? endforeach ?>
				<? endif ?>

			</div>

			<?= yii\widgets\LinkPager::widget([
				'pagination' => $pages,
				'options' => [
					'class' => 'pager-wrapper',
					'id' => 'pager-container',
				],
				'linkOptions' => ['class' => 'link'],
				'activePageCssClass' => 'active',
				'disabledPageCssClass' => 'disable',

				'prevPageCssClass' => 'mypre',
				'nextPageCssClass' => 'mynext',
				'firstPageCssClass' => 'myfirst',
				'lastPageCssClass' => 'mylast',
				'maxButtonCount' => 3,
			]); ?>

		<? endif ?>
	</div>

	<div style="display: none">
		<div id="rates-icons-info">
			<div id="shit">
				Клиент без багов, работает как часы
			</div>
			<div id="money">
				Донат не дает игрового преимущества
			</div>
			<div id="automatization">
				Нет ботам и автоматизации
			</div>
			<div id="windows">
				Максимум 3 окна
			</div>
		</div>
	</div>
</section>