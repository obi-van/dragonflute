<section class="news-1">
	<div class="news-1__wrap">
		<div class="news-1__date">
			<?= \app\helpers\KUseful::date_for_human($newsDetail->date) ?> г.
		</div>
		<div class="news-1__content">
			<h1><?= $newsDetail->title ?></h1>

			<?= $newsDetail->content ?>

		</div>
	</div>
</section>
