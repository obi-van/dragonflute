<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
?>

<div class="page-auth__activation-page-wrap">
	<div class="page-auth__activation-page-logo">
		<img src="<?= Yii::getAlias('@web') ?>/imgs/logo_black.svg">
	</div>
	<div class="page-auth__activation-page-content">
		<div class="page-auth__form-wrap-step-1">
			<div class="page-auth__activation-page-title">Смена / восстановления пароля</div>

			<input type="hidden" name="af_hash-code" value="<?= $code ?>">

			<fieldset class="form-group">
				<input id="af_password" name="af_password" type="password" placeholder="Новый пароль" class="form-control">
				<div id="af-error-password" class="page-auth__error hidden"></div>
			</fieldset>
			<fieldset class="form-group">
				<input id="af_repassword" name="af_repassword" type="password" placeholder="Повторите пароль" class="form-control">
				<div id="af-error-repassword" class="page-auth__error hidden"></div>
			</fieldset>

			<div class="page-auth__change-pass-btn-wrap">
				<a href="#" class="page-auth__change-pass--btn js--change-pass-form-submit">
					<span class="js--change-pass-form-submit-text">Отправить</span>
					<span class="js--reg-form-submit-loader hidden">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/ajax_loader.svg" alt="">
					</span>
				</a>
			</div>
		</div>

		<div class="page-auth__form-wrap-step-2 hidden">
			<div class="page-auth__activation-page-content">
				<div class="page-auth__activation-page-title">Вы успешно изменили пароль</div>
				<div class="page-auth__activation-page-description">Вы можете использовать данные для входа в игру. Если у вас
					появились вопросы - вы можете связаться с администратором <a href="mailto:l2dragonflute@gmail.com">l2dragonflute@gmail.com</a></div>
			</div>
			<div class="page-auth__activation-page-back-btn-wrap">
				<a href="<?= yii\helpers\BaseUrl::home() ?>" class="page-auth__activation-page-back-btn">Вернуться на сайт</a>
			</div>
		</div>

	</div>
</div>