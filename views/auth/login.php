<?
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;

	$this->title = 'Авторизация';
?>

<div class="page-auth__wrap">
	<h1 class="page-auth__title"><?= Html::encode($this->title) ?></h1>

	<div class="page-auth__info">
		<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'page-auth__login-form']]); ?>
			<?= $form->field($model, 'email', ['options' => ['class' => 'page-auth__form-group']])->input('text', ['placeholder' => "E-mail"])->label(false) ?>
			<?= $form->field($model, 'password', ['options' => ['class' => 'page-auth__form-group']])->passwordInput(['placeholder' => "Пароль"])->label(false) ?>
			<?= $form->field($model, 'rememberMe',[
					'template' => '<div class="checkbox">{input}{label}</div><div class=\"col-lg-8\">{error}</div>'
                ],
				[
					'options' => ['class' => 'page-auth__form-group']
				])->checkbox([],false)->label('Запомнить меня') ?>
			<div class="page-auth__form-group page-auth__btn">
				<?= Html::submitButton('Войти', ['class' => 'btn page-auth__btn', 'name' => 'login-button']) ?>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>