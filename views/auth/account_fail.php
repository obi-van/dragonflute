<div class="page-auth__activation-page-wrap">
	<div class="page-auth__activation-page-logo">
		<img src="<?= Yii::getAlias('@web') ?>/imgs/logo_black.svg">
	</div>
	<div class="page-auth__activation-page-content">
		<div class="page-auth__activation-page-title">Что-то пошло не так, или срок действия вашей ссылки истёк.</div>
		<div class="page-auth__activation-page-description">Вы можете связаться с администратором <a href="mailto:l2dragonflute@gmail.com">l2dragonflute@gmail.com</a>
			или попробовать повторить процедуру позже.</div>
	</div>
	<div class="page-auth__activation-page-back-btn-wrap">
		<a href="<?= yii\helpers\BaseUrl::home() ?>" class="page-auth__activation-page-back-btn">Вернуться на сайт</a>
	</div>
</div>