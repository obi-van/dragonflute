<div class="page-auth__activation-page-wrap">
	<div class="page-auth__activation-page-logo">
		<img src="<?= Yii::getAlias('@web') ?>/imgs/logo_black.svg">
	</div>
	<div class="page-auth__activation-page-content">
		<div class="page-auth__activation-page-title">Вы успешно подтвердили аккаунт</div>
		<div class="page-auth__activation-page-description">Вы можете использовать данные для входа в игру. Если у вас
			появились вопросы - вы можете связаться с администратором <a href="mailto:l2dragonflute@gmail.com">l2dragonflute@gmail.com</a></div>
	</div>
	<div class="page-auth__activation-page-back-btn-wrap">
		<a href="<?= yii\helpers\BaseUrl::home() ?>" class="page-auth__activation-page-back-btn">Вернуться на сайт</a>
	</div>
</div>