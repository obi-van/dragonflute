<?php
namespace app\widgets;

use yii\base\Widget;
use app\helpers\Debug;

class ForumBreadcrumbs extends Widget
{
	public $params;

	public function init()
	{
		parent::init();
	}

	public function run()
	{
		$data = [
			'params' => $this->params
		];

		return $this->render('forum-breadcrumbs', $data);
	}
}
