<?php
namespace app\widgets;

use yii\base\Widget;
use app\helpers\Debug;
use Yii;

class CustomForumPagination extends Widget
{
	public $pagination;
	public $maxButtonCount;
	public $url;

	public function init()
	{
		parent::init();
	}

	public function run()
	{
		$pagination = $this->pagination;
		$maxButtonCount = $this->maxButtonCount;
		$url = $this->url;

		$data = [
			'pagination' => $pagination,
			'maxButtonCount' => $maxButtonCount,
			'url' => $url
		];

		return $this->render('forum-pagination', $data);
	}
}
