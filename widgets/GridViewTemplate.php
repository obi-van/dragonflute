<?php

namespace app\widgets;

use yii\base\Widget;

class GridViewTemplate extends Widget
{
	public $params = [
		'removing' => true,
		'editing'  => true,
	];
	public $type = 'default';

	public function init()
	{
		parent::init();

	}

	public function run()
	{
		if ($this->type == 'settings')
			return $this->render('grid-settings', ['params' => $this->params]);
		else
			return $this->render('grid-view', ['params' => $this->params]);
	}
}
