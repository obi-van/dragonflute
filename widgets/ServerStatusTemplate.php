<?php
namespace app\widgets;
use app\components\DragonApi;
use app\helpers\KUseful;
use yii\base\Widget;
use app\helpers\Debug;

class ServerStatusTemplate extends Widget
{
	public function init()
	{
		parent::init();
	}

	public function run()
	{

		$countPlayers = 0;
		$countSellers = 0;
		$response = NULL;
		$ip = KUseful::cnf('status_server_ip');
		$port = KUseful::cnf('status_server_port');
		$response = @fsockopen($ip, $port, $errno, $errstr, 1);

		//если сервер online - узнаем загрузку пользователями
		if(isset($response)) {
			$api = new DragonApi();
			$allPlayers = json_decode($api->getCountPlayers());
			$countPlayers = $allPlayers->online ? $allPlayers->online : 0;
			$countSellers = $allPlayers->sellers ? $allPlayers->sellers : 0;
		}

		$data = [
			'response'      => $response,
			'countPlayers'  => $countPlayers,
			'countSellers'  => $countSellers,
		];

		return $this->render('server-status-template', $data);
	}
}