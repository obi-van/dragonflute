<?php
namespace app\widgets;
use Yii;
use yii\base\Widget;
use app\helpers\Debug;
use app\helpers\KUseful;

class ServerWorkingTimeTemplate extends Widget
{
	public function init()
	{
		parent::init();
	}

	public function run()
	{
		$dateStart = date(KUseful::cnf('date_server_start'));
		$currentDate = date("Y-m-d H:i:s");

		$diff = abs(strtotime($currentDate) - strtotime($dateStart));
		$years   = floor($diff / (365*60*60*24));
		$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		//для вывода на фронт <span>728</span> дней <span>26</span> часов <span>54</span> минуты
		$daysOnly = floor($diff / (3600 * 24));
		$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

		$data = [
			'daysOnly'  => $daysOnly,
			'hours'     => $hours,
			'minuts'    => $minuts,
			'dateStart' => $dateStart,
			'diff'      => $diff,
			'currentDate' => $currentDate,
		];

		return $this->render('server-working-time-template', $data);
	}
}