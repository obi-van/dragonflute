<?php
namespace app\widgets;

use yii\base\Widget;
use app\helpers\Debug;
use yii\helpers\Url;
use Yii;

class BodyClass extends Widget
{
	public function init()
	{
		parent::init();
	}

	public function run()
	{
		$action = Yii::$app->controller->action->id;
		
		//в зависимости от страницы - весим класс на body
		switch ($action)
		{
			case 'index':
				$bodyclass = 'index';
				break;
			default :
				$bodyclass = 'bg-narrow';
		}
		
		echo $bodyclass;
	}
}