<script type="text/x-template" id="pagination-template">
	<div v-if="totalPages > 1" class="title-fixed-paginator">
		<ul class="pagination paginations">
			<li class="page-item"><span class="page-link" @click="changePage(1)" :class="{ active: current == 1 }">1</span></li>
			<li v-if="firstElipsis" class="page-item"><span class="page-link">...</span></li>
			<li v-for="page in pages" class="page-item">
				<span class="page-link" @click="changePage(page)" :class="{ active: current == page }">
					{{ page }}
				</span>
			</li>
			<li v-if="lastElipsis" class="page-item"><span class="page-link">...</span></li>
			<li class="page-item"><span class="page-link" @click="changePage(totalPages)" :class="{ active: current == totalPages }">{{ totalPages }}</span></li>
		</ul>
	</div>
</script>