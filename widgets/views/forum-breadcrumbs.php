<? if(!empty($params)): ?>
	<ul class="forum__breadcrumbs">
		<? $i = 1 ?>
		<? foreach ($params as $param): ?>
			<? if($i < count($params)): ?>
				<li>
					<a href="<?= $param['url'] ?>" class="forum__breadcrumbs-link"><?= $param['label'] ?></a>
					<span class="forum__breadcrumbs-link-arrow">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/breadcrumbs_arrow.svg">
					</span>
				</li>
			<? else: ?>
				<li>
					<span class="forum__breadcrumbs-link active"><?= $param['label'] ?></span>
				</li>
			<? endif ?>
			<? $i++ ?>
		<? endforeach ?>
	</ul>
<? endif ?>