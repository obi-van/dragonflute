<div class="index-1__rates-server-time">
	<div class="js--timer-container">
		<div class="index-1__rates-server-time-title">Серверу</div>
		<div class="index-1__rates-server-time-counter" id="time-counter">
			<? if(app\helpers\KUseful::cnf('date_server_start')): ?>
				<span><?= $daysOnly ?></span> <?= \app\helpers\KUseful::declination($daysOnly, 'день', 'дня', 'дней', true) ?>
				<span><?= $hours ?></span> <?= \app\helpers\KUseful::declination($hours, 'час', 'часа', 'часов', true) ?>
				<span><?= $minuts ?></span> <?= \app\helpers\KUseful::declination($minuts, 'минута', 'минуты', 'минут', true) ?>
			<? else: ?>
				ЗАКРЫТОЕ БЕТА ТЕСТИРОВАНИЕ
			<? endif ?>
		</div>
	</div>
	<div class="index-1__rates-info js--rates-info hidden"></div>
</div>

<script type="text/javascript">
	var diff_const = '<?= $diff ?>';
	var serverDate = '<?= $currentDate ?>'
</script>