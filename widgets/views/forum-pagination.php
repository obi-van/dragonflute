<?
	use app\helpers\Debug;
?>

<ul class="forum__pagination">
	<?
		//кол-во страниц в пагинации
		$quantityPagesForPagination = $pagination->getPageCount();
		//текущая страница
		$currentPage = $pagination->getPage() + 1;
		$start = 1;
		$end = 0;

		if ($quantityPagesForPagination > 1)
		{
			$left = $currentPage - 1;
			$right = $quantityPagesForPagination - $currentPage;
			if ($left < floor($maxButtonCount / 2)) $start = 1;
			else $start = $currentPage - floor($maxButtonCount / 2);
			$end = $start + $maxButtonCount - 1;
			if ($end > $quantityPagesForPagination) {
				$start -= ($end - $quantityPagesForPagination);
				$end = $quantityPagesForPagination;
				if ($start < 1) $start = 1;
			}
		}
	?>

	<? if($currentPage != 1 && $currentPage >= $maxButtonCount): ?>
		<li>
			<a href="<?= Yii::$app->getUrlManager()->createUrl($url) ?>/<?= $currentPage - 1 ?>" class="forum__pagination-link active"><<</a>
		</li>
	<? endif ?>

	<? for($i = $start; $i <= $end; $i++): ?>
		<? if($i == $currentPage): ?>
			<li>
				<span class="forum__pagination-link active"><?= $currentPage ?></span>
			</li>
		<? else: ?>
			<? if($i == 1): ?>
				<li>
					<a href="<?= Yii::$app->getUrlManager()->createUrl($url) ?>" class="forum__pagination-link"><?= $i ?></a>
				</li>
			<? else: ?>
				<li>
					<a href="<?= Yii::$app->getUrlManager()->createUrl($url) ?><?= '/'.$i ?>" class="forum__pagination-link"><?= $i ?></a>
				</li>
			<? endif ?>
		<? endif ?>
	<? endfor ?>

	<? if($currentPage < $quantityPagesForPagination): ?>
		<li>
			<a href="<?= Yii::$app->getUrlManager()->createUrl($url) ?>/<?= $currentPage + 1 ?>" class="forum__pagination-link active">>></a>
		</li>
	<? endif ?>
</ul>