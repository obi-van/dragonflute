<ul class="forum__heder-menu-list">
	<li>
		<a href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>forum" class="forum__heder-menu-link">главная</a>
	</li>
	<li>
		<a href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>forum/rules" class="forum__heder-menu-link">правила форума</a>
	</li>
	<? if(Yii::$app->user->can('manageForum')): ?>
		<li>
			<a href="<?= Yii::$app->getUrlManager()->createUrl('/') ?>forum/players" class="forum__heder-menu-link">пользователи</a>
		</li>
	<? endif ?>
</ul>