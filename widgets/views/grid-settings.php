<!-- component template -->
<script type="text/x-template" id="grid-template">
	<div>
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th v-for="key in columns"
					    @click="sortBy(key['label'], key['relation'])"
					    :class="[{ active: sortKey == key['label']}, key['class']]">
						<span>{{ key['title'] }}</span>
						<i v-if="sortOrders[key['label']] == 'ASC'" class="fa fa-caret-up" aria-hidden="true"></i>
						<i v-else-if="sortOrders[key['label']] == 'DESC'" class="fa fa-caret-down" aria-hidden="true"></i>
						<i v-else-if="key['sort'] == undefined || key['sort'] == true" class="fa fa-sort" aria-hidden="true"></i>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="entry in filteredData">
					<td v-for="key in columns" :class="key['class']">
						<textarea v-bind:name="key['label']" rows="4" v-if="key['label'] == 'value' && entry.type == 2"
						          class="form-control" v-model="entry[key['label']]" @input="changeParams()"></textarea>
						<span v-else-if="key['label'] == 'value' && entry.type == 5">
							<input v-bind:name="key['label']" v-model="entry[key['label']]" type="checkbox" @input="changeParams()" />
						</span>
						<span v-else-if="key['label'] == 'value' && entry.type == 3">
							<input class="form-control form-control_min-width-50" v-bind:name="key['label']" type="text"
							       v-model="entry[key['label']]" @input="changeParams()">
						</span>
						<span v-else="">
							{{entry[key['label']]}}
						</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</script>