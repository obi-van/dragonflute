<!-- component template -->
<script type="text/x-template" id="grid-template">
	<div>
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<? if ($params['removing'] == true): ?>
						<th v-if="modelName != 'childrenstatuses'" class="td-col td-col_checkbox"><i class="fa fa-check-square-o" aria-hidden="true"></i></th>
					<? endif ?>
					<th v-for="key in columns"
					    @click="sortBy(key['label'], key['relation'])"
					    :class="[{ active: sortKey == key['label']}, key['class'], {'th-link': key['sort'] != false}]">
						<span v-if="key['label'] == 'visible'"><i class="fa fa-eye" aria-hidden="true"></i></span>
						<span v-else-if="key['label'] == 'order'"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></span>
						<span v-else>{{ key['title'] }}</span>
						<i v-if="sortOrders[key['label']] == 'ASC'" class="fa fa-caret-up" aria-hidden="true"></i>
						<i v-else-if="sortOrders[key['label']] == 'DESC'" class="fa fa-caret-down" aria-hidden="true"></i>
						<i v-else-if="key['sort'] == undefined || key['sort'] == true" class="fa fa-sort" aria-hidden="true"></i>
					</th>
					<? if ($params['editing'] == true || $params['removing'] == true): ?>
						<th class="td-col td-col_param"><i class="fa fa-wrench" aria-hidden="true"></i></th>
					<? endif ?>
				</tr>
			</thead>
			<tbody>
				<tr v-for="entry in filteredData">
					<? if ($params['removing'] == true): ?>
						<td  v-if="modelName != 'childrenstatuses'" class="td-col td-col_checkbox">
							<input class="th-link" type="checkbox" v-bind:id="entry.id" v-bind:value="entry.id" v-model="checkedItems">
						</td>
					<? endif ?>
					<td v-for="key in columns" :class="key['class']">
						<span v-if="['photo', 'img', 'preview', 'svgimg'].includes(key['label'])" v-html="entry[key['label']]"></span>
						<span v-else-if="key['label'] == 'order'">
							<input class="form-control form-control_min-width-50" v-bind:name="key['label']" type="text" v-model="entry[key['label']]"
							       @change="changeParams()">
						</span>
						<span v-else-if="key['label'] == 'visible'">
							<input class="th-link" v-bind:name="key['label']" v-model="visibleItems" v-bind:value="entry.id"
							       type="checkbox" @change="changeParams()">
						</span>
						<span v-else="" v-html="entry[key['label']]"></span>
					</td>
					<? if ($params['editing'] == true || $params['removing'] == true): ?>
						<td class="nowrap td-col td-col_param">
							<? if ($params['editing'] == true): ?>
								<i  v-if="!(modelName == 'childrenstatuses' && entry.key == 'new')"
									class="vue-editer fa fa-pencil" aria-hidden="true" @click="callFormModel(entry.id)" title="Edit"></i>
							<? endif ?>
							<? if ($params['removing'] == true): ?>&nbsp;
								<i v-if="!(modelName == 'childrenstatuses' && entry.key == 'new')"
								   class="vue-remover fa fa-times" aria-hidden="true" @click="removeItem(entry.id)" title="Remove"></i>
							<? endif ?>
						</td>
					<? endif ?>
				</tr>
			</tbody>
		</table>
		<simplert :useRadius="true"
		          :useIcon="true"
		          ref="simplert">
		</simplert>
	</div>
</script>