<?php

$params = require(__DIR__ . '/params.php');

// Определяем окружение
require(__DIR__ . '/db.php');

$config = [
	'id' => 'dragonflute-console',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'controllerNamespace' => 'app\commands',
	'components' => [
		'authManager' => [
			'class' => 'yii\rbac\DbManager',
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => setDbOptions(dirname(__FILE__) . '/../')
	],
	'params' => $params,
	'controllerMap' => [
		'generate' => [
			'class' => 'bizley\migration\controllers\MigrationController',
			'migrationTable' => '{{%migration}}',
		],
		'migrate' => [
			'class' => 'yii\console\controllers\MigrateController',
			'migrationTable' => '{{%migration}}',
		],
	],
];

return $config;