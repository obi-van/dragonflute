<?php

/**
 * Список разработчиков
 */
function getDevelopers($docroot)
{
	$developers = [
		'ivan' => $docroot.'ivan.cnf',
	];

	return $developers;
}

function setDbOptions($docroot) {

	$developers = getDevelopers($docroot);

	// Базовые установки соединения
	$settings = [
		'class' => 'yii\db\Connection',
		//'emulatePrepare' => true,
		//'enableProfiling' => YII_ENV_DEV,
		//'enableParamLogging' => YII_ENV_DEV,
		'charset' => 'utf8',
		'tablePrefix' => 'df_',
	];

	if(YII_ENV_PROD)
	{
		return $settings + [
			'dsn' => 'mysql:host=localhost;dbname=agafondh_dragonf',
			'username' => 'agafondh_dragonf',
			'password' => '9Lr%oO2J',
		];
	}
	elseif (YII_ENV_TEST)
	{
		return $settings + [
			'dsn' => 'mysql:host=shared-28.smartape.ru;dbname=user2906_dragonflute',
			'username' => 'user2906_dragonflute',
			'password' => '8K9r8U7j',
		];
	}
	elseif(YII_ENV_DEV && file_exists($developers['ivan']) && !is_dir($developers['ivan']))
	{
		return $settings + [
			'dsn' => 'mysql:host=localhost;dbname=dragon_flute_dev',
			'username' => 'root',
			'password' => file_get_contents($developers['ivan']),
		];
	}
}