<?php

$params = require __DIR__ . '/params.php';
require(dirname(__FILE__) . '/db.php');

$config = [
    'id' => 'dragonflute',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
	'language' => 'ru-RU',
	'modules' => [
		'admin' => [
			'class' => 'app\modules\admin\Module',
		],
		'forum' => [
			'class' => 'app\modules\forum\ForumModule',
		],
		'rbac' => [
			'class' => 'mdm\admin\Module',
			'layout' => 'left-menu',
			'controllerMap' => [
				'assignment' => [
					'class' => 'mdm\admin\controllers\AssignmentController',
					/* 'userClassName' => 'app\models\User', */
					'idField' => 'user_id',
					'usernameField' => 'email',
				],
			],
			//'mainLayout' => "@app/views/layouts/auth.php"
		]
	],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'ICWHuSKDZeRWrIvaVYDa3#fBO8dGcg@t@UTu8',
	        'baseUrl' => ''
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
	    'assetManager' => [
		    'bundles' => [
			    'yii\web\JqueryAsset' => [
				    'js'=>[]
			    ],
			    'yii\bootstrap\BootstrapPluginAsset' => [
				    'js'=>[]
			    ],
			    'yii\bootstrap\BootstrapAsset' => [
				    'css' => [],
			    ],
		    ],
	    ],
	    'mailer'       => [
		    'class'     => 'yii\swiftmailer\Mailer',
		    // send all mails to a file by default. You have to set
		    // 'useFileTransport' to false and configure a transport
		    // for the mailer to send real emails.
		    //'useFileTransport' => true,
		    'transport' => [
			    'class'      => 'Swift_SmtpTransport',
			    'host'       => 'smtp.yandex.ru',
			    'username'   => 'reg@dragonflute.ru',
			    'password'   => 'Perimeter1!',
			    'port'       => '465',
			    'encryption' => 'ssl',
		    ],
	    ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => setDbOptions($_SERVER['DOCUMENT_ROOT'] . '/../'),
	    'authManager' => [
		    'class' => 'yii\rbac\DbManager',
	    ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

            	//ПАГИНАЦИЯ
	            '/page/<page:\d+>' => 'main/index',

	            //АВТОРИЗАЦИЯ
	            '<controller(auth)>/<action(restore)>/<code>' => '<controller>/<action>',
	            '<controller(auth)>/<action:[\w-]+>'  => '<controller>/<action>',

	            //АДМИНПАНЕЛЬ
	            '<module(admin/)>'                    => 'admin/settings',
	            '<module(admin)>'                     => 'admin/settings',
	            '<module(admin)>/<action:[\w-]+>'     => 'admin/<action>',

	            //FORUM

	            '<module(forum)>/cabinet/<action:remove-img>'  => 'forum/cabinet/<action>',
	            '<module(forum)>/cabinet/<action:update|remove-img|ban>/<id:\d+>'  => 'forum/cabinet/<action>',
	            '<module(forum)>/<controller(cabinet)>/<id:\d+>'  => 'forum/cabinet',
	            '<module(forum)>/players'  => 'forum/cabinet/players',


	            '<module(forum)>/<category:[\w-]+>/<action:(create|delete)>'  => 'forum/topics/<action>',
	            '<module(forum)>/<category:[\w-]+>/<action:(update)>/<id:\d+>'  => 'forum/topics/<action>',

	            '<module(forum)>/<category:[\w-]+>/<topic:[\w-]+>'  => 'forum/topics',
	            '<module(forum)>/<category:[\w-]+>/<topic:[\w-]+>/<page:\d+>'  => 'forum/topics',
	            '<module(forum)>/<category:[\w-]+>/<topic:[\w-]+>/<action:(answer-delete)>'  => 'forum/topics/<action>',

	            '<module(forum)>/rules'  => 'forum/main/rules',
	            '<module(forum)>/<category:[\w-]+>'  => 'forum/categories',

	            '<module(forum)>'                    => 'forum/main',
	            '<module(forum/)>'                    => 'forum/main',

	            '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',

	            //CRON
	            '<controller(cron)>/<action:[\w-]+>'  => '<controller>/<action>',

	            //FRONT
	            '/'                               => 'main/index',
	            '/<action:[\w-]+>'                => 'main/<action>',
	            '/<action:[\w-]+>/<alias:[\w-]+>' => 'main/<action>',

	            //SITEMAP
	            'sitemap.xml'=>'sitemap/index',

            ],
        ],
	    'as access' => [
		    'class' => 'mdm\admin\components\AccessControl',
		    'allowActions' => [
			    'main/*',
			    'admin/*',
			    //'rbac/*',
			    'some-controller/some-action',
			    // The actions listed here will be allowed to everyone including guests.
			    // So, 'admin/*' should not appear here in the production, of course.
			    // But in the earlier stages of your development, you may probably want to
			    // add a lot of actions here until you finally completed setting up rbac,
			    // otherwise you may not even take a first step.
		    ]
	    ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
	    'generators' => [
		    'kiloCrud'  => [
			    'class'     => 'app\components\gii\crud\Generator',
			    'templates' => [
				    'admin' => '@app/components/gii/crud/admin',
			    ]
		    ],
		    'kiloModel' => [
			    'class'     => 'app\components\gii\model\Generator',
			    'templates' => [
				    'admin' => '@app/components/gii/model/admin'
			    ]
		    ],
		    'model'     => [
			    'class'     => 'yii\gii\generators\model\Generator',
			    'templates' => [
				    'kilo' => '@app/components/gii/model/base'
			    ]
		    ]
	    ],
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
