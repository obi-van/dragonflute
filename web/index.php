<?php

if (in_array($_SERVER['HTTP_HOST'], [
	'dragonflute',
	'localhost'
]))
{
	defined('YII_ENV') or define('YII_ENV', 'dev');
	defined('YII_DEBUG') or define('YII_DEBUG', true);
}
elseif (in_array($_SERVER['HTTP_HOST'],[
	'dragonflute.igrushata.ru'
]))
{
	defined('YII_ENV') or define('YII_ENV', 'test');
	defined('YII_DEBUG') or define('YII_DEBUG', false);
}
else
{
	defined('YII_ENV') or define('YII_ENV', 'prod');
	defined('YII_DEBUG') or define('YII_DEBUG', false);
};

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
