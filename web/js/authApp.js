$(document).ready(function () {
	//amnesia form
	$('.js--change-pass-form-submit').unbind('click').on('click', function (e) {
		e.preventDefault();

		//дизэйблим форму
		amnesiaFormDisable();

		const params = {};
		const csrfParam = $('meta[name="csrf-param"]').attr("content");
		const csrfToken = $('meta[name="csrf-token"]').attr("content");
		params[csrfParam] = csrfToken;
		params['hash_code'] = $('input[name="af_hash-code"]').val();
		params['password'] = $('#af_password').val();
		params['repassword'] = $('#af_repassword').val();

		$.ajax({
			url: '/auth/amnesia-restore',
			global: false,
			type: 'POST',
			data: params,
			dataType: 'json',
			success: function (json) {

				//раздизэйбливаем форму
				amnesiaFormUnDisable();

				if (json.success !== undefined) {
					$('.page-auth__form-wrap-step-1').addClass('hidden');
					$('.page-auth__form-wrap-step-2').removeClass('hidden');
				} else {
					if(json.error === 'fatal_error'){
						window.location.replace(window.location.host + '/auth/restore/account_fail');
					}

					if (json.error.password && json.error.password[0] !== undefined) {
						$('#af-error-password').html(json.error.password[0]).removeClass('hidden');
					}

					if (json.error.repassword && json.error.repassword[0] !== undefined) {
						$('#af-error-repassword').html(json.error.repassword[0]).removeClass('hidden');
					}

					$('input[name="af_password"]').unbind('input').on('input', function () {
						$('#af-error-password').addClass('hidden');
						if ($(this).val() === '')
							$('#af-error-password').removeClass('hidden');
						return false;
					});

					$('input[name="af_repassword"]').unbind('input').on('input', function () {
						$('#af-error-repassword').addClass('hidden');
						if ($(this).val() === '')
							$('#af-error-repassword').removeClass('hidden');
						return false;
					});
				}

			}
		});
	});

	function amnesiaFormDisable() {
		$('input[name="af_password"]').attr("disabled", true);
		$('input[name="af_repassword"]').attr("disabled", true);
		$('.js--change-pass-form-submit').addClass('disabled');
		$('.js--change-pass-form-submit-text').addClass('invisible');
		$('.js--reg-form-submit-loader').removeClass('hidden');
	}

	function amnesiaFormUnDisable() {
		$('input[name="af_password"]').attr("disabled", false);
		$('input[name="af_repassword"]').attr("disabled", false);
		$('.js--change-pass-form-submit').removeClass('disabled');
		$('.js--change-pass-form-submit-text').removeClass('invisible');
		$('.js--reg-form-submit-loader').addClass('hidden');
	}
});