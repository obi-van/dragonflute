GridView = Vue.component('grid-view', {
	template: '#grid-template',
	props: {
		data: Array,
		columns: Array,
		filterKey: String,
		programType: String,
		modelName: String,
		tableName: String,
		refreshing: Boolean,
		changed: Boolean,
		searchColumns: Array,
		limit: String,
		controllerId: String,
		pageNumber: Number,
		countItems: Number,
		checkedForRemove: Array
	},
	data: function () {
		var sortOrders = {};
		this.columns.forEach(function (key) {
			sortOrders[key] = '';
		});
		return {
			sortKey: '',
			sortOrders: sortOrders,
			filteredData: this.data,
			checkedItems: [],
			visibleItems: []
		}
	},
	watch: {
		//отрабатывает поиск
		filterKey: function (val) {
			var searchQuery = {
				searchQuery: val,
				searchColumns: this.searchColumns
			};
			var parameters = {
				method: 'search',
				params: JSON.stringify(searchQuery)
			};
			this.changeCriteria(parameters);
		},
		// выводить по
		limit: function (val) {
			var parameters = {
				method: 'limit',
				params: val
			};
			this.changeCriteria(parameters);
		},
		// номер страницы
		pageNumber: function (val) {
			var offset;
			if (val === 1) {
				offset = -1;
			}
			else if (val > 1) {
				offset = this.limit * (val - 1);
			}
			var parameters = {
				method: 'offset',
				params: offset
			};
			this.changeCriteria(parameters);
		},
		checkedItems: function (arr) {
			Checked = arr;
		},
		checkedForRemove: function (arr) {
			this.removeItems(arr);
		},
		tableName: function (val) {
			var parameters = {
				tableName: val,
				columns: Columns
			};
			this.getItemsByCriteria(parameters);
		},
		programType: function (val) {
			var parameters = {
				method: 'where',
				params: JSON.stringify({
					field: 'type',
					value: val
				})
			};
			this.changeCriteria(parameters);
		},
		refreshing: function () {
			this.getItemsByCriteria();
		}
	},
	methods: {
		sortBy: function (key, relation) {
			this.sortKey = key;
			var sortOrders = this.sortOrders;
			this.columns.forEach(function (column) {
				if (column['label'] !== key)
					sortOrders[column['label']] = '';
				if (column['label'] === key && (sortOrders[key] === undefined || sortOrders[key] === ""))
					sortOrders[column['label']] = "ASC";
				if (column['sort'] !== undefined && !column['sort'])
					delete sortOrders[column['label']];
			});
			this.sortOrders = sortOrders;
			if (this.sortOrders[key] !== undefined) {
				if (this.sortOrders[key] === "DESC") {
					this.sortOrders[key] = "ASC";
				}
				else if (this.sortOrders[key] === "ASC") {
					this.sortOrders[key] = "DESC";
				}

				var order;
				if (relation === undefined) {
					order = {
						field: key,
						trend: this.sortOrders[key]
					};
				} else {
					order = {
						field: key,
						trend: this.sortOrders[key]
					};
				}
				var parameters = {
					method: 'orderBy',
					params: JSON.stringify(order)
				};

				this.changeCriteria(parameters);
			}
		},
		changeCriteria: function (params) {
			var _this = this;
			var csrfParam = $('meta[name="csrf-param"]').attr("content");
			var csrfToken = $('meta[name="csrf-token"]').attr("content");
			params[csrfParam] = csrfToken;
			$.ajax({
				url: window.location.protocol + '//' + window.location.host + '/admin/'+ this.controllerId + '/changecriteria',
				type: "POST",
				data: params,
				success: function (response) { // Success.
					response = JSON.parse(response);
					if (response.success !== undefined && response.success !== null) {
						_this.getItemsByCriteria();
					}
					else if (response.error !== undefined && response.error !== null)
						console.log(response.error);
					else
						console.log("Ошибка.");
				},
				error: function (response) { // Error.
					console.log('An error occurred.');
				}
			});
		},
		getItemsByCriteria: function (params) {
			if (params === undefined)
				params = {};
			var _this = this;
			var csrfParam = $('meta[name="csrf-param"]').attr("content");
			var csrfToken = $('meta[name="csrf-token"]').attr("content");
			params[csrfParam] = csrfToken;
			$.ajax({
				url: window.location.protocol + '//' + window.location.host + '/admin/'+ this.controllerId + '/getitemsbycriteria',
				type: "POST",
				data: params,
				success: function (response) { // Success.
					response = JSON.parse(response);
					_this.filteredData = response.items;
					Count = parseInt(response.count);
					_this.changeCountItems(Count);
					_this.visibleChange();
					if (response.columns !== undefined) {
						Columns = response.columns;
						this.changeColumns(Columns);
					}
				},
				error: function (response) { // Error.
					console.log('An error occurred.');
				}
			});
		},
		removeItem: function (id) {
			var _this = this;
			var obj = {
				message: "Вы уверены, что хотите удалить элемент?",
				type: 'info',
				useConfirmBtn: true,
				customConfirmBtnText: 'Да',
				onConfirm: function () {
					var params = {};
					var csrfParam = $('meta[name="csrf-param"]').attr("content");
					var csrfToken = $('meta[name="csrf-token"]').attr("content");
					params[csrfParam] = csrfToken;
					$.ajax({
						url: window.location.protocol + '//' + window.location.host + '/admin/' + _this.controllerId + '/delete/' + id,
						type: "POST",
						data: params,
						success: function (response) { // Success.
							if (response === "1")
								_this.getItemsByCriteria();
							else
								alert("Не удалось удалить элемент. Пожалуйста, попробуйте снова, либо обратитесь к разработчикам системы.")
						},
						error: function (response) { // Error.
							console.log('An error occurred.');
						}
					});
				},
				customCloseBtnText: 'Нет'
			};
			this.$refs.simplert.openSimplert(obj);
		},
		removeItems: function (ids) {
			var _this = this;
			var obj = {
				message: "Вы уверены, что хотите удалить выбранные элементы: " + ids.length + " шт.?",
				type: 'info',
				useConfirmBtn: true,
				customConfirmBtnText: 'Да',
				onConfirm: function () {
					var params = {};
					params.ids = ids;
					var csrfParam = $('meta[name="csrf-param"]').attr("content");
					var csrfToken = $('meta[name="csrf-token"]').attr("content");
					params[csrfParam] = csrfToken;
					$.ajax({
						url: window.location.protocol + '//' + window.location.host + '/admin/' + _this.controllerId + '/removeitems',
						type: "POST",
						data: params,
						success: function (response) { // Success.
							if (response === "1") {
								_this.getItemsByCriteria();
								_this.checkedItems = [];
							}
							else
								alert("Не удалось удалить выбранные элементы. Пожалуйста, попробуйте снова, либо обратитесь к разработчикам системы.")
						},
						error: function (response) { // Error.
							console.log('An error occurred.');
						}
					});
				},
				customCloseBtnText: 'Нет'
			};
			this.$refs.simplert.openSimplert(obj);
		},
		changeCountItems: function (count) {
			this.$emit('change-count-items', count);
		},
		changeColumns: function (columns) {
			this.$emit('change-columns', columns);
		},
		callFormModel: function (itemId) {
			var params = {};
			params.id = itemId;

			this.$emit('show-update-form', params);
		},
		goToLink: function (type, id, name) {
			this.$emit('go-to-link', type, id, name);
		},
		changeParams: function () {
			this.$emit('changing', this.filteredData, this.visibleItems);
		},
		visibleChange: function () {
			var _this = this;
			var result = [];
			this.columns.forEach(function (key) {
				if (key['label'] === 'visible') {
					_this.filteredData.forEach(function(item, i, arr) {
						if (item.visible == 1)
							result.push(item.id);
					});
				}
			});
			_this.visibleItems = result;
		}
	},
	mounted: function () {
		this.visibleChange();
	}
});

Vue.component('pagination', {
	template: '#pagination-template',
	props: {
		current: {
			type: Number,
			default: 1
		},
		total: {
			type: Number,
			default: 0
		},
		perPage: {
			type: Number,
			default: 20
		},
		pageRange: {
			type: Number,
			default: 2
		}
	},
	data: function () {
		return {
			firstElipsis: false,
			lastElipsis: false
		}
	},
	computed: {
		pages: function () {
			var pages = [];

			for (var i = this.rangeStart; i <= this.rangeEnd; i++) {
				pages.push(i)
			}

			if (this.rangeStart > 2)
				this.firstElipsis = true;
			else
				this.firstElipsis = false;

			if (this.rangeEnd < (this.totalPages - 1))
				this.lastElipsis = true;
			else
				this.lastElipsis = false;

			return pages
		},
		rangeStart: function () {
			var start = this.current - this.pageRange;

			return (start > 1) ? start : 2
		},
		rangeEnd: function () {
			var end = this.current + this.pageRange;

			return (end < this.totalPages) ? end : (this.totalPages - 1)
		},
		totalPages: function () {
			return Math.ceil(this.total / this.perPage)
		},
		nextPage: function () {
			return (this.current + 1) < this.totalPages
		},
		prevPage: function () {
			return this.current - 1
		}
	},
	methods: {
		hasFirst: function () {
			return this.rangeStart !== 2
		},
		hasLast: function () {
			return this.rangeEnd < this.totalPages
		},
		hasPrev: function () {
			return this.current > 1
		},
		hasNext: function () {
			return this.current < this.totalPages
		},
		changePage: function (page) {
			this.$emit('page-changed', page);
		}
	}
	/*created: function() {
		console.log(this);
	}*/
});