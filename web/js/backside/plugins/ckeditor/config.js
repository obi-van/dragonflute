/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.removePlugins = 'image';
	config.extraPlugins = 'image2,widgetselection,lineutils,wpmore,youtube';
	// config.enterMode = CKEDITOR.ENTER_BR;
	config.fillEmptyBlocks = false;
	config.allowedContent = true;
	config.youtube_responsive = true;
	config.youtube_controls = true;

	config.toolbar = [
		{ name: 'document', items: [ 'Source', '-'] },
		{ name: 'clipboard', items: [ 'Format' ] },
		{ name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'RemoveFormat' ] },
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight' ] },
		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', items: [ 'Image', 'Youtube', '-', 'Undo', 'Redo', 'Maximize'] },
	];
};