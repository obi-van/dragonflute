function previewFile(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#imgSrc').attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function getBase64(file) {
	var reader = new FileReader();
	reader.readAsDataURL(file);
}

function removeImg(controllerId, id, attrName) {
	var params = {};
	params.id = id;
	params.attrName = attrName;
	var csrfParam = $('meta[name="csrf-param"]').attr("content");
	var csrfToken = $('meta[name="csrf-token"]').attr("content");
	params[csrfParam] = csrfToken;

	if (confirm('Вы действительно хотите удалить изображение?')) {
		removeImgCallback(controllerId, params);
	}
}


function removeImgCallback(controllerId, params) {
	var _this = this;

	$.ajax({
		url: window.location.protocol + '//' + window.location.host + '/admin/' + controllerId + '/remove-img',
		type: "POST",
		data: params,
		success: function (response) { // Success.
			if (parseInt(response) === 1)
				$('#imgSrc').attr('src', window.location.protocol + '//' + window.location.host + '/imgs/noimage.png');
			else
				alert('Не удалось удалить изображение');
		},

		error: function (response) { // Error.
			console.log(response);
		}
	});
}