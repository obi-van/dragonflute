$(document).ready(function () {
	const wbbOpt = {
		lang: "ru",
		buttons: "bold,italic,underline,|,img,video,link,quote,|,removeFormat",
		allButtons: {
			img: {
				title: "Вставте ссылку на изображение",
				modal: {
					title: "Вставте ссылку на изображение",
				},
			}
		}
	}

	$("#editor").wysibb(wbbOpt);


	//Удаление топика
	const deleteBtn = $('.js-topic-remove');

	deleteBtn.each(function () {
		let _delete = $(this);

		_delete.unbind('click').on('click', function (e) {
			e.preventDefault();

			const topicId = $(this).data('id');
			const url = document.location.href;

			if (confirm('Вы уверены, что хотите удалить тему?'))
			{
				const params = {};
				const csrfParam = $('meta[name="csrf-param"]').attr("content");
				const csrfToken = $('meta[name="csrf-token"]').attr("content");
				params[csrfParam] = csrfToken;
				params['id'] = topicId;

				$.ajax({
					url: url + '/delete',
					global: false,
					type: 'POST',
					data: params,
					dataType: 'json',
					success: function () {
						window.location = url;
					}
				});
			}
		})
	});

	const removeAvatarBtn = $('.js-remove-avatar');

	$('.js-avatar-input').on('change', function () {
		const _this = this;
		let params = {};

		if (_this.files && _this.files[0]) {
			const reader = new FileReader();

			reader.onload = function (e) {
				$('#imgSrc').attr('src', e.target.result);

				const file = document.querySelector('#imgInput').files[0];

				if (file !== undefined)
					params.img = $('#imgSrc').attr('src');

				var csrfParam = $('meta[name="csrf-param"]').attr("content");
				var csrfToken = $('meta[name="csrf-token"]').attr("content");
				params[csrfParam] = csrfToken;

				const userId = $('.js-user-info').data('id');

				$.ajax({
					url: window.location.protocol + '//' + window.location.host + '/forum/cabinet/update/' + userId,
					type: "POST",
					data: params,
					success: function (response) { // Success.
						response = JSON.parse(response);
						if (response.success !== undefined && response.success !== null) {
							$('#forum-user-avatar-error').addClass('hidden');
							removeAvatarBtn.removeClass('hidden');
						}
						else{
							$('#forum-user-avatar-error').text(response.error);
							$('#forum-user-avatar-error').removeClass('hidden');
						}
					},
					error: function (response) { // Error.
						console.log(response);
					}
				});
			};

			reader.readAsDataURL(_this.files[0]);
		}
	})

	$('.js-remove-avatar').on('click', function () {
		let params = {};
		const id = $('.js-user-info').data('id');
		params.id = id;
		params.attrName = 'img';
		var csrfParam = $('meta[name="csrf-param"]').attr("content");
		var csrfToken = $('meta[name="csrf-token"]').attr("content");
		params[csrfParam] = csrfToken;

		if (confirm('Вы действительно хотите удалить изображение?')) {
			$.ajax({
				url: window.location.protocol + '//' + window.location.host + '/forum/cabinet/remove-img',
				type: "POST",
				data: params,
				success: function (response) { // Success.
					if (parseInt(response) === 1)
					{
						$('#imgSrc').attr('src', window.location.protocol + '//' + window.location.host + '/imgs/forum/forum_not_avatar.svg');
						removeAvatarBtn.addClass('hidden');
					}
					else
					{
						$('#forum-user-avatar-error').text('Не удалось удалить изображение');
						$('#forum-user-avatar-error').removeClass('hidden');
					}

				},

				error: function (response) { // Error.
					console.log(response);
				}
			});
		}
	})

	//кнопка удаления ответа по теме
	$('.js-answer-remove').unbind('click').on('click', function (e) {
		e.preventDefault();

		if (confirm('Вы действительно хотите удалить ответ')) {
			let params = {};
			params.id = $(this).data('id');
			var csrfParam = $('meta[name="csrf-param"]').attr("content");
			var csrfToken = $('meta[name="csrf-token"]').attr("content");
			params[csrfParam] = csrfToken;
			const url = document.location.href;

			$.ajax({
				url: window.location.protocol + '//' + window.location.host + '/forum/category/topic/answer-delete',
				global: false,
				type: 'POST',
				data: params,
				dataType: 'json',
				success: function () {
					window.location = url;
				}
			});
		}

	})

	//кнопка бана игрока
	$('.js-user-edit-ban').unbind('click').on('click', function (e) {
		e.preventDefault();

		if (confirm('Вы действительно хотите забанить/разбанить пользователя')) {
			let params = {};
			params.id = $(this).data('id');
			var csrfParam = $('meta[name="csrf-param"]').attr("content");
			var csrfToken = $('meta[name="csrf-token"]').attr("content");
			params[csrfParam] = csrfToken;
			const url = document.location.href;

			$.ajax({
				url: window.location.protocol + '//' + window.location.host + '/forum/cabinet/ban/' + params.id,
				global: false,
				type: 'POST',
				data: params,
				dataType: 'json',
				success: function () {
					window.location = url;
				}
			});
		}
	})
});