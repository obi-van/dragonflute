$(document).ready(function () {

	//таймер на время работы сервера

	if(dateStart){
		let jsServerDate = new Date(serverDate);

		setInterval(function () {
			//прибавляем интервал к серверному времени
			jsServerDate.setMinutes(jsServerDate.getMinutes() + 1);

			const diff = Math.floor(Math.abs(jsServerDate - new Date(dateStart))/1000);

			const years = Math.floor(diff / (365*60*60*24));
			const months = Math.floor((diff - years * 365*60*60*24) / (30*60*60*24));
			const days = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24)/ (60*60*24));
			const daysOnly = Math.floor(diff / (3600 * 24));
			const hours = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24 - days*60*60*24)/ (60*60));
			const minuts = Math.floor((diff - years * 365*60*60*24 - months*30*60*60*24 - days*60*60*24 - hours*60*60)/ 60);

			const workingTime = '<span>' + daysOnly + '</span> ' + num2str(daysOnly, ['день', 'дня', 'дней']) + ' <span>' + hours + '</span> ' + num2str(hours, ['час', 'часа', 'часов']) + ' <span>' + minuts + '</span> ' + num2str(minuts, ['минута', 'минуты', 'минут']);

			if(diff !== diff_const) {
				document.getElementById('time-counter').innerHTML = workingTime;
			}


		}, 60000);
	}

	//склонение числа num
	function num2str(n, text_forms) {
		n = Math.abs(n) % 100; var n1 = n % 10;
		if (n > 10 && n < 20) { return text_forms[2]; }
		if (n1 > 1 && n1 < 5) { return text_forms[1]; }
		if (n1 == 1) { return text_forms[0]; }
		return text_forms[2];
	}

	//header
	window.onscroll = function() {
		myFunction();
	};

	var nav = document.getElementById("nav");
	var navbar = document.getElementById("navbar");
	var sticky = navbar.offsetTop;
	var windowWidth = $( window ).width();
	//for mobile
	var navHeight = $(nav).height();

	$( window ).resize(function() {
		windowWidth = $( window ).width();
		navHeight = $(nav).height();
	});

	if (windowWidth < 415){
		nav.classList.add("sticky");
	}

	function myFunction() {

		//desktop
		if (windowWidth > 414){
			if (window.pageYOffset >= sticky) {
				nav.classList.add("sticky")
			} else {
				nav.classList.remove("sticky");
			}
		}
	}

	//left menu open
	$('.js--burger-menu').unbind('click').on('click', function (e) {
		e.preventDefault();

		$('.js--left-panel').addClass('is-active');
		$('body').addClass('is-overflow');
	});

	//left menu close
	$('.js--left-panel-cross').unbind('click').on('click', function (e) {
		e.preventDefault();

		leftMenuClose();
	});

	// swiper slider - big slider om index page
	const slideCount = $("#index-slider .swiper-slide").length;
	let options = {};

	if(slideCount > 1) {
		options = {
			loop: true,

			pagination: {
				el: '.swiper-pagination',
				type: 'fraction',
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			autoplay: {
				delay: 5000,
			},
		}

		var mySwiper = new Swiper ('#index-slider', options);
	}

	//swiper slider - small slider statistics by ridboss
	var rbSwiper = new Swiper ('#rb-slider', {
		slidesPerView: 1,
		loop: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.boss-swiper-button-next',
			prevEl: '.boss-swiper-button-prev',
		},

		breakpoints: {
			415: {
				slidesPerView: 3,
			},
		}
	})

	//registration form
	const registrationBtn = $('.js--registration-btn');

	registrationBtn.unbind('click').on('click', function (e) {
		e.preventDefault();

		leftMenuClose();

		//colorbox init
		$.colorbox({
			open: true,
			inline:true,
			maxWidth: '95%',
			maxheight: '95%',
			close: '<span class="cb_close_btn"><svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg></span>',
			href: $('#registration-form'),
			onOpen: function () {
				$('body').addClass('is-overflow');
			},
			onClosed: function () {
				$('body').removeClass('is-overflow');
				clearRegForm();
			}
		});

	});

	//left menu close
	function leftMenuClose() {
		$('.js--left-panel').removeClass('is-active');
		$('body').removeClass('is-overflow');
	}

	//Форма регистрации
	$('.js--reg-form-submit').unbind('click').on('click', function (e) {
		e.preventDefault();

		//дизэйблим форму
		registrationFormDisable();

		const params = {};
		const csrfParam = $('meta[name="csrf-param"]').attr("content");
		const csrfToken = $('meta[name="csrf-token"]').attr("content");
		params[csrfParam] = csrfToken;
		params['account'] = $('#rf_account').val();
		params['email'] = $('#rf_email').val();
		params['password'] = $('#rf_password').val();
		params['repassword'] = $('#rf_repassword').val();

		$.ajax({
			url: '/auth/registration',
			global: false,
			type: 'POST',
			data: params,
			dataType: 'json',
			success: function (json) {

				//раздизэйбливаем форму
				registrationFormUnDisable();

				if (json.success !== undefined) {
					//скрываем форму регистрации и отображаем step_2
					$('.registration-form-1__step-1').addClass('hidden');
					$('.registration-form-1__step-2').removeClass('hidden');
					$.colorbox.resize();
				} else {
					//раскидываем ошибки из JSON
					if (json.error.account && json.error.account[0] !== undefined) {
						$('#rf-error-account').html(json.error.account[0]).removeClass('hidden');
					}

					if (json.error.email && json.error.email[0] !== undefined) {
						$('#rf-error-email').html(json.error.email[0]).removeClass('hidden');
					}

					if (json.error.password && json.error.password[0] !== undefined) {
						$('#rf-error-password').html(json.error.password[0]).removeClass('hidden');
					}

					if (json.error.repassword && json.error.repassword[0] !== undefined) {
						$('#rf-error-repassword').html(json.error.repassword[0]).removeClass('hidden');
					}

					//скрываем ошибки при активности на инпутах
					$('input[name="rf_account"]').unbind('input').on('input', function () {
						$('#rf-error-account').addClass('hidden');
						if ($(this).val() === '')
							$('#rf-error-account').removeClass('hidden');
						return false;
					});

					$('input[name="rf_email"]').unbind('input').on('input', function () {
						$('#rf-error-email').addClass('hidden');
						if ($(this).val() === '')
							$('#rf-error-email').removeClass('hidden');
						return false;
					});

					$('input[name="rf_password"]').unbind('input').on('input', function () {
						$('#rf-error-password').addClass('hidden');
						if ($(this).val() === '')
							$('#rf-error-password').removeClass('hidden');
						return false;
					});

					$('input[name="rf_repassword"]').unbind('input').on('input', function () {
						$('#rf-error-repassword').addClass('hidden');
						if ($(this).val() === '')
							$('#rf-error-repassword').removeClass('hidden');
						return false;
					});
				}

			}
		});
	});

	function clearRegForm() {
		$('input[name="rf_account"]').val('');
		$('input[name="rf_email"]').val('');
		$('input[name="rf_password"]').val('');
		$('input[name="rf_repassword"]').val('');

		if($('.registration-form-1__step-1').hasClass('hidden')){
			$('.registration-form-1__step-1').removeClass('hidden');
		}

		if(!$('.registration-form-1__step-2').hasClass('hidden')){
			$('.registration-form-1__step-2').addClass('hidden');
		}
	}

	function registrationFormDisable() {
		$('input[name="rf_account"]').attr("disabled", true);
		$('input[name="rf_email"]').attr("disabled", true);
		$('input[name="rf_password"]').attr("disabled", true);
		$('input[name="rf_repassword"]').attr("disabled", true);
		$('.js--reg-form-submit').addClass('disabled');
		$('.js--reg-form-submit-text').addClass('invisible');
		$('.js--reg-form-submit-loader').removeClass('hidden');
	}

	function registrationFormUnDisable() {
		$('input[name="rf_account"]').attr("disabled", false);
		$('input[name="rf_email"]').attr("disabled", false);
		$('input[name="rf_password"]').attr("disabled", false);
		$('input[name="rf_repassword"]').attr("disabled", false);
		$('.js--reg-form-submit').removeClass('disabled');
		$('.js--reg-form-submit-text').removeClass('invisible');
		$('.js--reg-form-submit-loader').addClass('hidden');
	}

	//amnesia form

	$('.js--amnesia-btn').unbind('click').on('click', function (e) {
		e.preventDefault();

		leftMenuClose();

		//colorbox init
		$.colorbox({
			open: true,
			inline:true,
			maxWidth: '95%',
			maxheight: '95%',
			close: '<span class="cb_close_btn"><svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg></span>',
			href: $('#amnesia-form'),
			onOpen: function () {
				$('body').addClass('is-overflow');
				clearAmnesiaForm();
			},
			onClosed: function () {
				$('body').removeClass('is-overflow');
			}
		});

	});

	function clearAmnesiaForm(){
		$('input[name="af_email"]').val('');
		if($('.amnesia-form-1__step-1').hasClass('hidden')){
			$('.amnesia-form-1__step-1').removeClass('hidden');
		}

		if(!$('.amnesia-form-1__step-2').hasClass('hidden')){
			$('.amnesia-form-1__step-2').addClass('hidden');
		}
	}

	$('.js--amn-form-submit').unbind('click').on('click', function (e) {
		e.preventDefault();

		//дизэйблим форму
		amnesiaFormDisable();

		const params = {};
		const csrfParam = $('meta[name="csrf-param"]').attr("content");
		const csrfToken = $('meta[name="csrf-token"]').attr("content");
		params[csrfParam] = csrfToken;
		params['email'] = $('#af_email').val();

		$.ajax({
			url: '/auth/amnesia',
			global: false,
			type: 'POST',
			data: params,
			dataType: 'json',
			success: function (json) {

				//раздизэйбливаем форму
				amnesiaFormUnDisable();

				if (json.success !== undefined) {
					//закидываем почту
					$('.amnesia-form-1__step-2').find('.js-player-email').text(params['email']);
					$('.amnesia-form-1__step-1').addClass('hidden');
					$('.amnesia-form-1__step-2').removeClass('hidden');
					$.colorbox.resize();
				} else {
					//раскидываем ошибки из JSON
					if (json.error.email && json.error.email[0] !== undefined) {
						$('#af-error-email').html(json.error.email[0]).removeClass('hidden');
					}

					//скрываем ошибки при активности на инпутах

					$('input[name="af_email"]').unbind('input').on('input', function () {
						$('#af-error-email').addClass('hidden');
						if ($(this).val() === '')
							$('#af-error-email').removeClass('hidden');
						return false;
					});
				}

			}
		});
	});

	function amnesiaFormDisable() {
		$('input[name="af_email"]').attr("disabled", true);
		$('.js--amn-form-submit').addClass('disabled');
		$('.js--amn-form-submit-text').addClass('invisible');
		$('.js--amn-form-submit-loader').removeClass('hidden');
	}

	function amnesiaFormUnDisable() {
		$('input[name="af_email"]').attr("disabled", false);
		$('.js--amn-form-submit').removeClass('disabled');
		$('.js--amn-form-submit-text').removeClass('invisible');
		$('.js--amn-form-submit-loader').addClass('hidden');
	}

	//показ информации при наведении на иконку
	const timer = $('.js--timer-container');
	const infoBlock = $('.js--rates-info');
	const infoContainer = $('#rates-icons-info');

	$('.js--not-shit').hover(function () {
		//получаем id
		let id = $(this).data('id');
		//ищем элемент с этим id
		let formatId = "#"+id;
		let info = infoContainer.find(formatId);
		let text = info.text();
		//заполняем и отображаем блок с текстом
		if(text.length){
			visibleInfoBlock(text);
		}
	}, function (e) {
		e.stopPropagation();
		hiddenInfoBlock();
	})

	//скрываем таймер
	function hiddenTimer() {
		timer.addClass('hidden');
	}

	//отображаем таймер
	function visibleTimer() {
		timer.removeClass('hidden');
	}

	//заполняем блок и отображаем
	function visibleInfoBlock(text) {
		infoBlock.html(text);
		hiddenTimer();
		infoBlock.removeClass('hidden');
	}

	//скрываем блок с описанием
	function hiddenInfoBlock() {
		infoBlock.addClass('hidden');
		visibleTimer();
	}
});