<?php
namespace app\components;

use app\helpers\Debug;
use app\helpers\KUseful;
use app\models\Categories;
use app\models\Seomapping;
use app\models\Projects;
use app\models\News;
use http\Encoding\Stream\Debrotli;
use Yii;

class KSeoHeaders
{
	public static function get($param)
	{
		$result = '';
		$controller = Yii::$app->controller->id;
		$action = Yii::$app->controller->action->id;

		$request = Yii::$app->request;
		$get = $request->get('alias');

		//$page = KArr::get($_GET, 'page', '');
		if (empty($action) && empty($controller))
			$uri = '/';
		elseif ($action == 'index')
			$uri = '/' . $controller;
		elseif ($controller == 'main' && !$get)
			$uri = '/' . $action;
		elseif ($controller == 'main' && $get)
			$uri = '/' . $action.'/'.$get;
		else
			$uri = '/' . $controller . '/' . $action.'/'.$get;

		$seomapping = Seomapping::find()->where('uri = :uri', [
			':uri' => $uri
		])->one();

		if (empty($seomapping)) {
			$result = self::indexPage($param);
		}
		else {
			if (isset($seomapping->{$param}) && !empty($seomapping->{$param})) {
				$result = $seomapping->{$param};
			}
		}

		//НОВОСТИ
		if($action == 'news')
		{
			$news = News::find()->where('alias = :alias', [':alias' => $get])->one();
			if($news)
			{
				$result = $news->{'seo'.$param};
			}
		}


		// Если по каким-то причинам значение параметра ничему не оказалось равно, берём значение с главной
		if (trim($result) == '') {
			$result = self::indexPage($param);
		}

		return $result;
	}

	public static function indexPage($param)
	{
		$result = '';
		$seomapping = Seomapping::find()->where('uri = "/"')->one();

		if (isset($seomapping->{$param}) && $seomapping->{$param} !== '') {
			$result = $seomapping->{$param};
		}

		return $result;
	}
}