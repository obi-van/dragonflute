<?php
namespace app\components;

use linslin\yii2\curl\Curl;
use Yii;
use app\helpers\Debug;

class DragonApi {
	private $url;
	private $api;

	public function __construct(){
		$this->url = 'http://dfapi.dragonflute.ru/api/';
		$this->api = '1bfa0a9c-3dad-400c-846b-e42caa3ed306';
	}

	public function getCountPlayers(){
		$curl = new Curl();

		$response = $curl->setRawPostData(
			json_encode([
				'api_key' => $this->api,
		        'action' => 'get_quantity_players'
		     ])
		)->post($this->url, true);

		return $response;
	}

	public function checkIssetPlayer($account, $email){
		$curl = new Curl();

		$response = $curl->setRawPostData(
			json_encode([
				'api_key' => $this->api,
				'action' => 'get_player',
				'user_data' => [
					'account' => $account,
					'email' => $email
				]
			])
		)->post($this->url, true);

		return $response;
	}

	public function createPlayer($account, $email, $key){
		$curl = new Curl();

		$response = $curl->setRawPostData(
			json_encode([
				'api_key' => $this->api,
				'action' => 'set_create_player',
				'user_data' => [
					'account' => $account,
					'email' => $email,
					'key' => $key
				]
			])
		)->post($this->url, true);

		return $response;
	}

	public function updatePlayer($account, $email, $key){
		$curl = new Curl();

		$response = $curl->setRawPostData(
			json_encode([
				'api_key' => $this->api,
				'action' => 'set_update_player',
				'user_data' => [
					'account' => $account,
					'email' => $email,
					'key' => $key
				]
			])
		)->post($this->url, true);

		return $response;
	}
}