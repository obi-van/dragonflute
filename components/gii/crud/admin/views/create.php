<?php

/* @var $this yii\web\View */
/* @var $generator app\components\gii\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

?>

<form id="createForm">

	<?= "<?= " ?> $this->render('_form', [
		'model' => $model,
	]) ?>

</form>