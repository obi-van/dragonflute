<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */
echo "<?php\n";
?>

use yii\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
?>

<form id="updateForm">
	<input type="hidden" name="id" value="<?= "<?= " ?> $model->id ?>">

	<?= "<?= " ?> $this->render('_form', [
		'model' => $model,
	]) ?>

</form>
