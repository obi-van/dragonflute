<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator app\components\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
$baseControllerClass = StringHelper::basename($generator->baseControllerClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use app\helpers\KDebug;
use app\helpers\KUseful;
use Yii;
use <?= ltrim($generator->modelClass, '\\') ?>;
use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use yii\web\NotFoundHttpException;

/**
 * <?= $controllerClass ?> implements the CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends <?= $baseControllerClass . "\n" ?>
{
    /**
     * Lists all <?= $modelClass ?> models.
     * @return mixed
     */
    public function actionIndex()
    {
		$_SESSION['<?= $generator->sessionArrayName ?>']['criteria']['orderBy'] = ['id' => SORT_DESC];
		if (!isset($_SESSION['<?= $generator->sessionArrayName ?>']['criteria']['limit']))
			$_SESSION['<?= $generator->sessionArrayName ?>']['criteria']['limit'] = AdminController::PAGER_LIMIT;
		$query = <?= $modelClass ?>::createQuery($_SESSION['<?= $generator->sessionArrayName ?>']['criteria']);
		if (isset($_SESSION['<?= $generator->sessionArrayName ?>']) && !empty($_SESSION['<?= $generator->sessionArrayName ?>']['searchQuery']))
			$searchQuery = $_SESSION['<?= $generator->sessionArrayName ?>']['searchQuery'];
		else
			$searchQuery = '';
		$models = $query->all();
		$count = $query->count();

		return $this->render('index', [
			'models'  => <?= $modelClass ?>::dataOutput($models),
			'count'   => $count,
			'columns' => <?= $modelClass ?>::columns(),
			'searchQuery' => $searchQuery,
			'limit'   => $_SESSION['<?= $generator->sessionArrayName ?>']['criteria']['limit']
		]);
    }

    /**
     * Creates a new <?= $modelClass ?> model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model = new <?= $modelClass ?>();
		$request = Yii::$app->request->post();
<? if ($generator->uploadImg): ?>
		// если есть картинка, то готовимся к ее загрузке
		if (isset($request['img'])) {
			$img = $request['img'];
			unset($request['img']);
		}
<? endif ?>
		$model->attributes = $request;
<? if ($generator->visible): ?>
		if (!isset($request['onlyShow']) || $request['onlyShow'] == 'false')
			if (isset($model->visible) && in_array($model->visible, ['on', 1]))
				$model->visible = 1;
			else
				$model->visible = 0;
		else
			$model->visible = 1;
<? endif ?>

		// если в запросе пришел только параметр "onlyShow",
		// значит нужно просто отобразить форму
		if (isset($request['onlyShow']) && $request['onlyShow'] == 'true') {
			return $this->renderPartial('create', [
				'model' => $model,
			]);
		}
		// если пришли данные с формы, мы загрузили их в модель и она сохранилась - отдаем "success"
		else if ($model->save()) {
<? if ($generator->uploadImg): ?>
			// загрузка картинки
			if (isset($img)) {
				$settings['model'] = $model;
				$settings['attribute'] = '<?= $generator->imgName ?>';
				$settings['sizes'] = [<?= $generator->arraySizes ?>]; // массив с размерами в формате ['w'=>100, 'h'=>100]
				$this->kUploadBase64($img, $settings);
			}
<? endif ?>
			return json_encode([
				'success' => 'Сохранено'
			]);
		}
		// если позицию не удалось сохранить, выводим сообщение с ошибками
		else {
			if (!empty($model->getErrors()))
			return json_encode([
				'error' => KUseful::errorsToString($model->getErrors())
			]);
		else
			return json_encode([
				'error' => 'Произошла непредвиденная ошибка при сохранении'
			]);
		}
    }

    /**
     * Updates an existing <?= $modelClass ?> model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionUpdate(<?= $actionParams ?>)
    {
		$model = $this->findModel($id);
		$request = Yii::$app->request->post();
<? if ($generator->uploadImg): ?>
	// если есть картинка, то готовимся к ее загрузке
		if (isset($request['img'])) {
			$img = $request['img'];
			unset($request['img']);
		}
<? endif ?>

		$model->attributes = $request;
<? if ($generator->visible): ?>
        if (!isset($request['onlyShow']) || $request['onlyShow'] == 'false')
			if (isset($request['visible']) && in_array($request['visible'], ['on', 1]))
				$model->visible = 1;
			else
				$model->visible = 0;
<? endif ?>

		// если в запросе пришел только параметр "onlyShow",
		// значит нужно просто отобразить форму
		if (isset($request['onlyShow']) && $request['onlyShow'] == 'true') {
			return $this->renderPartial('update', [
				'model' => $model,
			]);
		}
		// если пришли данные с формы, мы загрузили их в модель и она сохранилась - отдаем "success"
		else if ($model->save()) {
<? if ($generator->uploadImg): ?>
			// загрузка картинки
			if (isset($img)) {
				$settings['model'] = $model;
				$settings['attribute'] = '<?= $generator->imgName ?>';
				$settings['sizes'] = [<?= $generator->arraySizes ?>]; // массив с размерами в формате ['w'=>100, 'h'=>100]
				$this->kUploadBase64($img, $settings);
			}
<? endif ?>
			return json_encode([
				'success' => 'Сохранено'
			]);
		}
		// если позицию не удалось сохранить, выводим сообщение с ошибками
		else {
			if (!empty($model->getErrors()))
				return json_encode([
					'error' => KUseful::errorsToString($model->getErrors())
				]);
			else
				return json_encode([
					'error' => 'Произошла непредвиденная ошибка при сохранении'
				]);
		}
    }

    /**
     * Deletes an existing <?= $modelClass ?> model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if ($this->findModel($id)->delete())
			return "1";
		else
			return "0";
    }

	public function actionRemoveitems()
	{
		$request = Yii::$app->request->post();
		if (isset($request['ids']) && is_array($request['ids'])) {
			foreach ($request['ids'] as $id) {
				$this->findModel($id)->delete();
			}
			return "1";
		}
		else {
			return "0";
		}
	}

	<? if ($generator->uploadImg): ?>

	public function actionRemoveImg()
	{

		$request = Yii::$app->request->post();

		if (isset($request['id']) && !empty($request['attrName']) && !empty($this->findModel($request['id']))) {

			$model = $this->findModel($request['id']);

			$attrName = $request['attrName'];

			if (!empty($model->$attrName))

				$this->kImageDeleteBase64($model, $attrName);

			return '1';
		}

		return "0";
	}

	<? endif ?>

	/*
	* С помощью этого метода обновляем представление таблицы.
	* На входе может быть пусто, либо передай параметры для изменения таблицы (колонок).
	* На выход обязательно отдать 'items' и 'count'.
	* */
	public function actionGetitemsbycriteria()
	{
		$request = Yii::$app->request->post();

		$query = <?= $modelClass ?>::createQuery($_SESSION['<?= $generator->sessionArrayName ?>']['criteria']);
		$items = $query->all();
		$count = $query->count();

		$result['items'] = <?= $modelClass ?>::dataOutput($items);
		$result['count'] = $count;

		return json_encode($result);
	}

	/*
	* В этот метод передаем то, что нужно прибавить к существующей критерии,
	* измени его в соответствии с требованиями, если это необходимо
	* */
	public function actionChangecriteria()
	{
		$request = Yii::$app->request->post();
		$query = <?= $modelClass ?>::createQuery($_SESSION['<?= $generator->sessionArrayName ?>']['criteria']);

		if (!isset($request['method']) || !isset($request['params']))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);

		$params = json_decode($request['params']);

		// limit, offset используются при пагинации
		if ($request['method'] == 'limit') {
			$query->limit = $params;
			$query->offset = -1;
		}
		if ($request['method'] == 'offset') {
			$query->offset = $params;
		}
		if ($request['method'] == 'orderBy') {
		if (!isset($params->field) || !isset($params->trend))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);
		if ($params->trend == 'DESC')
			$trend = SORT_DESC;
		else
			$trend = SORT_ASC;
			$query->orderBy = [
				$params->field => $trend
			];
		}
		// если нужно добавить еще условия запроса (например при добавлении фильтра), описываем всё здесь
		if ($request['method'] == 'where') {
			if (!isset($params->field) || !isset($params->value))
				return json_encode([
					'error' => 'Некорректно указаны параметры'
				]);
			$query->where = [$params->field => $params->value];
		}

		$_SESSION['<?= $generator->sessionArrayName ?>']['criteria'] = <?= $modelClass ?>::createCriteria($query);
		if ($request['method'] == 'search') {
		if (!isset($params->searchColumns) || !isset($params->searchQuery))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);
			$_SESSION['<?= $generator->sessionArrayName ?>']['searchQuery'] = $params->searchQuery;
			$whereArr = ['OR'];
			foreach ($params->searchColumns as $column) {
				array_push($whereArr, ['like', $column, $params->searchQuery]);
			}
			$_SESSION['<?= $generator->sessionArrayName ?>']['criteria']['where'] = $whereArr;
			$_SESSION['<?= $generator->sessionArrayName ?>']['criteria']['offset'] = -1;
		}

		return json_encode([
			'success' => 'Критерий запроса обновлен'
		]);
	}

	// сохраняет изменения в полях "visible" и "order"
	public function actionSavechanges()
	{
		$request = Yii::$app->request->post();

		if (isset($request['data'])) {
	    	if (!isset($request['visible']))
			    $request['visible'] = [];
			foreach ($request['data'] as $item) {
				$model = $this->findModel($item['id']);
				$model->order = $item['order'];
				if (in_array($item['id'], $request['visible'])) {
					$model->visible = 1;
				} else {
					$model->visible = 0;
				}
				if (!$model->save() && !empty($model->getErrors()))
				{
					return json_encode([
						'error' => KUseful::errorsToString($model->getErrors())
					]);
				}
				elseif (!$model->save()) {
					return json_encode([
						'error' => 'Ошибка при сохранении элемента с id = '.$item['id']
					]);
				}
			}
		}
		else {
			return json_encode([
				'error' => 'недостаточно данных для выполнения данного действия'
			]);
		}

		return json_encode([
			'success' => 'Сохранено'
		]);
	}


    /**
     * Finds the <?= $modelClass ?> model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return <?=                   $modelClass ?> the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(<?= $actionParams ?>)
    {
<?php
if (count($pks) === 1) {
    $condition = '$id';
} else {
    $condition = [];
    foreach ($pks as $pk) {
        $condition[] = "'$pk' => \$$pk";
    }
    $condition = '[' . implode(', ', $condition) . ']';
}
?>
        if (($model = <?= $modelClass ?>::findOne(<?= $condition ?>)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
