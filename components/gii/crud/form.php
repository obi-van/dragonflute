<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\crud\Generator */

echo $form->field($generator, 'modelClass');
//echo $form->field($generator, 'searchModelClass');
echo $form->field($generator, 'controllerClass');
echo $form->field($generator, 'viewPath');
echo $form->field($generator, 'baseControllerClass');
echo $form->field($generator, 'sessionArrayName');
echo $form->field($generator, 'visible')->checkbox();
echo $form->field($generator, 'uploadImg')->checkbox();
echo $form->field($generator, 'imgName');
echo $form->field($generator, 'arraySizes');
echo $form->field($generator, 'titleIndex');
/*echo $form->field($generator, 'indexWidgetType')->dropDownList([
    'grid' => 'GridView',
    'list' => 'ListView',
]);
echo $form->field($generator, 'enableI18N')->checkbox();
echo $form->field($generator, 'enablePjax')->checkbox();*/
//echo $form->field($generator, 'messageCategory');
