import '../less/main.less'
import 'jquery-colorbox'

$(document).ready(function () {
	//header
	window.onscroll = function() {
		myFunction();
	};

	var nav = document.getElementById("nav");
	var navbar = document.getElementById("navbar");
	var sticky = navbar.offsetTop;
	var windowWidth = $( window ).width();
	//for mobile
	var navHeight = $(nav).height();

	$( window ).resize(function() {
		windowWidth = $( window ).width();
		navHeight = $(nav).height();
	});

	if (windowWidth < 415){
		nav.classList.add("sticky");
	}

	function myFunction() {

		//desktop
		if (windowWidth > 414){
			if (window.pageYOffset >= sticky) {
				nav.classList.add("sticky")
			} else {
				nav.classList.remove("sticky");
			}
		}
	}

	//left menu open
	$('.js--burger-menu').unbind('click').on('click', function (e) {
		e.preventDefault();

		$('.js--left-panel').addClass('is-active');
		$('body').addClass('is-overflow');
	});

	//left menu close
	$('.js--left-panel-cross').unbind('click').on('click', function (e) {
		e.preventDefault();

		leftMenuClose();
	});

	// swiper slider - big slider om index page
	var mySwiper = new Swiper ('#index-slider', {
		loop: true,

		pagination: {
			el: '.swiper-pagination',
			type: 'fraction',
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		autoplay: {
			delay: 5000,
		},
	})

	//swiper slider - small slider statistics by ridboss
	var rbSwiper = new Swiper ('#rb-slider', {
		slidesPerView: 1,
		loop: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		navigation: {
			nextEl: '.boss-swiper-button-next',
			prevEl: '.boss-swiper-button-prev',
		},

		breakpoints: {
			415: {
				slidesPerView: 3,
			},
		}
	})

	//registration form
	const registrationBtn = $('.js--registration-btn');

	registrationBtn.unbind('click').on('click', function (e) {
		e.preventDefault();

		leftMenuClose();

		//colorbox init
		$('#registration-form').colorbox({
			open: true,
			maxWidth: '95%',
			maxheight: '95%',
			close: '<span class="cb_close_btn"><svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg></span>',
			html: $('#registration-form').html(),
			onOpen: function () {
				$('body').addClass('is-overflow');
			},
			onClosed: function () {
				$('body').removeClass('is-overflow');
			}
		});

	});

	//amnesia form
	const amnesiaBtn = $('.js--amnesia-btn');
	amnesiaBtn.unbind('click').on('click', function (e) {
		e.preventDefault();

		leftMenuClose();

		//colorbox init
		$.colorbox({
			open: true,
			inline:true,
			maxWidth: '95%',
			maxheight: '95%',
			close: '<span class="cb_close_btn"><svg viewBox="0 0 329.26933 329" xmlns="http://www.w3.org/2000/svg"><path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/></svg></span>',
			href: $('#amnesia-form'),
			onOpen: function () {
				$('body').addClass('is-overflow');
			},
			onClosed: function () {
				$('body').removeClass('is-overflow');
			}
		});

	});

	//left menu close
	function leftMenuClose() {
		$('.js--left-panel').removeClass('is-active');
		$('body').removeClass('is-overflow');
	}

	//показ информации при наведении на иконку
	const timer = $('.js--timer-container');
	const infoBlock = $('.js--rates-info');
	const infoContainer = $('#rates-icons-info');

	$('.js--not-shit').hover(function () {
		//получаем id
		let id = $(this).data('id');
		//ищем элемент с этим id
		let formatId = "#"+id;
		let info = infoContainer.find(formatId);
		let text = info.text();
		//заполняем и отображаем блок с текстом
		if(text.length){
			visibleInfoBlock(text);
		}
	}, function (e) {
		e.stopPropagation();
		hiddenInfoBlock();
	})

	//скрываем таймер
	function hiddenTimer() {
		timer.addClass('hidden');
	}

	//отображаем таймер
	function visibleTimer() {
		timer.removeClass('hidden');
	}

	//заполняем блок и отображаем
	function visibleInfoBlock(text) {
		infoBlock.html(text);
		hiddenTimer();
		infoBlock.removeClass('hidden');
	}

	//скрываем блок с описанием
	function hiddenInfoBlock() {
		infoBlock.addClass('hidden');
		visibleTimer();
	}
});