const path = require(`path`);
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: `development`,
    entry: {
        html: './html/js/app.js',
        forum: './forum/js/app.js',
    },
    output: {
        path: __dirname + '/public', //абсолютный путь к директории сборки
        filename: "[name].js", // шаблон вместо [name] при сборке будут подставлены home.js и about.js
        library:  "[name]", // аналогично: глобальные переменные home н about
    },
    devtool: `source-map`,
    module: {
        rules: [
            {
                test: /\.(less)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "less-loader"
                ]
            }
        ]
    },
    devServer: {
        contentBase: [path.join(__dirname, 'html'), path.join(__dirname, 'forum')],
        watchContentBase: true,
        open: true,
        port: `9002`
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "./css/[name].css"
        })
    ]
};