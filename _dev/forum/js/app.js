import '../less/main.less'

$(document).ready(function () {
	const wbbOpt = {
		lang: "ru",
		buttons: "bold,italic,underline,|,img,video,link,quote,|,removeFormat",
		allButtons: {
			img: {
				title: "Вставте ссылку на изображение",
				modal: {
					title: "Вставте ссылку на изображение",
					tabs: [
						{
							input: [
								{param: "SRC",title:"Enter image URL",validation: '^http(s)?://.*?\.(jpg|png|gif|jpeg)$'}
							]
						}
					],
				},
				transform: {
					'<img src="{SRC}" title="{TITLE}" />':'[img title={TITLE}]{SRC}[/img]',
					'<div class="quote"><cite>{AUTHOR} wrote:</cite>{SELTEXT}</div>':'[quote={AUTHOR}]{SELTEXT}[/quote]'
				}
			}
		}
	}
	$("#editor").wysibb(wbbOpt);
});