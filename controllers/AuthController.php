<?php

namespace app\controllers;

use app\components\DragonApi;
use app\helpers\KUseful;
use app\models\Accounts;
use app\models\forms\AmnesiaRestoreForm;
use app\models\forms\GameRegistrationForm;
use app\models\Players;
use app\models\forms\LoginForm;
use app\models\forms\AmnesiaForm;
use app\models\User;
use http\Encoding\Stream\Debrotli;
use Symfony\Component\HttpKernel\EventListener\DebugHandlersListener;
use \Yii;
use yii\web\Controller;
use app\helpers\Debug;
use yii\widgets\ActiveForm;
use yii\web\Response;

class AuthController extends Controller
{
	public $layout = 'auth';

	public function actionLogin()
	{
		if (!Yii::$app->getUser()->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();

		if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
			$userRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
			$currentRole = KUseful::getUserRoleName($userRole);

			if ($currentRole == User::ROLE_ADMIN) {
				return $this->redirect('/admin');
			}
			else {
				return $this->goBack();
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	/**
	 * Logout
	 * @return string
	 */
	public function actionLogout()
	{
		Yii::$app->getUser()->logout();

		return $this->goHome();
	}

	public function actionRegistration()
	{
		$form = new GameRegistrationForm();
		$request = Yii::$app->request->post();

		$form->account = KUseful::parseCleanValue($request['account']);
		$form->email = KUseful::parseCleanValue($request['email']);
		$form->password = $request['password'];
		$form->repassword = $request['repassword'];

		if($form->validate() && KUseful::cnf('registration') === 'true'){
			$model = new User();
			$model->email = $form->email;
			$model->account = $form->account;
			$model->password = $form->password;
			$model->password_hash = $model->setPassword($form->password);
			$model->password_reset_token = $model->generatePasswordResetToken();
			$model->date_create = strtotime(date("Y-m-d h:i:s", time()));
			$model->activate = 0;
			//лютый костыль - приходится временно сохранять пароль без хэша
			$model->key = $form->password;
			$model->role = 'player';

			if($model->validate()){
				//чекнув БД сайта - чекаем БД игры (на наличие email или account)
				$api = new DragonApi();
				$playerInfo = json_decode($api->checkIssetPlayer($model->account, $model->email));

				if($playerInfo->account){
					return json_encode([
						'error' => ['account' => ['0' => 'Имя аккаунта уже используется']]
					]);
				}

				if($playerInfo->email){
					return json_encode([
						'error' => ['email' => ['0' => 'Адрес электронной почты уже используется']]
					]);
				}

				if($model->save())
				{
					//назначаем роль новому юзеру
					$userRole = Yii::$app->authManager->getRole('player');
					Yii::$app->authManager->assign($userRole, $model->id);

					//отправляем почту для подтверждения аккаунта
					$subject = "Запрос на подтверждение аккаунта для " . $_SERVER['HTTP_HOST'];

					$data = [
						'code' => $model->password_reset_token,
					];

					KUseful::sendEmail($subject, 'account', $data, Yii::$app->params['senderEmail'], $_SERVER['HTTP_HOST'], trim($model->email));


					return json_encode([
						'success' => 'Сохранено'
					]);
				}
				else
				{
					if (!empty($model->getErrors()))
					{
						return json_encode([
							'error' => $model->getErrors()
						]);
					}
				}
			}
			else
			{
				return json_encode([
					'error' => $model->getErrors()
				]);
			}
		}
		else
		{
			return json_encode([
				'error' => $form->getErrors()
			]);
		}
	}

	public function actionRestore(){
		$request = Yii::$app->request;
		$code = $request->get('code');

		//находим юзера
		$user = User::find()->where(['password_reset_token' => $code])->one();

		//если есть такой игрок и не протухло время
		if(!$user || $user->date_create < strtotime(date('Y-m-d H:i:s')) - 86400 || $user->date_create < 0){
			return $this->render('account_fail');
		}
		else
		{
			//если аккаунт не активирован (регистрация нового аккаунта)
			if(!$user->activate){
				$user->activate = 1;
				$user->password_reset_token = NULL;
				$user->scenario = 'update';

				if($user->save()){

					$api = new DragonApi();
					$request = json_decode($api->createPlayer($user->account, $user->email, $user->key));

					if($request->result == 'success'){
						$user->key = NULL;
						$user->save();

						return $this->render('account_success');
					}
					else
					{
						return $this->render('account_fail');
					}
				}
			}
			else //восстановление пароля
			{
				$data = [
					'code' => $code
				];
				return $this->render('account_change_pass', $data);
			}
		}
	}

	public function actionAmnesia(){
		$model = new AmnesiaForm();
		$request = Yii::$app->request->post();
		$model->email = KUseful::parseCleanValue($request['email']);

		if($model->validate() && KUseful::cnf('registration') === 'true'){
			//проверяем наличие пользователя в БД сайта
			$user = User::findOne(['email' => $model->email]);

			if(!$user){
				return json_encode([
					'error' => ['email' => ['0' => 'Пользователя с таким E-mail не существует']]
				]);
			}

			$user->password_reset_token = $user->generatePasswordResetToken();
			$user->date_update = strtotime(date("Y-m-d h:i:s", time()));
			$user->scenario = 'update';

			if($user->save()){
				//отправляем почту для подтверждения аккаунта
				$subject = "Запрос на смену пароля для аккаунта для " . $_SERVER['HTTP_HOST'];

				$data = [
					'code' => $user->password_reset_token,
				];

				KUseful::sendEmail($subject, 'amnesia', $data, Yii::$app->params['senderEmail'], $_SERVER['HTTP_HOST'], trim($model->email));

				return json_encode([
					'success' => 'Сохранено'
				]);
			}
			else
			{
				return json_encode([
					'error' => ['email' => ['0' => 'Что-то пошло не так, попробуйте повторить попытку позже']]
				]);
			}

		}else{
			return json_encode([
				'error' => $model->getErrors()
			]);
		}
	}

	//создание нового пароля при восстановлении
	public function actionAmnesiaRestore(){
		$request = Yii::$app->request->post();
		$form = new AmnesiaRestoreForm();
		$form->attributes = $request;

		if($form->validate()){
			//меняем пароль
			$user = User::find()->where(['password_reset_token' => $request['hash_code']])->one();
			$user->password_hash = $user->setPassword($form->password);
			$user->key = $form->password;
			$user->scenario = 'update';

			if($user->save()){
				$api = new DragonApi();
				$request = json_decode($api->updatePlayer($user->account, $user->email, $user->key));

				if($request->result == 'success'){
					//снова меняем hash чтобы не жмакали повторно письмо
					$user->password_reset_token = $user->generatePasswordResetToken();
					$user->key = NULL;
					$user->scenario = 'update';
					$user->save();

					return json_encode([
						'success' => 'Сохранено'
					]);
				}
				else
				{
					return json_encode([
						'error' => 'fatal_error'
					]);
				}
			}
			else
			{
				return json_encode([
					'error' => ['email' => ['0' => 'Что-то пошло не так, попробуйте повторить попытку позже']]
				]);
			}
		}
		else
		{
			return json_encode([
				'error' => $form->getErrors()
			]);
		}
	}
}
