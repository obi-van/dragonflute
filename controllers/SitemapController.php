<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\News;
use \Yii;

class SitemapController extends Controller
{
	public function actionIndex()
	{

		//Если нужно сбросить кэш, то расскоментируем и перезагрузим страницу
		//Yii::$app->cache->delete('sitemap');

		//проверяем есть ли закэшированная версия sitemap
		if (!$xml_sitemap = Yii::$app->cache->get('sitemap'))
		{
			$host = Yii::$app->request->hostInfo;
			$urls = [];

			$urls[] = [
				'loc' => $host . '/index',
				'lastmod' => date(DATE_W3C, strtotime('2020-09-15')),
				'changefreq' => 'daily',
				'priority' => 1.0
			];

			$urls[] = [
				'loc' => $host . '/about',
				'lastmod' => date(DATE_W3C, strtotime('2020-09-15')),
				'changefreq' => 'daily',
				'priority' => 1.0
			];

			$urls[] = [
				'loc' => $host . '/files',
				'lastmod' => date(DATE_W3C, strtotime('2020-09-15')),
				'changefreq' => 'daily',
				'priority' => 1.0
			];

			$news = News::find()->orderDate()->visibled()->all();
			foreach ($news as $item) {
				$urls[] = [
					'loc' => $host . '/news/' . $item->alias,
					'lastmod' => date(DATE_W3C, strtotime($item->date)),
					'changefreq' => 'daily',
					'priority' => 1.0
				];
			}

			$xml_sitemap = $this->renderPartial('index', [
				'urls' => $urls,
			]);

			Yii::$app->cache->set('sitemap', $xml_sitemap, 60*60*12); // кэшируем результат на 12 ч
		}

		Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
		$headers = Yii::$app->response->headers;
		$headers->add('Content-Type', 'text/xml');

		return $xml_sitemap;
	}
}