<?php

namespace app\controllers;

use app\helpers\Debug;
use app\models\Bosses;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Events;
use app\models\News;
use yii\data\Pagination;
use yii\web\HttpException;

class MainController extends Controller
{

	public function beforeAction($action)
	{
		if(!session_id())
		{
			session_start();
		}

		return parent::beforeAction($action);
	}

	/**
	 * Version
	 */
	const VERSION = 1.04;


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

	/**
	 * ERRORS
	 */
	public function actionError()
	{
		$exception = Yii::$app->errorHandler->exception;

		if($exception !== null)
		{
			$error = [
				'message' => 'Приносим свои извинения, произошла ошибка на сервере. <br>Мы уже знаем о проблеме и вскоре ее решим. А пока вы можете попробовать еще раз, либо связаться с администрацией сайта. <br>Спасибо за понимание.',
				'code'    => isset($exception->statusCode) ? $exception->statusCode : 'Ошибка'
			];

			$this->layout = 'error';
			if(Yii::$app->request->isAjax)
			{
				echo $error['message'];
			}
			else
			{
				switch($error['code'])
				{
					case 404:
						return $this->render('/error/error404', $error);
						break;
					case 403:
						return $this->render('/error/error403', $error);
						break;
					default:
						return $this->render('/error/error', $error);
						break;
				}
			}
		}
	}

    public function actionIndex()
    {
    	$queryArr = [];
	    $query = '';
	    $pageSize = 12;

	    $request = Yii::$app->request;
	    $page = $request->get('page');

		//Events for slider
	    $events = Events::find()->visibled()->order()->all();

	    //Bosses for slider
	    $bosses = Bosses::find()->visibled()->order()->all();

	    //top news
	    $topNews = News::find()->visibled()->where(['top' => 1])->orderDate()->all();

	    //news все кроме тех которые выведены в TOP
	    if(!empty($topNews)) {
	    	foreach($topNews as $news) {
			    $queryArr[] = $news->id;
		    }

		    $query = News::find()->visibled()->where(['not in','id', $queryArr])->orderDate();

	    } else {
		    $query = News::find()->visibled()->orderDate();
	    }

	    $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => $pageSize]);

	    $allNews = News::find()->where(['not in','id', $queryArr])->orderDate()->offset($pages->offset)->limit($pages->limit)->visibled()->all();

	    $pages->pageSizeParam = false;
	    $pages->forcePageParam = false;

	    //отдаем 404 если ввели страницу с несуществующим смещением
	    if($page > ceil($pages->totalCount / $pageSize)) {
		    throw new HttpException(404);
	    }

	    $data = [
		    'events'  => $events,
		    'bosses'  => $bosses,
		    'topNews' => $topNews,
		    'allNews' => $allNews,
		    'pages'   => $pages,
		    'page'    => $page,
	    ];

        return $this->render('index', $data);
    }

	public function actionNews()
	{
		$request = Yii::$app->request;
		$get = $request->get('alias');

		$newsDetail = News::find()->visibled()->where('alias = :alias', [':alias' => $get])->one();

		if(empty($newsDetail)) throw new HttpException(404);

		$data = [
			'newsDetail' => $newsDetail
		];

		return $this->render('news', $data);
	}

	public function actionAbout()
	{
		$data = [];

		return $this->render('about', $data);
	}

	public function actionFiles()
	{
		$data = [];

		return $this->render('files', $data);
	}
}