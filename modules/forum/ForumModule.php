<?php

namespace app\modules\forum;

use Yii;
use app\helpers\Debug;

class ForumModule extends \yii\base\Module
{
	public $controllerNamespace = 'app\modules\forum\controllers';

	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();
	}
}