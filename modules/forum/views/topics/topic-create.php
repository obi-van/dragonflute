<div class="forum__breadcrumbs-pagination-contauner">
	<?= app\widgets\ForumBreadcrumbs::widget(['params' => [
		[
			'label' => 'Главная',
			'url' => Yii::$app->getUrlManager()->createUrl('/forum')
		],
		[
			'label' => $category->title,
			'url' => Yii::$app->getUrlManager()->createUrl('/forum').'/'.$category->alias
		],
		[
			'label' => 'Создание',
			'url' => Yii::$app->getUrlManager()->createUrl('/forum').'/'.$category->alias
		]
	]]) ?>
</div>

<div class="forum__section-container">
	<table class="forum__section-post-table">
		<tr>
			<td class="forum__section-table-col-user-info">
				<div class="forum__user-info-container">
					<div class="forum__user-info">
						<div class="forum__user-info-name"><?= $user->nickname ? $user->nickname : $user->account ?></div>
						<div class="forum__user-info-role"><?= $user->role ?></div>
						<div class="forum__user-info-avatar">
							<?
								$imgs = json_decode($user->img);
								$img = isset($imgs->thumb_160x160) ? $imgs->thumb_160x160 : '';
							?>

							<img src="<?= app\helpers\KUseful::imgpatch($img, '/imgs/forum/forum_not_avatar.svg') ?>">
						</div>
						<div class="forum__user-info-registration-date">Регистрация: <?= \app\helpers\KUseful::date_for_human_from_unix($user->date_create) ?></div>
						<div class="forum__user-info-message-quantity">Сообщения: <?= $user->getQuantityMessages() ?></div>
					</div>
				</div>
			</td>
			<td class="forum__section-table-col-content">
				<div class="forum__post-message-container">
					<?php $form = yii\widgets\ActiveForm::begin([
							'options' => [
								'class' => 'forum__form-topic-create'
							],
						]) ?>
						<?= $form->field($model, 'title', [
							'inputOptions' => ['class' => 'forum__form-control', 'autocomplete' => 'off'],
							'template' => '<fieldset class="forum__form-group">{label}{input}<div id="forum-topic-name-error" class="forum__topic-create-error">{error}</div></fieldset>',
						]) ?>

						<? if(Yii::$app->user->can('manageForum')): ?>
							<?= $form->field($model, 'type', [
								'inputOptions' => ['class' => 'forum__form-control'],
								'template' => '<fieldset class="forum__form-group">{label}{input}<div id="forum-topic-name-error" class="forum__topic-create-error">{error}</div></fieldset>',
							])->dropDownList($types, ['options' => [
								'info' => ['selected' => true]
							]]) ?>
						<? endif ?>

						<?= $form->field($model, 'message', [
							'inputOptions' => [
								'id' => 'editor',
							],
							'template' => '<fieldset class="forum__form-group">{input}<div id="forum-topic-name-error" class="forum__topic-create-error">{error}</div></fieldset>',
						])->textarea() ?>

						<div class="forum__form-btn-container">
							<?= yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
						</div>

					<?php yii\widgets\ActiveForm::end() ?>
				</div>
			</td>
		</tr>
	</table>
</div>