<div class="forum__breadcrumbs-pagination-contauner">
	<?= app\widgets\ForumBreadcrumbs::widget(['params' => [
		[
			'label' => 'Главная',
			'url' => Yii::$app->getUrlManager()->createUrl('/forum')
		],
		[
			'label' => $category->title,
			'url' => Yii::$app->getUrlManager()->createUrl('/forum').'/'.$category->alias
		],
		[
			'label' => isset($topic->title) ? $topic->title : '...',
			'url' => Yii::$app->getUrlManager()->createUrl('/forum')
		]
	]]) ?>

	<?= app\widgets\CustomForumPagination::widget([
		'pagination' => $pages,
		'maxButtonCount' => 4,
		'url' => '/forum/'.$category->alias.'/'.$topic->alias
	]); ?>
</div>

<? if(isset($topic)): ?>
	<div class="forum__section-container">
		<div class="forum__section-header">
			<div class="forum__section-header-title-container">
				<div class="forum__section-header-icon">
					<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/section-list-icon.svg">
				</div>
				<div class="forum__section-header-title"><?= \app\helpers\KUseful::date_for_human($topic->date_create, true) ?></div>
			</div>
			<a href="#" class="forum__section-header-counter">#1</a>
		</div>
		<table class="forum__section-post-table">
			<tr>
				<td class="forum__section-table-col-user-info">
					<div class="forum__user-info-container">
						<div class="forum__user-info">
							<div class="forum__user-info-name"><?= !empty($topic->user->nickname) ? $topic->user->nickname : $topic->user->account ?></div>
							<div class="forum__user-info-role"><?= $topic->user->role ?></div>
							<div class="forum__user-info-avatar">
								<?
									$imgs = json_decode($topic->user->img);
									$img = isset($imgs->thumb_160x160) ? $imgs->thumb_160x160 : '';
								?>

								<img src="<?= app\helpers\KUseful::imgpatch($img, '/imgs/forum/forum_not_avatar.svg') ?>">
							</div>
							<div class="forum__user-info-registration-date">Регистрация: <?= \app\helpers\KUseful::date_for_human_from_unix($topic->user->date_create) ?></div>
							<div class="forum__user-info-message-quantity">Сообщения: <?= $topic->getQuantityMessages() ?></div>
						</div>
					</div>
				</td>
				<td class="forum__section-table-col-content">
					<div class="forum__post-message-container">
						<?= $topic->message ?>
					</div>
				</td>
			</tr>
		</table>
	</div>
<? else: ?>
	<div class="forum__section-container">
		<div class="forum__section-header">
			<div class="forum__section-header-title-container">
				<div class="forum__section-header-icon">
					<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/section-list-icon.svg">
				</div>
				<div class="forum__section-header-title">...</div>
			</div>
		</div>
		<table class="forum__section-post-table">
			<tr>
				<td class="forum__section-table-col-user-info">
					<div class="forum__user-info-container">
						<div class="forum__user-info">

						</div>
					</div>
				</td>
				<td class="forum__section-table-col-content">
					<div class="forum__post-message-container">
						Тема удалена или перемещена
					</div>
				</td>
			</tr>
		</table>
	</div>
<? endif ?>

<? if(isset($topic) && !empty($answers)): ?>
	<? $i = 2 ?>
	<? foreach($answers as $answer): ?>
		<div class="forum__section-container">
			<div class="forum__section-header">
				<div class="forum__section-header-title-container">
					<div class="forum__section-header-icon">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/section-list-icon.svg">
					</div>
					<div class="forum__section-header-title"><?= \app\helpers\KUseful::date_for_human($answer->date_create, true) ?></div>
				</div>
				<a href="#" class="forum__section-header-counter">#<?= $i ?></a>
			</div>
			<table class="forum__section-post-table">
				<tr>
					<td class="forum__section-table-col-user-info">
						<div class="forum__user-info-container">
							<div class="forum__user-info">
								<div class="forum__user-info-name"><?= $answer->user->nickname ? $answer->user->nickname : $answer->user->account ?></div>
								<div class="forum__user-info-role"><?= $answer->user->role ?></div>
								<div class="forum__user-info-avatar">
									<?
										$imgs = json_decode($answer->user->img);
										$img = isset($imgs->thumb_160x160) ? $imgs->thumb_160x160 : '';
									?>

									<img src="<?= app\helpers\KUseful::imgpatch($img, '/imgs/forum/forum_not_avatar.svg') ?>">
								</div>
								<div class="forum__user-info-registration-date">Регистрация: <?= \app\helpers\KUseful::date_for_human_from_unix($answer->user->date_create) ?></div>
								<div class="forum__user-info-message-quantity">Сообщения: <?= $answer->getQuantityMessages() ?></div>
							</div>
						</div>
					</td>
					<td class="forum__section-table-col-content">
						<div class="forum__post-message-container">
							<?= $answer->message ?>
						</div>
						<? if(Yii::$app->user->can('manageForum')): ?>
							<a href="#" class="forum__section-header-answer-remove js-answer-remove" data-id="<?= $answer->id ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
						<? endif ?>
					</td>
				</tr>
			</table>
		</div>
		<? $i++ ?>

		<? if($lastAnswer->id === $answer->id): ?>
			<? if(isset($topic) && $topic->type !== 'closed' && $user && !$user->forum_banned): ?>
				<div class="forum__section-container">
					<div class="forum__section-header">
						<div class="forum__section-header-title-container">
							<div class="forum__section-header-icon">
								<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/section-list-icon.svg">
							</div>
							<div class="forum__section-header-title"></div>
						</div>
					</div>
					<table class="forum__section-post-table">
						<tr>
							<td class="forum__section-table-col-user-info">
								<div class="forum__user-info-container">
									<div class="forum__user-info">
										<div class="forum__user-info-name"><?= $user->nickname ? $user->nickname : $user->account ?></div>
										<div class="forum__user-info-role"><?= $user->role ?></div>
										<div class="forum__user-info-avatar">
											<?
											$imgs = json_decode($user->img);
											$img = isset($imgs->thumb_160x160) ? $imgs->thumb_160x160 : '';
											?>

											<img src="<?= app\helpers\KUseful::imgpatch($img, '/imgs/forum/forum_not_avatar.svg') ?>">
										</div>
										<div class="forum__user-info-registration-date">Регистрация: <?= \app\helpers\KUseful::date_for_human_from_unix($user->date_create) ?></div>
										<div class="forum__user-info-message-quantity">Сообщения: <?= $user->getQuantityMessages() ?></div>
									</div>
								</div>
							</td>
							<td class="forum__section-table-col-content">
								<div class="forum__post-message-container">
									<?php $form = yii\widgets\ActiveForm::begin([
										'options' => [
											'class' => 'forum__form-topic-create'
										],
									]) ?>

									<?= $form->field($answerModel, 'message', [
										'inputOptions' => [
											'id' => 'editor',
										],
										'template' => '<fieldset class="forum__form-group">{input}<div id="forum-topic-name-error" class="forum__topic-create-error">{error}</div></fieldset>',
									])->textarea() ?>

									<div class="forum__form-btn-container">
										<?= yii\helpers\Html::submitButton('Ответить', ['class' => 'btn btn-primary']); ?>
									</div>

									<?php yii\widgets\ActiveForm::end() ?>
								</div>
							</td>
						</tr>
					</table>
				</div>
			<? endif ?>
		<? endif ?>
	<? endforeach ?>
<? else: ?>
	<? if(isset($topic) && $topic->type !== 'closed' && $user && !$user->forum_banned): ?>
		<div class="forum__section-container">
			<div class="forum__section-header">
				<div class="forum__section-header-title-container">
					<div class="forum__section-header-icon">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/section-list-icon.svg">
					</div>
					<div class="forum__section-header-title"></div>
				</div>
			</div>
			<table class="forum__section-post-table">
				<tr>
					<td class="forum__section-table-col-user-info">
						<div class="forum__user-info-container">
							<div class="forum__user-info">
								<div class="forum__user-info-name"><?= $user->nickname ? $user->nickname : $user->account ?></div>
								<div class="forum__user-info-role"><?= $user->role ?></div>
								<div class="forum__user-info-avatar">
									<?
									$imgs = json_decode($user->img);
									$img = isset($imgs->thumb_160x160) ? $imgs->thumb_160x160 : '';
									?>

									<img src="<?= app\helpers\KUseful::imgpatch($img, '/imgs/forum/forum_not_avatar.svg') ?>">
								</div>
								<div class="forum__user-info-registration-date">Регистрация: <?= \app\helpers\KUseful::date_for_human_from_unix($user->date_create) ?></div>
								<div class="forum__user-info-message-quantity">Сообщения: <?= $user->getQuantityMessages() ?></div>
							</div>
						</div>
					</td>
					<td class="forum__section-table-col-content">
						<div class="forum__post-message-container">
							<?php $form = yii\widgets\ActiveForm::begin([
								'options' => [
									'class' => 'forum__form-topic-create'
								],
							]) ?>

							<?= $form->field($answerModel, 'message', [
								'inputOptions' => [
									'id' => 'editor',
								],
								'template' => '<fieldset class="forum__form-group">{input}<div id="forum-topic-name-error" class="forum__topic-create-error">{error}</div></fieldset>',
							])->textarea() ?>

							<div class="forum__form-btn-container">
								<?= yii\helpers\Html::submitButton('Ответить', ['class' => 'btn btn-primary']); ?>
							</div>

							<?php yii\widgets\ActiveForm::end() ?>
						</div>
					</td>
				</tr>
			</table>
		</div>
	<? endif ?>
<? endif ?>
