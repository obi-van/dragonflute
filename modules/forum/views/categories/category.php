<div class="forum__breadcrumbs-pagination-contauner">
	<?= app\widgets\ForumBreadcrumbs::widget(['params' => [
		[
			'label' => 'Главная',
			'url' => Yii::$app->getUrlManager()->createUrl('/forum')
		],
		[
			'label' => $category->title,
			'url' => Yii::$app->getUrlManager()->createUrl('/').$category->alias
		],
	]]) ?>
</div>

<div class="forum__section-container">
	<div class="forum__section-header">
		<div class="forum__section-header-title-container">
			<div class="forum__section-header-icon">
				<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/section-list-icon.svg">
			</div>
			<div class="forum__section-header-title"><?= $category->title ?></div>
		</div>
	</div>
	<table class="forum__section-table">
		<tr>
			<th class="forum__section-table-col-forum">Тема</th>
			<th class="forum__section-table-col-theme">Ответов</th>
			<th class="forum__section-table-col-message">Просмотров</th>
			<th class="forum__section-table-col-last-message">Последнее сообщение</th>
		</tr>

		<? if(!empty($topics)): ?>
			<? foreach($topics as $topic): ?>
				<tr>
					<td class="forum__section-table-col-forum">
						<div class="forum__theme-forum-container">
							<div class="forum__theme-forum-icon">
								<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/<?= $topic->getTopicTypeImg() ?>">
							</div>
							<div class="forum__theme-forum-text-container">
								<div class="forum__theme-forum-text-wrap">
									<div class="forum__theme-status"><?= $topic->getTopicType() ?>:</div>
									<a href="<?= Yii::$app->getUrlManager()->createUrl('/forum') ?>/<?= $category->alias ?>/<?= $topic->alias ?>" class="forum__theme-forum-text-title">
										<?=  app\helpers\KUseful::date_for_robot($topic->date_create, false) ?> — <?= app\helpers\KUseful::lineCutter($topic->title, 30) ?>
									</a>
								</div>
								<div class="forum__theme-forum-text-description">
									Автор: <?= $topic->getTopicAuthorName() ?>
								</div>
							</div>
						</div>
					</td>
					<td class="forum__section-table-col-theme"><?= $topic->getQuantityAnswers() ?></td>
					<td class="forum__section-table-col-message">0</td>
					<td class="forum__section-table-col-last-message">
						<div class="forum__theme-message-container">
							<?= $topic->getLastMessageInfo() ?>
						</div>
						<? if(Yii::$app->user->can('manageForum')): ?>
							<div class="forum__settings-container">
								<a href="<?= Yii::$app->getUrlManager()->createUrl('/forum') ?>/<?= $category->alias ?>/update/<?= $topic->id ?>" class="forum__settings-link"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
								<a href="#" class="forum__settings-link js-topic-remove" data-id="<?= $topic->id ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						<? endif ?>
					</td>
				</tr>
			<? endforeach ?>
		<? endif ?>
	</table>

	<? if(!Yii::$app->getUser()->isGuest && $category->for_players || Yii::$app->user->can('manageForum') && $user && !$user->forum_banned): ?>
		<div class="forum__create-topic-container">
			<a href="<?= Yii::$app->getUrlManager()->createUrl('/forum') ?>/<?= $category->alias ?>/create" class="forum__create-topic-btn">Создать</a>
		</div>
	<? endif ?>
</div>