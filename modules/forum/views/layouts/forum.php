<?php
	use yii\helpers\Html;
	use app\helpers\Debug;
	use app\helpers\KUseful;
	use app\components\KSeoHeaders;
	use yii\helpers\Url;
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<?= Html::csrfMetaTags() ?>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title><?= KSeoHeaders::get('title') ?></title>
		<meta content="<?= KSeoHeaders::get('description') ?>" name="description" />
		<meta content="<?= KSeoHeaders::get('keywords') ?>" name="keywords" />
		<link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@web') ?>/imgs/apple-touch-icon.png" />
		<link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@web') ?>/imgs/favicon-32x32.png" />
		<link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@web') ?>/imgs/favicon-16x16.png" />

		<meta property="og:locale" content="ru_RU" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?= Url::canonical() ?>" />
		<meta property="og:title" content="<?= KSeoHeaders::get('title') ?>" />
		<meta property="og:description" content="<?= KSeoHeaders::get('description') ?>" />
		<meta property="og:image" content="<?= Url::to('/imgs/dragonflute.png', true) ?>" />

		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:site" content="Dragon Flute"/>
		<meta name="twitter:title" content="<?= KSeoHeaders::get('title') ?>">
		<meta name="twitter:description" content="<?= KSeoHeaders::get('description') ?>"/>
		<meta name="twitter:image:src" content="<?= Url::to('/imgs/dragonflute.png', true) ?>"/>
		<meta name="twitter:domain" content="<?= Url::canonical() ?>"/>
		<meta name="yandex-verification" content="183c7286cde442a4" />

		<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">
		<script src="https://use.fontawesome.com/e08a08be9a.js"></script>
		<link href="<?= Yii::getAlias('@web') ?>/css/forum.css?<?= \app\controllers\MainController::VERSION ?>" rel="stylesheet" type="text/css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="<?= Yii::getAlias('@web') ?>/js/jquery.wysibb.js"></script>
<!--		<script src="http://cdn.wysibb.com/js/jquery.wysibb.min.js"></script>-->
		<link rel="stylesheet" href="https://cdn.wysibb.com/css/default/wbbtheme.css" />
		<link href="<?= Yii::getAlias('@web') ?>/css/forum.css?<?= \app\controllers\MainController::VERSION ?>" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<?php $this->beginBody(); ?>
			<div class="forum-content">
				<div class="forum-content__wrap">
					<a href="/" class="forum-content__main-logo">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/main_logo.svg" alt="Dragon Flute">
					</a>
				</div>

				<section class="forum">
					<div class="forum__wrap">
						<div class="forum__header">

							<?= app\widgets\ForumMainMenu::widget() ?>

							<?/*
							<form class="forum__heder-search" action="/searchpage" method="post">
								<input type="text" name="searchForm[query]" value="" placeholder="Поиск">
								<button type="submit" class="forum__search-btn">
									<span class="forum__search-btn-wrap">
										<svg viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M12.0206 2.52037C9.27458 -0.238316 4.80516 -0.238316 2.0591 2.52037C-0.686366 5.27965 -0.686366 9.76902 2.0591 12.5283C4.50454 14.9844 8.31319 15.2475 11.0575 13.3297C11.1152 13.6042 11.2474 13.8661 11.4599 14.0796L15.459 18.0971C16.0418 18.6814 16.9835 18.6814 17.5634 18.0971C18.1455 17.5122 18.1455 16.5662 17.5634 15.9831L13.5642 11.9644C13.3529 11.7527 13.0916 11.6193 12.8183 11.5613C14.7286 8.80381 14.4667 4.97825 12.0206 2.52037ZM10.758 11.2599C8.70786 13.3195 5.37128 13.3195 3.3217 11.2599C1.27272 9.2003 1.27272 5.84897 3.3217 3.78938C5.37128 1.73038 8.70786 1.73038 10.758 3.78938C12.8082 5.84897 12.8082 9.2003 10.758 11.2599Z" fill="white"/>
										</svg>
									</span>
								</button>
							</form>
							*/?>
						</div>
						<div class="forum__main-container">
							<div class="forum__avtorisation-container">
								<? if(Yii::$app->user->isGuest): ?>
									Здравствуйте, <a href="<?= Yii::$app->getUrlManager()->createUrl('/auth/login') ?>">войдите</a> или <a href="#">зарегистрируйтесь</a>.
								<? else: ?>
									Здравствуйте, <a href="<?= Yii::$app->getUrlManager()->createUrl('/forum/cabinet') ?>/<?= Yii::$app->user->identity->id ?>">
										<?= app\helpers\KUseful::getUserAccount(Yii::$app->user->id) ?>
									</a>
								<? endif ?>
							</div>


							<?= $content ?>

						</div>
					</div>
				</section>
			</div>

			<footer class="footer-1">
				<div class="footer-1__logo">
					<img src="<?= Yii::getAlias('@web') ?>/imgs/main_logo.svg" alt="Dragon Flute">
				</div>
				<div class="footer-1__copy">
					<?= KUseful::cnf('footer_copy') ?>
				</div>
			</footer>

			<script src="<?= Yii::getAlias('@web') ?>/js/forumApp.js?<?= \app\controllers\MainController::VERSION ?>"></script>

		<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>