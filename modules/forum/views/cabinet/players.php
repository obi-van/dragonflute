<div class="forum__players-container">
	<div class="forum__players-list">
		<? if(!empty($players)): ?>
			<? foreach($players as $player): ?>
				<div class="forum__players-item <? if($player->forum_banned): ?> forum__players-banned<? endif ?>">
					<a href="<?= Yii::$app->getUrlManager()->createUrl('/forum/cabinet') ?>/<?= $player->id ?>" class="forum__players-cabinet"><?= $player->nickname ? $player->nickname : $player->account ?></a>

					<? if(Yii::$app->user->can('manageForum')): ?>
						<a href="#" class="forum__players-edit js-user-edit-ban" data-id="<?= $player->id ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
					<? endif ?>
				</div>
			<? endforeach ?>
		<? endif ?>
	</div>
</div>