<?php

use app\helpers\KUseful;

?>
<div class="forum__cabinet-container">

	<div class="forum__cabinet-user-name"><?= !empty($user->nickname) ? $user->nickname : $user->account ?></div>

	<? if($user->id === $currentUserId): ?>
		<?php $form = yii\widgets\ActiveForm::begin([
			'options' => [
				'class' => 'forum__form-user-nick-create'
			],
		]) ?>
		<?= $form->field($user, 'nickname', [
			'inputOptions' => ['class' => 'forum__form-control', 'autocomplete' => 'off'],
			'template' => '<fieldset class="forum__form-group">{label}{input}<div id="forum-topic-name-error" class="forum__topic-create-error">{error}</div></fieldset>',
		]) ?>

		<div class="forum__form-btn-container">
			<?= yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
		</div>

		<?php yii\widgets\ActiveForm::end() ?>

		<? if($user->id === $currentUserId): ?>
			<div class="forum__user-avatar-img-label">Изображение аватарки будет обработано до размера 160x160px</div>
			<div class="forum__user-avatar-error hidden" id="forum-user-avatar-error"></div>
		<? endif ?>
		<div class="forum__user-avatar-container js-user-info" data-id="<?= $user->id ?>">

			<?
				$imgs = json_decode($user->img);
				$img = isset($imgs->thumb_160x160) ? $imgs->thumb_160x160 : '';
			?>

			<img id="imgSrc" class="forum__user-avatar-img" src="<?= app\helpers\KUseful::imgpatch($img, '/imgs/forum/forum_not_avatar.svg') ?>" alt="Image">

			<? if($user->id === $currentUserId): ?>
				<span class="forum__user-remove-avatar-btn js-remove-avatar <?= !empty($img) ? '' : 'hidden' ?>"><i class="fa fa-times"></i></span>
				<div class="forum__user-add-avatar-container">
					<input type="file" id="imgInput" name="img" class="custom-file-input js-avatar-input" accept="image/*"">
					<span class="forum__add-avatar-img-btn">Выберите файл</span>
				</div>
			<? endif ?>
		</div>
	<? else: ?>
		<div class="forum__user-avatar-container">

			<?
				$imgs = json_decode($user->img);
				$img = isset($imgs->thumb_160x160) ? $imgs->thumb_160x160 : '';
			?>

			<img id="imgSrc" class="forum__user-avatar-img" src="<?= app\helpers\KUseful::imgpatch($img, '/imgs/forum/forum_not_avatar.svg') ?>" alt="Image">
		</div>
	<? endif ?>
</div>