<div class="forum__breadcrumbs-pagination-contauner">
	<?= app\widgets\ForumBreadcrumbs::widget(['params' => [
		[
			'label' => 'Главная',
			'url' => Yii::$app->getUrlManager()->createUrl('/forum')
		],
	]]) ?>
</div>

<? if(!empty($forumData)): ?>
	<? foreach ($forumData as $section): ?>
		<div class="forum__section-container">
			<div class="forum__section-header">
				<div class="forum__section-header-title-container">
					<div class="forum__section-header-icon">
						<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/section-list-icon.svg">
					</div>
					<div class="forum__section-header-title"><?= $section->title ?></div>
				</div>
			</div>
			<table class="forum__section-table">
				<tr>
					<th class="forum__section-table-col-forum">Форум</th>
					<th class="forum__section-table-col-theme">Темы</th>
					<th class="forum__section-table-col-message">Сообщения</th>
					<th class="forum__section-table-col-last-message">Последнее сообщение</th>
				</tr>

				<? if(!empty($section->categories)): ?>
					<? foreach ($section->categories as $category): ?>
						<tr>
							<td class="forum__section-table-col-forum">
								<div class="forum__category-forum-container">
									<div class="forum__category-forum-icon">
										<img src="<?= Yii::getAlias('@web') ?>/imgs/forum/<?= $section->img ?>">
									</div>
									<div class="forum__category-forum-text-container">
										<a href="<?= Yii::$app->getUrlManager()->createUrl('/forum') ?>/<?= $category->alias ?>" class="forum__category-forum-text-title"><?= $category->title ?></a>
										<? if(!empty($category->description)): ?>
											<div class="forum__category-forum-text-description">
												<?= $category->description ?>
											</div>
										<? endif ?>
									</div>
								</div>
							</td>
							<td class="forum__section-table-col-theme"><?= $category->getQuantityTopics() ?></td>
							<td class="forum__section-table-col-message"><?= $category->getQuantityAnswers() ?></td>
							<td class="forum__section-table-col-last-message">
								<div class="forum__category-message-container">
									<?= $category->getLastMessage() ?>
								</div>
							</td>
						</tr>
					<? endforeach ?>
				<? endif ?>
			</table>
		</div>
	<? endforeach ?>
<? endif ?>