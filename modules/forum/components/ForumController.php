<?php

namespace app\modules\forum\components;

use app\helpers\KUseful;
use app\helpers\Debug;
use yii\web\Controller;
use Yii;

class ForumController extends Controller
{
	public $layout = 'forum';

	/**
	 * Удаление изображения,
	 * представленного в БД как json
	 * {img:"img", thumb:"thumb"}
	 *
	 * @param int $settings ['id'] - id ячейки
	 * @param string $settings ['model'] - модель.свойство
	 * @param string $settings ['action'] - имя префикса у экшена
	 * @param array $settings ['params'] - дополнительные параметры
	 */
	protected function kImageDeleteBase64($model, $attribute)
	{
		if (!isset($settings['params'])) $settings['params'] = [];

		if ($model) {
			$img = (array)json_decode($model->$attribute);

			foreach ($img as $entry) {
				$filename = $_SERVER['DOCUMENT_ROOT'] . Yii::$app->params['updir'] . $entry;
				if (file_exists($filename) && !is_dir($filename)) unlink($filename);
			}

			$model->$attribute = NULL;
			$model->scenario = 'update';
			$model->save();
		}

		/*if (isset($settings['params']['redirect']) && $settings['params']['redirect'] === true) {
			$this->redirect($this->createUrl('/admin/' . strtolower($settings['action']) . 'edit/' . $settings['id']));
		}*/
	}

	/**
	 * загрузка файлов base64
	 *
	 * @param string - строка в кодировке base64,
	 * @param 'img' or 'doc' - тип загружаемого файла,
	 * @param object $settings ['model'] - объект модели,
	 * @param array $settings ['params'] - массив с параметрами,
	 */
	protected function kUploadBase64($base64string, $settings, $type = 'img')
	{
		$docExtensions = [
			'application/pdf'    => '.pdf',
			'application/msword' => '.doc',
			'application/rtf'    => '.rtf',
			'application/plain'  => '.txt'
		];
		$docContentTypes = array_keys($docExtensions);
		$imgExtensions = [
			'image/jpeg' => '.jpg',
			'image/png'  => '.png',
			'image/gif'  => '.gif',
			'image/svg+xml' => '.svg'
		];
		$imgContentTypes = array_keys($imgExtensions);

		$base64arr = explode(',', $base64string);
		$posBase64 = explode(';', $base64arr[0]);
		$contentType = substr($posBase64[0], 5);

		if ($type == 'img' && in_array($contentType, $imgContentTypes)) {
			$ext = $imgExtensions[$contentType];

			self::kImageDeleteBase64($settings['model'], $settings['attribute']);

			$newname = strtolower(rand() . time() . $ext);
			$newpath = Yii::getAlias('@webroot') . Yii::$app->params['updir'] . $newname;

			header('Content-Type: ' . $contentType);
			$img = base64_decode($base64arr[1]);
			$f = fopen($newpath, "w+");
			fwrite($f,$img);
			fclose($f);

			/* парсим hidden поля с высотами и ширинами тумб */
			$sizes = [];

			if (isset($settings['sizes']) && $ext != '.svg') {
				foreach ($settings['sizes'] as $size) {
					$sizes[] = [$size['w'], $size['h']];
				}
			}
			elseif($ext == '.svg')
			{
				$settings['model']->{$settings['attribute']} = $newname;
				if ($settings['model']->save())
					return true;
			}
			else{
				return false;
			}

			/* конец парсинга */
			foreach ($sizes as $size) {
				$thumbpath = 't-' . $size[0] . 'x' . $size[1] . '-' . $newname;
				$rows['thumb_' . $size[0] . 'x' . $size[1]] = $thumbpath;
				if (isset($size[2])) {
					$thumbname[] = KUseful::create_thumb($newname, $thumbpath, $size[0], $size[1], isset($settings['params']['crop']) ? $settings['params']['crop'] : true, $size[2]);
				}
				else {
					$thumbname[] = KUseful::create_thumb($newname, $thumbpath, $size[0], $size[1], isset($settings['params']['crop']) ? $settings['params']['crop'] : true);
				}
			}
			$merged = array_merge(['img' => $newname, 'thumb' => $thumbname[0]], $rows);
			$settings['model']->{$settings['attribute']} = json_encode($merged);
			$settings['model']->scenario = 'update';

			if ($settings['model']->save())
				return true;
		}
		elseif ($type == 'doc' && in_array($contentType, $docContentTypes)) {
			$ext = $docExtensions[$contentType];

			$newname = strtolower(rand() . time() . $ext);
			$newpath = $_SERVER['DOCUMENT_ROOT'] . Yii::$app->params['updir'] . $newname;

			header('Content-Type: ' . $contentType);
			$img = base64_decode($base64arr[1]);
			$f = fopen($newpath, "w+");
			fwrite($f,$img);
			fclose($f);

			if (isset($settings['doc_param'])) {
				$settings['model']->{$settings['attribute']} = $newname;
				//$settings['model']->scenario = 'aftersave';
				if ($settings['model']->save())
					return true;
				else
					return false;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
}