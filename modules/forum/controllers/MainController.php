<?php
namespace app\modules\forum\controllers;

use app\models\ForumCategories;
use app\models\ForumSections;
use app\modules\forum\components\ForumController;
use app\helpers\Debug;
use http\Encoding\Stream\Debrotli;
use Yii;

class MainController extends ForumController
{
	public function actionIndex()
	{
		//разделы
		$sections = ForumSections::find()->visibled()->order()->all();

		//собираем массив данных
		$forumData = [];
		if(!empty($sections))
		{
			foreach ($sections as $section)
			{
				//разделы доступные для всех
				if(!$section->for_admins)
				{
					//получаем все категории в этой секции
					$categories = ForumCategories::find()->where(['section_id' => $section->id])->visibled()->order()->all();
					$section->img = $section->getImg();
					$section->categories = $categories;
					$forumData[] = $section;
				}
				else
				{
					//проверяем имеет ли право пользователь видеть этот раздел
					if(Yii::$app->user->can('manageForum'))
					{
						$categories = ForumCategories::find()->where(['section_id' => $section->id])->visibled()->order()->all();
						$section->img = $section->getImg();
						$section->categories = $categories;
						$forumData[] = $section;
					}
				}
			}
		}

		$data = [
			'forumData' => $forumData,
		];

		return $this->render('index', $data);
	}

	public function actionRules()
	{
		return $this->render('rules');
	}
}