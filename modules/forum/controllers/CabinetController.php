<?php
namespace app\modules\forum\controllers;

use http\Encoding\Stream\Debrotli;
use yii\web\HttpException;
use app\models\User;
use app\modules\forum\components\ForumController;
use app\helpers\Debug;
use Yii;

class CabinetController extends ForumController
{
	public function actionIndex($id)
	{
		if(!$id) throw new HttpException(404 ,'User not found');

		$user = User::findOne($id);

		$currentUserId = Yii::$app->getUser()->identity->id;

		if(!$user) throw new HttpException(404 ,'User not found');


		$post = Yii::$app->request->post();

		if($post && $user->id === $currentUserId && !Yii::$app->getUser()->isGuest)
		{
			$user->attributes = $post['User'];
			$user->scenario = 'update';
			if($user->validate())
			{
				$user->save();
			}
		}

		$data = [
			'user' => $user,
			'currentUserId' => $currentUserId
		];

		return $this->render('cabinet', $data);
	}

	public function actionUpdate($id)
	{
		$user = User::findOne($id);

		if(!$user)
		{
			return json_encode([
				'error' => 'Такого пользователя нет в системе'
			]);
		}

		if(!Yii::$app->getUser()->isGuest && Yii::$app->getUser()->identity->id === $user->id)
		{
			$request = Yii::$app->request->post();

			if (isset($request['img'])) {
				$img = $request['img'];
				unset($request['img']);
			}

			if (isset($img)) {
				$settings['model'] = $user;
				$settings['attribute'] = 'img';
				$settings['sizes'] = [['w'=>160, 'h'=>160]]; // массив с размерами в формате ['w'=>100, 'h'=>100]

				$this->kUploadBase64($img, $settings);

				return json_encode([
					'success' => 'Аватарка сохранена'
				]);
			}
			else
			{
				return json_encode([
					'error' => 'Произошла непредвиденная ошибка при сохранении'
				]);
			}
		}
		else
		{
			return json_encode([
				'error' => 'У вас нет прав добавлять или изменять аватар для этого пользователя'
			]);
		}
	}

	public function actionRemoveImg()
	{
		$request = Yii::$app->request->post();

		$user = User::findOne($request['id']);

		if(!Yii::$app->getUser()->isGuest && Yii::$app->getUser()->identity->id === $user->id)
		{
			if (isset($request['id']) && !empty($request['attrName']) && !empty($user)) {

				$attrName = $request['attrName'];

				if (!empty($user->$attrName))

					$this->kImageDeleteBase64($user, $attrName);

				return '1';
			}
		}

		return "0";
	}

	public function actionPlayers()
	{
		if(!Yii::$app->user->can('manageForum'))
		{
			return $this->redirect('/forum');
		}

		$players = User::find()->where(['not', ['role' => 'admin']])->all();

		$data = [
			'players' => $players
		];

		return $this->render('players', $data);
	}

	public function actionBan()
	{
		$request = Yii::$app->request->post();

		if(Yii::$app->user->can('manageForum'))
		{
			$user = User::findOne($request['id']);

			if($user && $user->role !== 'moder' && $user->role !== 'admin')
			{
				$user->scenario = 'update';

				if($user->forum_banned){

					$user->forum_banned = 0;
				}
				else
				{
					$user->forum_banned = 1;
				}

				if($user->validate() && $user->save())
				{
					return true;
				}
			}
		}
	}
}