<?php

namespace app\modules\forum\controllers;

use app\models\ForumAnswers;
use app\models\ForumCategories;
use app\models\User;
use app\modules\forum\components\ForumController;
use app\helpers\Debug;
use http\Encoding\Stream\Debrotli;
use \Yii;
use app\models\ForumTopics;
use yii\data\Pagination;
use yii\web\HttpException;

class TopicsController extends ForumController
{
	public function actionIndex()
	{
		$answersLimit = 20;
		//ответы в топике
		$post = Yii::$app->request->post();
		$answerModel = new ForumAnswers();

		$answers = null;
		$request = Yii::$app->request;
		$topicAlias = $request->get('topic');
		$categoryAlias = $request->get('category');
		$page = $request->get('page');
        
		$topic = ForumTopics::find()->where(['alias' => $topicAlias])->notDeleted()->one();
		$currentCategory = ForumCategories::find()->visibled()->where(['alias' => $categoryAlias])->one();
		
		if(isset($topic))
		{
			$query = ForumAnswers::find()->where(['topic_id' => $topic->id]);
			$pages = new Pagination([
				'totalCount' => $query->count(),
				'pageSize' => $answersLimit,
				'forcePageParam' => false,
				'pageSizeParam' => false,
				'route' => 'forum/'.$currentCategory->alias.'/'.$topic->alias,
			]);

			if($page > $pages->getPageCount()) throw new HttpException(404);

			$answers = $query->offset($pages->offset)->limit($pages->limit)->notDeleted()->orderByDate()->all();

		}

		$user = Yii::$app->getUser()->identity;
	
		//последний ответ (чтобы форму с добавлением ответа выводить после последнего ответа)
		$lastAnswer = ForumAnswers::find()->where(['topic_id' => $topic->id])->orderByDateDesc()->one();

		if(isset($post['ForumAnswers']) && $user && !$user->forum_banned)
		{
			$answerModel->attributes = $post['ForumAnswers'];

			if($answerModel->validate())
			{
				$answerModel->date_create = date("Y-m-d H:i:s", time());
				$answerModel->user_id = $user->id;
				$answerModel->topic_id = $topic->id;

				if($answerModel->save())
				{


					//переиничиваем пагинацию, чтобы верно отправлять юзера после сохранения ответа на страницу с ответом
					$query = ForumAnswers::find()->where(['topic_id' => $topic->id]);
					$pages = new Pagination([
						'totalCount' => $query->count(),
						'pageSize' => $answersLimit,
						'forcePageParam' => false,
						'pageSizeParam' => false,
						'route' => 'forum/'.$currentCategory->alias.'/'.$topic->alias,
					]);

					$answers = $query->offset($pages->offset)->limit($pages->limit)->orderByDate()->all();

					if($pages->getPageCount() > 1)
					{
						return $this->redirect('/forum/'.$currentCategory->alias.'/'.$topic->alias.'/'.$pages->getPageCount());
					}
					else
					{
						return $this->redirect('/forum/'.$currentCategory->alias.'/'.$topic->alias);
					}
				}
			}
		}

		$params = [
			'topic' => $topic,
			'category' => $currentCategory,
			'answers' => $answers,
			'answerModel' => $answerModel,
			'user' => $user,
			'pages' => $pages,
			'lastAnswer' => $lastAnswer
		];

		return $this->render('topic', $params);
	}

	public function actionCreate()
	{
		//пользователь дожен быть зарегистрированным и с активным аккаунтом
		if(!Yii::$app->getUser()->isGuest && Yii::$app->getUser()->identity->activate)
		{
			$model = new ForumTopics();
			$request = Yii::$app->request;
			$categoryAlias = $request->get('category');
			$currentCategory = ForumCategories::find()->visibled()->where(['alias' => $categoryAlias])->one();
			$user = Yii::$app->getUser()->identity;

			//если разрешено всем в том числе и игрокам
			if($currentCategory->for_players && !$user->forum_banned || Yii::$app->user->can('manageForum'))
			{
				$post = Yii::$app->request->post();

				if(isset($post['ForumTopics']))
				{
					$model->attributes = $post['ForumTopics'];

					if($model->validate())
					{
						$model->date_create = date("Y-m-d H:i:s", time());
						$model->forum_category_id = $currentCategory->id;
						$model->user_id = Yii::$app->getUser()->identity->getId();

						if($model->save())
						{
							return $this->redirect('/forum/'.$currentCategory->alias);
						}
					}
				}

				$types = [
					'important' => 'important',
					'info'      => 'info',
					'closed'    => 'closed',
				];

				$params = [
					'model'    => $model,
					'category' => $currentCategory,
					'types'    => $types,
					'user'     => $user
				];

				return $this->render('topic-create', $params);
			}
		}
		else
		{
			return $this->redirect('/auth/login');
		}
	}

	public function actionUpdate($id)
	{
		if(!$id)
		{
			throw new HttpException(404 ,'User not found');
		}

		$request = Yii::$app->request;
		$categoryAlias = $request->get('category');
		$user = Yii::$app->getUser()->identity;

		if(Yii::$app->user->can('manageForum'))
		{
			$currentCategory = ForumCategories::find()->visibled()->where(['alias' => $categoryAlias])->one();
			$topic = ForumTopics::find()->where(['id' => $id])->one();

			$post = Yii::$app->request->post();

			if(isset($post['ForumTopics']))
			{
				$topic->attributes = $post['ForumTopics'];

				if($topic->save())
				{
					return $this->redirect('/forum/'.$currentCategory->alias);
				}
			}

			$categories = ForumCategories::find()->order()->visibled()->all();

			$formattedCateg = [];
			if(!empty($categories))
			{
				foreach ($categories as $category)
				{
					$formattedCateg[$category->id] = $category->title;
				}
			}

			$types = [
				'important' => 'important',
				'info'      => 'info',
				'closed'    => 'closed',
			];

			$params = [
				'model'      => $topic,
				'category'   => $currentCategory,
				'types'      => $types,
				'categories' => $formattedCateg,
				'user'       => $user
			];

			return $this->render('topic-update', $params);
		}
		else
		{
			return $this->redirect('/forum/'.$categoryAlias);
		}
	}

	public function actionDelete()
	{
		$post = Yii::$app->request->post();
		$id = $post['id'];

		if($id && Yii::$app->user->can('manageForum'))
		{
			$topic = ForumTopics::find()->where(['id' => $id])->one();
			if($topic)
			{
				$topic->deleted = 1;
				if($topic->save())
				{
					return true;
				}
			}
		}
	}

	public function actionAnswerDelete()
	{
		$post = Yii::$app->request->post();
		$id = $post['id'];

		if($id && Yii::$app->user->can('manageForum'))
		{
			$answer = ForumAnswers::findOne($id);

			if($answer)
			{
				$answer->deleted = 1;
				if($answer->save())
				{
					return true;
				}
			}
		}
	}
}