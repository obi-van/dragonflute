<?php
namespace app\modules\forum\controllers;

use app\models\ForumCategories;
use app\models\ForumTopics;
use app\modules\forum\components\ForumController;
use Yii;
use app\helpers\Debug;

class CategoriesController extends ForumController
{
	public function actionIndex()
	{
		$request = Yii::$app->request;
		$categoryAlias = $request->get('category');

		$category = ForumCategories::find()->visibled()->where(['alias' => $categoryAlias])->one();
		$user = Yii::$app->getUser()->identity;

		$topics = ForumTopics::find()->where(['forum_category_id' => $category->id])->orderByDateAndType()->notDeleted()->all();

		$params = [
			'category' => $category,
			'topics'   => $topics,
			'user'     => $user
		];

		return $this->render('category', $params);
	}
}