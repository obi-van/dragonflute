<?php

namespace app\modules\admin\models;

use app\helpers\KUseful;
use Yii;

/**
 * This is the model class for table "df_settings".
 *
 * @property int $id
 * @property string $key
 * @property int $type
 * @property string|null $value
 * @property string|null $title
 */
class Settings extends \app\models\Settings
{
	public static function createQuery($criteria)
	{
		$query = self::find();
		$query->alias('t');
		if (isset($criteria['where']))
			$query->where = $criteria['where'];
		if (isset($criteria['params']))
			$query->params = $criteria['params'];
		if (isset($criteria['offset']))
			$query->offset = $criteria['offset'];
		if (isset($criteria['limit']))
			$query->limit = $criteria['limit'];
		if (isset($criteria['orderBy']))
			$query->orderBy = $criteria['orderBy'];
		if (isset($criteria['indexBy']))
			$query->indexBy = $criteria['indexBy'];
		if (isset($criteria['join']))
			$query->join = $criteria['join'];
		if (isset($criteria['andWhere']))
			$query->andWhere($criteria['andWhere']);

		return $query;
	}


	public static function createCriteria($query)
	{
		$criteria = [];
		$criteria['where'] = $query->where;
		$criteria['params'] = $query->params;
		$criteria['offset'] = $query->offset;
		$criteria['limit'] = $query->limit;
		$criteria['orderBy'] = $query->orderBy;
		$criteria['indexBy'] = $query->indexBy;
		$criteria['join'] = $query->join;

		return $criteria;
	}

	// преобразование перед выводом в GridView
	// $items - массив моделей
	public static function dataOutput($items)
	{
		$result = [];
		foreach ($items as $item) {
			$preResult = $item->attributes;
			if ($item->key == 'partners') {
				$imgs = json_decode($item->value);
				$img = isset($imgs->thumb) ? $imgs->thumb : '';
				$preResult['value'] = '<img class="img-in-form" src="' . KUseful::imgpatch($img) . '">';
			}

			$result[] = $preResult;
		}

		return $result;
	}

	//колонки в Grid таблице
	public static function columns()
	{
		$result = [
			[
				'label' => 'id',
				'title' => 'ID',
				'class' => 'td-col td-col_id',
				'sort'  => false
			],
			[
				'label' => 'title',
				'title' => 'Название',
				'class' => 'td-col',
				'sort'  => false
			],
			[
				'label' => 'value',
				'title' => 'Значение',
				'class' => 'td-col',
				'sort'  => false
			],
		];

		return $result;
	}
}
