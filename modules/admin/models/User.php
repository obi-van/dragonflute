<?php

namespace app\modules\admin\models;

use Yii;
use app\helpers\KUseful;
use app\helpers\Debug;

/**
 * This is the model class for table "df_users".
 *
 * @property string $id
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property integer $created_at
 * @property integer $update_at
 * @property string $role
 * @property string $password_reset_token
 * @property integer $date_create
 * @property integer $date_update
 * @property integer $activate
 * @property string $key
 */
class User extends \app\models\User
{
	public $password;

    public static function createQuery($criteria)
	{
			$query = self::find();
			$query->alias('t');
			if (isset($criteria['where']))
				$query->where = $criteria['where'];
			if (isset($criteria['params']))
				$query->params = $criteria['params'];
			if (isset($criteria['offset']))
				$query->offset = $criteria['offset'];
			if (isset($criteria['limit']))
				$query->limit = $criteria['limit'];
			if (isset($criteria['orderBy']))
				$query->orderBy = $criteria['orderBy'];
			if (isset($criteria['indexBy']))
				$query->indexBy = $criteria['indexBy'];
			if (isset($criteria['with']))
				if (is_array($criteria['with']))
					foreach ($criteria['with'] as $joinWith) {
						$query->joinWith($joinWith);
					}
				else
					$query->joinWith($criteria['with']);
			if (isset($criteria['joinWith']))
				$query->joinWith = $criteria['joinWith'];
			if (isset($criteria['andWhere']))
				$query->andWhere($criteria['andWhere']);

			return $query;
		}


		public static function createCriteria($query)
		{
			$criteria = [];
			$criteria['where'] = $query->where;
			$criteria['params'] = $query->params;
			$criteria['offset'] = $query->offset;
			$criteria['limit'] = $query->limit;
			$criteria['orderBy'] = $query->orderBy;
			$criteria['indexBy'] = $query->indexBy;
			$criteria['joinWith'] = $query->joinWith;

			return $criteria;
		}

		// преобразование перед выводом в GridView
		// $items - массив моделей
		public static function dataOutput($items)
		{
			$result = [];
			foreach ($items as $item) {
				$preResult = $item->attributes;
				$result[] = $preResult;
			}

			return $result;
		}

		//колонки в Grid таблице
		public static function columns()
		{
			$result = [
				[
					'label' => 'id',
					'title' => 'ID',
					'class' => 'td-col td-col_id'
				],
				[
					'label' => 'email',
					'title' => 'E-mail',
					'class' => 'td-col'
				],
				[
					'label' => 'role',
					'title' => 'Роль',
					'class' => 'td-col'
				],
			];

			return $result;
	}

	public function setPassword($password)
	{
		$password = Yii::$app->security->generatePasswordHash($password);
		return $password;
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
		return $password_reset_token;
	}
}
