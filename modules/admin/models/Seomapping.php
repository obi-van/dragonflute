<?php

namespace app\modules\admin\models;

use Yii;
use app\helpers\KUseful;

/**
 * This is the model class for table "df_seomapping".
 *
 * @property string $id
 * @property string $uri
 * @property string $title
 * @property string $keywords
 * @property string $description
 */
class Seomapping extends \app\models\Seomapping
{
   public static function createQuery($criteria)
	{
			$query = self::find();
			$query->alias('t');
			if (isset($criteria['where']))
				$query->where = $criteria['where'];
			if (isset($criteria['params']))
				$query->params = $criteria['params'];
			if (isset($criteria['offset']))
				$query->offset = $criteria['offset'];
			if (isset($criteria['limit']))
				$query->limit = $criteria['limit'];
			if (isset($criteria['orderBy']))
				$query->orderBy = $criteria['orderBy'];
			if (isset($criteria['indexBy']))
				$query->indexBy = $criteria['indexBy'];
			if (isset($criteria['with']))
				if (is_array($criteria['with']))
					foreach ($criteria['with'] as $joinWith) {
						$query->joinWith($joinWith);
					}
				else
					$query->joinWith($criteria['with']);
			if (isset($criteria['joinWith']))
				$query->joinWith = $criteria['joinWith'];
			if (isset($criteria['andWhere']))
				$query->andWhere($criteria['andWhere']);

			return $query;
		}


		public static function createCriteria($query)
		{
			$criteria = [];
			$criteria['where'] = $query->where;
			$criteria['params'] = $query->params;
			$criteria['offset'] = $query->offset;
			$criteria['limit'] = $query->limit;
			$criteria['orderBy'] = $query->orderBy;
			$criteria['indexBy'] = $query->indexBy;
			$criteria['joinWith'] = $query->joinWith;

			return $criteria;
		}

		// преобразование перед выводом в GridView
		// $items - массив моделей
		public static function dataOutput($items)
		{
			$result = [];
			foreach ($items as $item) {
				$preResult = $item->attributes;

				$result[] = $preResult;
			}

			return $result;
		}

		//колонки в Grid таблице
		public static function columns()
		{
			$result = [
				[
					'label' => 'id',
					'title' => 'ID',
					'class' => 'td-col td-col_id'
				],
				[
					'label' => 'uri',
					'title' => 'Uri',
					'class' => 'td-col'
				],
				[
					'label' => 'title',
					'title' => 'Title',
					'class' => 'td-col'
				],
				[
					'label' => 'keywords',
					'title' => 'Keywords',
					'class' => 'td-col'
				],
				[
					'label' => 'description',
					'title' => 'Description',
					'class' => 'td-col'
				],
			];

			return $result;
	}
}
