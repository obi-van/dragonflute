<?php

use app\helpers\KAForm;
use app\helpers\Debug;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ForumSections */

?>

    <?= KAForm::textInput($model, 'title', 'Title') ?>

    <?= KAForm::checkbox($model, 'for_admins', 'Только для администраторов и модераторов', 2) ?>