<?php

use app\widgets\GridViewTemplate;
use app\widgets\PaginationTemplate;

echo GridViewTemplate::widget([
'params' => [
'removing' => true,
'editing' => true
]]);
echo PaginationTemplate::widget(); ?>

<div id="forumSections">
	<h3 class="title-fixed-top">
		<span class="pull-left">Разделы форума</span>
		<button class="btn btn-primary btn-sm pull-right" @click="showCreateForm()"
		        style="color: white"><i class="glyphicon glyphicon-plus"></i>&nbsp;Добавить
		                                                                     позицию
		</button>
		<button class="btn-save-confirm btn btn-danger btn-sm pull-right" @click="removeChecked()">
			<i class="glyphicon glyphicon-remove"></i>&nbsp;Удалить выделенные
		</button>
		<button v-if="changed == true" class="btn-save-confirm btn btn-success btn-sm pull-right" @click="saveParams()">
			<i class="glyphicon glyphicon-remove"></i>&nbsp;Сохранить
		</button>
		<pagination
				:current="currentPage"
				:total="Number(countItems)"
				:per-page="Number(pageLimit)"
				@page-changed="changePage"
		></pagination>
	</h3>
	<div class="panel-body">
		<div class="row">
			<div class="col-xl-8">
				<div class="form-group">
					<input name="query" v-model="searchQuery" class="form-control input-sm"
					       placeholder="Поиск">
									</div>
			</div>
			<div class="col-xl-4">
				<div class="form-group row">
					<label for="limit" class="col-md-8 col-form-label text-right">Выводить по:</label>
					<div class="col-md-4">
						<select class="form-control" v-model="pageLimit" id="limit">
							<option>8</option>
							<option>20</option>
							<option>40</option>
							<option>60</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	<grid-view
			:data="gridData"
			:columns="gridColumns"
			:filter-key="searchQuery"
			:search-columns="searchColumns"
			:model-name="modelName"
			:table-name="tableName"
			:limit="pageLimit"
			:page-number="currentPage"
			:count-items="countItems"
			:checked-for-remove="checkedForRemove"
			:refreshing="refreshing"
			:controller-id="controllerId"
			:changed="changed"
			@changing="changing"
			@change-count-items="changeCountItems"
			@show-update-form="showUpdateForm"
	>
	</grid-view>
	<simplert :use-radius="true"
	          :use-icon="true"
	          ref="simplertnotmodal">
	</simplert>
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog" data-last=""
	     aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h5 class="modal-title">{{modalTitle}}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть без сохранения
					</button>
					<button type="button" class="btn btn-primary" @click="saveChanges()">Сохранить изменения
					</button>
				</div>
			</div>
		</div>
		<simplert :use-radius="true"
		          :use-icon="true"
		          ref="simplert">
		</simplert>
	</div>
</div>

<script>
	Data = <?=  json_encode($models)?>;
	Columns = <?=  json_encode($columns)?>;
	Count = <?=  $count ?>;
	Checked = [];

	var forumSections = new Vue({
		el: '#forumSections',
		data: {
			searchQuery: "<?= $searchQuery ?>",
			searchColumns: [], // здесь добавь колонки, по которым можно искать позиции - ['title', 'description']
			gridColumns: Columns,
			gridData: Data,
			modelName: 'forumsections',
			tableName: 'forumSections',
			formId: 'createForm',
			controllerId: "<?= Yii::$app->controller->id ?>",
			refreshing: false,
			pageLimit: "<?= $limit ?>",
			currentPage: 1,
			countItems: Count,
			checkedForRemove: Checked,
			modalTitle: "Форма редактирования",
			changed: false
		},
		watch: {
			pageLimit: function (val) {
				this.currentPage = 1
			}
		},
		methods: {
			changePage: function (page) {
				this.countItems = Count;
				this.currentPage = page;
			},
			removeChecked: function () {
				this.checkedForRemove = Checked; // после этого сработает метод удаления из компонента GridView
			},
			changeCountItems: function (count) {
				this.countItems = count;
			},
			refreshData: function () {
				if (this.refreshing === false)
					this.refreshing = true;
				else
					this.refreshing = false;
			},
			changing: function (data, visibleItems) {
				this.changed = true;
				this.changedData = data;
				this.visibleItems = visibleItems;
			},
			showCreateForm: function () {
				var _this = this;
				this.modalTitle = "Форма создания";
				this.formId = "createForm";

				var params = {};
				params.onlyShow = true;
				var csrfParam = $('meta[name="csrf-param"]').attr("content");
				var csrfToken = $('meta[name="csrf-token"]').attr("content");
				params[csrfParam] = csrfToken;

				$("#editModal").modal("show");
				if ($("#editModal").data('last') !== "create")
					$.ajax({
						url: window.location.protocol + '//' + window.location.host + '/admin/forum-sections/create',
						type: "POST",
						data: params,
						success: function (response) { // Success.
							$('.modal-body').html(response);
							$("#editModal").data('last', 'create');
						},
						error: function (response) { // Error.
							_this.errorMsg("Непредвиденная ошибка. Попробуйте еще раз, либо свяжитесь с разработчиками.");
						}
					});
			},
			showUpdateForm: function (params) {
				var _this = this;
				this.modalTitle = "Форма редактирования";
				this.formId = "updateForm";

				params.onlyShow = true;
				var csrfParam = $('meta[name="csrf-param"]').attr("content");
				var csrfToken = $('meta[name="csrf-token"]').attr("content");
				params[csrfParam] = csrfToken;

				$("#editModal").modal("show");
				$.ajax({
					url: window.location.protocol + '//' + window.location.host + '/admin/forum-sections/update/' + params.id,
					type: "POST",
					data: params,
					success: function (response) { // Success.
						$('.modal-body').html(response);
					},
					error: function (response) { // Error.
						_this.errorMsg("Непредвиденная ошибка. Попробуйте еще раз, либо свяжитесь с разработчиками.");
					}
				});
			},
			saveChanges: function () {
				var _this = this;
				var fields = $("#" + this.formId).serializeArray();
				var params = {};
				fields.forEach(function(item, i, arr) {
					params[item.name] = item.value;
				});
								var csrfParam = $('meta[name="csrf-param"]').attr("content");
				var csrfToken = $('meta[name="csrf-token"]').attr("content");
				params[csrfParam] = csrfToken;
				if (this.formId === "updateForm") {
					$.ajax({
						url: window.location.protocol + '//' + window.location.host + '/admin/forum-sections/update/' + params.id,
						type: "POST",
						data: params,
						success: function (response) { // Success.
							response = JSON.parse(response);
							if (response.success !== undefined && response.success !== null) {
								_this.successMsg(response.success, function() {$("#editModal").modal("hide")});
								_this.refreshData();
							}
							else if (response.error !== undefined && response.error !== null)
								_this.errorMsg(response.error);
							else
								_this.errorMsg("Непредвиденная ошибка. Попробуйте еще раз.");
						},
						error: function (response) { // Error.
							_this.errorMsg("Непредвиденная ошибка. Попробуйте еще раз, либо свяжитесь с разработчиками.");
						}
					});
				}
				else if (this.formId === "createForm") {
					$.ajax({
						url: window.location.protocol + '//' + window.location.host + '/admin/forum-sections/create',
						type: "POST",
						data: params,
						success: function (response) { // Success.
							response = JSON.parse(response);
							if (response.success !== undefined && response.success !== null) {
								_this.successMsg(response.success, function() {
									$("#editModal").data('last', 'createisdone');
									$("#editModal").modal("hide");
								});
								_this.refreshData();
							}
							else if (response.error !== undefined && response.error !== null)
								_this.errorMsg(response.error);
							else
								_this.errorMsg("Непредвиденная ошибка. Попробуйте еще раз.");
						},
						error: function (response) { // Error.
							_this.errorMsg("Непредвиденная ошибка. Попробуйте еще раз, либо свяжитесь с разработчиками.");
						}
					});
				}
			},
			saveParams: function () {
				var _this = this;
				var params = {};
				params.data = _this.changedData;
				params.visible = _this.visibleItems;
				var csrfParam = $('meta[name="csrf-param"]').attr("content");
				var csrfToken = $('meta[name="csrf-token"]').attr("content");
				params[csrfParam] = csrfToken;
				$.ajax({
					url: window.location.protocol + '//' + window.location.host + '/admin/forum-sections/savechanges',
					type: "POST",
					data: params,
					success: function (response) { // Success.
						response = JSON.parse(response);
						if (response.success !== undefined && response.success !== null) {
							_this.successMsg(response.success, '', 'simplertnotmodal');
							_this.refreshData();
							_this.changed = false;
						}
						else if (response.error !== undefined && response.error !== null)
							_this.errorMsg(response.error, '', '', 'simplertnotmodal');
						else
							_this.errorMsg("Непредвиденная ошибка. Попробуйте еще раз.", '', '', 'simplertnotmodal');
					},
					error: function (response) { // Error.
						_this.errorMsg("Непредвиденная ошибка. Попробуйте еще раз, либо свяжитесь с разработчиками.", '', '', 'simplertnotmodal');
					}
				});
			},
			successMsg: function ($msg, callback, simplertname = 'simplert') {
				var obj = {
					message: $msg,
					type: 'success',
					customCloseBtnText: 'OK'
				};
				if (callback !== undefined && typeof callback === "function")
					obj['onClose'] = callback;
				this.$refs[simplertname].openSimplert(obj);
			},
			errorMsg: function ($msg, $title, callback, simplertname = 'simplert') {
				var obj = {
					title: $title,
					message: $msg,
					type: 'error',
					customCloseBtnText: 'OK'
				};
				if (callback !== undefined && typeof callback === "function")
					obj['onClose'] = callback;
				this.$refs[simplertname].openSimplert(obj);
			}
		},
		created: function () {

		}
	})

</script>