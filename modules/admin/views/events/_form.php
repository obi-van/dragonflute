<?php

use app\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Events */

?>
    <?= KAForm::img($model, 'img', 'Изображение 600x644px') ?>

    <?= KAForm::textInput($model, 'title', 'Заголовок') ?>

    <?= KAForm::textarea($model, 'description', 'Краткое описание') ?>

    <?= KAForm::textInput($model, 'link', 'Ссылка') ?>

