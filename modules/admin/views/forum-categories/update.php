<?php

use yii\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ForumCategories */
?>

<form id="updateForm">
	<input type="hidden" name="id" value="<?=  $model->id ?>">

	<?=  $this->render('_form', [
		'model' => $model,
	]) ?>

</form>
