<?php

use app\helpers\KAForm;
use app\models\ForumSections;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ForumCategories */

    $sections = ForumSections::find()->visibled()->order()->all();

?>

    <?= KAForm::textInput($model, 'title', 'Title') ?>

    <?= KAForm::textarea($model, 'description', 'Description') ?>

    <?= KAForm::select($model, 'section_id', 'Раздел', $sections, 'title', 10, $params = []) ?>

    <?= KAForm::checkbox($model, 'for_players', 'Разрешить игрокам добавлять топики', 2) ?>
