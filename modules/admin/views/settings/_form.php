<?php

use app\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Settings */

?>

<?= KAForm::textInput($model, 'title', 'Название') ?>
<? if ($model->type == 6): ?>
	<?= KAForm::img($model, 'value', 'Картинка', 'img') ?>
<? else: ?>
	<?= KAForm::textarea($model, 'value', 'Значение') ?>
<? endif ?>