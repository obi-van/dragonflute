<?php

use app\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Textbloks */

?>
    <?= KAForm::textInput($model, 'title', 'Заголовок') ?>

    <?= KAForm::wysiwyg($model, 'content', 'Текст') ?>

