<?php

use yii\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Textbloks */

?>

<form id="createForm">

	<?=  $this->render('_form', [
		'model' => $model,
	]) ?>

</form>