<?

use app\modules\admin\components\AdminController;
use yii\helpers\Html;

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Администрирование</title>
		<meta charset="utf-8">
		<meta name="description" content="Администрирование">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<?= Html::csrfMetaTags() ?>
		<link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@web') ?>/imgs/apple-touch-icon.png" />
		<link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@web') ?>/imgs/favicon-32x32.png" />
		<link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@web') ?>/imgs/favicon-16x16.png" />
		<!-- fonts -->
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;amp;subset=cyrillic"
			  rel="stylesheet">
		<!-- Bootstrap 4 cdn -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/back.css?<?= AdminController::VERSION ?>">
		<link rel="stylesheet" href="<?= Yii::getAlias('@web') ?>/css/datepicker.min.css">
		<script src="https://use.fontawesome.com/e08a08be9a.js"></script>
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<? if (YII_ENV_DEV): ?>
			<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
		<? else: ?>
			<script src="https://cdn.jsdelivr.net/npm/vue"></script>
		<? endif ?>
		<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
		<!-- диалоговые окна vue -->
		<script src="https://unpkg.com/vue2-simplert@0.5.1/dist/simplert.bundle.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/locale/ru.js"></script>
		<script src="<?= Yii::getAlias('@web') ?>/js/backside/plugins/pikaday.js"></script>
		<script src="<?= Yii::getAlias('@web') ?>/js/backside/plugins/datepicker.min.js"></script>
		<script src="<?= Yii::getAlias('@web') ?>/js/backside/plugins/ckeditor/ckeditor.js"></script>
		<script src="<?= Yii::getAlias('@web') ?>/js/backside/plugins/ckfinder/ckfinder.js"></script>
		<script src="<?= Yii::getAlias('@web') ?>/js/backside/plugins/ckeditor/adapters/jquery.js"></script>
		<script src="<?= Yii::getAlias('@web') ?>/js/backside/components.vue.js?<?= AdminController::VERSION ?>"></script>
	</head>
	<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color: #e3f2fd;">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<a class="navbar-brand" href="<?= Yii::getAlias('@web') ?>/" target="_blank">
			<img src="<?= Yii::getAlias('@web') ?>/imgs/white_dragon_logo.svg">
		</a>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
<!--				<li class="nav-item --><?//= Yii::$app->controller->id == 'news' ? 'active' : '' ?><!--">-->
<!--					<a class="nav-link" href="--><?//= Yii::getAlias('@web') ?><!--/admin/news">Новости</a>-->
<!--				</li>-->
<!--				<li class="nav-item --><?//= Yii::$app->controller->id == 'projects' ? 'active' : '' ?><!--">-->
<!--					<a class="nav-link" href="--><?//= Yii::getAlias('@web') ?><!--/admin/projects">Проекты</a>-->
<!--				</li>-->
<!--				<li class="nav-item dropdown">-->
<!--					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"-->
<!--					   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--						Главная страница-->
<!--					</a>-->
<!--					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">-->
<!--						<a class="dropdown-item" href="--><?//= Yii::getAlias('@web') ?><!--/admin/main-slider">Баннер на главной</a>-->
<!--						<a class="dropdown-item" href="--><?//= Yii::getAlias('@web') ?><!--/admin/safety">Системы безопасности</a>-->
<!--						<a class="dropdown-item" href="--><?//= Yii::getAlias('@web') ?><!--/admin/cooperation">Форма сотрудничества</a>-->
<!--					</div>-->
<!--				</li>-->
				<li class="nav-item <?= Yii::$app->controller->id == 'bosses' ? 'active' : '' ?>">
					<a class="nav-link" href="<?= Yii::getAlias('@web') ?>/admin/bosses">Raid Bosses</a>
				</li>
				<li class="nav-item <?= Yii::$app->controller->id == 'events' ? 'active' : '' ?>">
					<a class="nav-link" href="<?= Yii::getAlias('@web') ?>/admin/events">Events</a>
				</li>
				<li class="nav-item <?= Yii::$app->controller->id == 'news' ? 'active' : '' ?>">
					<a class="nav-link" href="<?= Yii::getAlias('@web') ?>/admin/news">Новости</a>
				</li>
				<li class="nav-item <?= Yii::$app->controller->id == 'users' ? 'active' : '' ?>">
					<a class="nav-link" href="<?= Yii::getAlias('@web') ?>/admin/users">Пользователи</a>
				</li>
				<li class="nav-item <?= Yii::$app->controller->id == 'textbloks' ? 'active' : '' ?>">
					<a class="nav-link" href="<?= Yii::getAlias('@web') ?>/admin/textbloks">Текстовые блоки</a>
				</li>
				<li class="nav-item dropdown mr-2">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Форум
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="<?= Yii::getAlias('@web') ?>/admin/forum-sections">Разделы</a>
						<a class="dropdown-item" href="<?= Yii::getAlias('@web') ?>/admin/forum-categories">Категории</a>
					</div>
				</li>
			</ul>
			<ul class="nav navbar-nav pull-right">
				<li class="nav-item dropdown mr-2">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Настройки
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="<?= Yii::getAlias('@web') ?>/admin/settings">Настройки сайта</a>
						<a class="dropdown-item" href="<?= Yii::getAlias('@web') ?>/admin/seomapping">Настройки SEO</a>
					</div>
				</li>
				<li class="d-flex align-items-center">
					<a href="<?= Yii::getAlias('@web') ?>/auth/logout" class="text-white">Выход</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="container-fluid main-container">
		<?= $content ?>
	</div>
	</body>
	<script src="<?= Yii::getAlias('@web') ?>/js/backside/backside.js?<?= AdminController::VERSION ?>"></script>
</html>