<?php

use app\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */

?>
    <?= KAForm::checkbox($model, 'top', 'Зафиксировать в топе', 2) ?>

    <?= KAForm::dateInput($model, 'date', 'Дата создания') ?>

    <?= KAForm::textInput($model, 'seotitle', '&lt;title&gt;') ?>

    <?= KAForm::textInput($model, 'seodescription', '&lt;meta name="description"&gt;') ?>

    <?= KAForm::textInput($model, 'seokeywords', '&lt;meta name="keywords"&gt;') ?>

    <?= KAForm::img($model, 'img', 'Изображение 368x196px') ?>

    <?= KAForm::textInput($model, 'short_title', 'Short Title') ?>

    <?= KAForm::textInput($model, 'short_description', 'Short Description') ?>

    <?= KAForm::textInput($model, 'title', 'Title') ?>

    <?= KAForm::wysiwyg($model, 'content', 'Контент') ?>