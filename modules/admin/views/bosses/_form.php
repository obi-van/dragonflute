<?php

use app\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Bosses */

?>

    <?= KAForm::img($model, 'img', 'Изображение 300x300px') ?>

    <?= KAForm::textInput($model, 'name', 'Наименование') ?>

    <?= KAForm::textInput($model, 'deaths', 'Количество смертей') ?>

