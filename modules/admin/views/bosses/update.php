<?php

use yii\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Bosses */
?>

<form id="updateForm">
	<input type="hidden" name="id" value="<?=  $model->id ?>">

	<?=  $this->render('_form', [
		'model' => $model,
	]) ?>

</form>
