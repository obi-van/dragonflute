<?php

use yii\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

?>

<form id="createForm">

	<?=  $this->render('_form', [
		'model' => $model,
		'roles' => $roles,
	]) ?>

</form>