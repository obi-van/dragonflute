<?php

use yii\helpers\KAForm;
use app\helpers\Debug;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */
?>

<form id="updateForm">
	<input type="hidden" name="id" value="<?=  $model->id ?>">

	<?=  $this->render('_form', [
		'model'       => $model,
		'roles'       => $roles,
		'currentRole' => $currentRole
	]) ?>

</form>
