<?php

use app\helpers\KAForm;
use app\helpers\Debug;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */
    if (Yii::$app->controller->action->id == 'update')
        $param = [
            'disabled' => 'disabled'
        ];
    else
        $param = [];

?>

    <?= KAForm::textInput($model, 'account', 'Аккаунт <br><small>используется для входа в игру</small>', 10, $param) ?>

    <?= KAForm::textInput($model, 'email', 'E-mail <br><small>используется в качестве логина</small>', 10, $param) ?>

    <?= KAForm::passInput($model, 'password', 'Пароль <br><small>если не заполнять - пароль останется прежним</small>')?>

	<div class="form-group row">
		<label for="role" class="col-xl-2 col-sm-2 col-form-label text-right">Role</label>
		<div class="col-xl-10">

			<select autocomplete="off" class="form-control select" name="role" id="role">
				<option value="">--</option>
				<? foreach ($roles as $role): ?>
					<? $selected = $role->name == $currentRole ?? '' ? 'selected="selected"' : NULL; ?>
					<option value="<?= $role->name ?>" <?= $selected ?>><?= $role->name ?></option>
				<? endforeach ?>
			</select>
		</div>
	</div>

