<?php

use app\helpers\KAForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Seomapping */

?>

<?= KAForm::textInput($model, 'uri', 'URI') ?>

<?= KAForm::textInput($model, 'title', '&lt;title&gt;') ?>

<?= KAForm::textInput($model, 'keywords', '&lt;meta name="keywords"&gt;') ?>

<?= KAForm::textarea($model, 'description', '&lt;meta name="description"&gt;') ?>