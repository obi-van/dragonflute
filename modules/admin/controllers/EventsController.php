<?php

namespace app\modules\admin\controllers;

use app\helpers\KDebug;
use app\helpers\KUseful;
use Yii;
use app\modules\admin\models\Events;
use app\modules\admin\components\AdminController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends AdminController
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
		$_SESSION['events']['criteria']['orderBy'] = ['id' => SORT_DESC];
		if (!isset($_SESSION['events']['criteria']['limit']))
			$_SESSION['events']['criteria']['limit'] = AdminController::PAGER_LIMIT;
		$query = Events::createQuery($_SESSION['events']['criteria']);
		if (isset($_SESSION['events']) && !empty($_SESSION['events']['searchQuery']))
			$searchQuery = $_SESSION['events']['searchQuery'];
		else
			$searchQuery = '';
		$models = $query->order()->all();
		$count = $query->count();

		return $this->render('index', [
			'models'  => Events::dataOutput($models),
			'count'   => $count,
			'columns' => Events::columns(),
			'searchQuery' => $searchQuery,
			'limit'   => $_SESSION['events']['criteria']['limit']
		]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model = new Events();
		$request = Yii::$app->request->post();
		// если есть картинка, то готовимся к ее загрузке
		if (isset($request['img'])) {
			$img = $request['img'];
			unset($request['img']);
		}
		$model->attributes = $request;
		if (!isset($request['onlyShow']) || $request['onlyShow'] == 'false')
			if (isset($model->visible) && in_array($model->visible, ['on', 1]))
				$model->visible = 1;
			else
				$model->visible = 0;
		else
			$model->visible = 1;

		// если в запросе пришел только параметр "onlyShow",
		// значит нужно просто отобразить форму
		if (isset($request['onlyShow']) && $request['onlyShow'] == 'true') {
			return $this->renderPartial('create', [
				'model' => $model,
			]);
		}
		// если пришли данные с формы, мы загрузили их в модель и она сохранилась - отдаем "success"
		else if ($model->save()) {
			// загрузка картинки
			if (isset($img)) {
				$settings['model'] = $model;
				$settings['attribute'] = 'img';
				$settings['sizes'] = [['w'=>600, 'h'=>644]]; // массив с размерами в формате ['w'=>100, 'h'=>100]
				$this->kUploadBase64($img, $settings);
			}
			return json_encode([
				'success' => 'Сохранено'
			]);
		}
		// если позицию не удалось сохранить, выводим сообщение с ошибками
		else {
			if (!empty($model->getErrors()))
			return json_encode([
				'error' => KUseful::errorsToString($model->getErrors())
			]);
		else
			return json_encode([
				'error' => 'Произошла непредвиденная ошибка при сохранении'
			]);
		}
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		$request = Yii::$app->request->post();
	// если есть картинка, то готовимся к ее загрузке
		if (isset($request['img'])) {
			$img = $request['img'];
			unset($request['img']);
		}

		$model->attributes = $request;
        if (!isset($request['onlyShow']) || $request['onlyShow'] == 'false')
			if (isset($request['visible']) && in_array($request['visible'], ['on', 1]))
			{
				$model->visible = 1;
			}

		// если в запросе пришел только параметр "onlyShow",
		// значит нужно просто отобразить форму
		if (isset($request['onlyShow']) && $request['onlyShow'] == 'true') {
			return $this->renderPartial('update', [
				'model' => $model,
			]);
		}
		// если пришли данные с формы, мы загрузили их в модель и она сохранилась - отдаем "success"
		else if ($model->save()) {
			// загрузка картинки
			if (isset($img)) {
				$settings['model'] = $model;
				$settings['attribute'] = 'img';
				$settings['sizes'] = [['w'=>600, 'h'=>644]]; // массив с размерами в формате ['w'=>100, 'h'=>100]
				$this->kUploadBase64($img, $settings);
			}
			return json_encode([
				'success' => 'Сохранено'
			]);
		}
		// если позицию не удалось сохранить, выводим сообщение с ошибками
		else {
			if (!empty($model->getErrors()))
				return json_encode([
					'error' => KUseful::errorsToString($model->getErrors())
				]);
			else
				return json_encode([
					'error' => 'Произошла непредвиденная ошибка при сохранении'
				]);
		}
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if ($this->findModel($id)->delete())
			return "1";
		else
			return "0";
    }

	public function actionRemoveitems()
	{
		$request = Yii::$app->request->post();
		if (isset($request['ids']) && is_array($request['ids'])) {
			foreach ($request['ids'] as $id) {
				$this->findModel($id)->delete();
			}
			return "1";
		}
		else {
			return "0";
		}
	}

	
	public function actionRemoveImg()
	{

		$request = Yii::$app->request->post();

		if (isset($request['id']) && !empty($request['attrName']) && !empty($this->findModel($request['id']))) {

			$model = $this->findModel($request['id']);

			$attrName = $request['attrName'];

			if (!empty($model->$attrName))

				$this->kImageDeleteBase64($model, $attrName);

			return '1';
		}

		return "0";
	}

	
	/*
	* С помощью этого метода обновляем представление таблицы.
	* На входе может быть пусто, либо передай параметры для изменения таблицы (колонок).
	* На выход обязательно отдать 'items' и 'count'.
	* */
	public function actionGetitemsbycriteria()
	{
		$request = Yii::$app->request->post();

		$query = Events::createQuery($_SESSION['events']['criteria']);
		$items = $query->all();
		$count = $query->count();

		$result['items'] = Events::dataOutput($items);
		$result['count'] = $count;

		return json_encode($result);
	}

	/*
	* В этот метод передаем то, что нужно прибавить к существующей критерии,
	* измени его в соответствии с требованиями, если это необходимо
	* */
	public function actionChangecriteria()
	{
		$request = Yii::$app->request->post();
		$query = Events::createQuery($_SESSION['events']['criteria']);

		if (!isset($request['method']) || !isset($request['params']))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);

		$params = json_decode($request['params']);

		// limit, offset используются при пагинации
		if ($request['method'] == 'limit') {
			$query->limit = $params;
			$query->offset = -1;
		}
		if ($request['method'] == 'offset') {
			$query->offset = $params;
		}
		if ($request['method'] == 'orderBy') {
		if (!isset($params->field) || !isset($params->trend))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);
		if ($params->trend == 'DESC')
			$trend = SORT_DESC;
		else
			$trend = SORT_ASC;
			$query->orderBy = [
				$params->field => $trend
			];
		}
		// если нужно добавить еще условия запроса (например при добавлении фильтра), описываем всё здесь
		if ($request['method'] == 'where') {
			if (!isset($params->field) || !isset($params->value))
				return json_encode([
					'error' => 'Некорректно указаны параметры'
				]);
			$query->where = [$params->field => $params->value];
		}

		$_SESSION['events']['criteria'] = Events::createCriteria($query);
		if ($request['method'] == 'search') {
		if (!isset($params->searchColumns) || !isset($params->searchQuery))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);
			$_SESSION['events']['searchQuery'] = $params->searchQuery;
			$whereArr = ['OR'];
			foreach ($params->searchColumns as $column) {
				array_push($whereArr, ['like', $column, $params->searchQuery]);
			}
			$_SESSION['events']['criteria']['where'] = $whereArr;
			$_SESSION['events']['criteria']['offset'] = -1;
		}

		return json_encode([
			'success' => 'Критерий запроса обновлен'
		]);
	}

	// сохраняет изменения в полях "visible" и "order"
	public function actionSavechanges()
	{
		$request = Yii::$app->request->post();

		if (isset($request['data'])) {
	    	if (!isset($request['visible']))
			    $request['visible'] = [];
			foreach ($request['data'] as $item) {
				$model = $this->findModel($item['id']);
				$model->order = $item['order'];
				if (in_array($item['id'], $request['visible'])) {
					$model->visible = 1;
				} else {
					$model->visible = 0;
				}
				if (!$model->save() && !empty($model->getErrors()))
				{
					return json_encode([
						'error' => KUseful::errorsToString($model->getErrors())
					]);
				}
				elseif (!$model->save()) {
					return json_encode([
						'error' => 'Ошибка при сохранении элемента с id = '.$item['id']
					]);
				}
			}
		}
		else {
			return json_encode([
				'error' => 'недостаточно данных для выполнения данного действия'
			]);
		}

		return json_encode([
			'success' => 'Сохранено'
		]);
	}


    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
