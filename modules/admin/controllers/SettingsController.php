<?php

namespace app\modules\admin\controllers;

use app\helpers\KUseful;
use app\modules\admin\components\AdminController;
use Yii;
use app\modules\admin\models\Settings;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\Debug;
use yii\filters\AccessControl;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
	        'access' => [
		        'class' => AccessControl::className(),
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['admin'],
			        ],
		        ],
	        ],
        ];
    }

	public function actionIndex()
	{
		$_SESSION['settings']['criteria']['orderBy'] = ['id' => SORT_ASC];
		if (!isset($_SESSION['items']['criteria']['limit']))
			$_SESSION['items']['criteria']['limit'] = AdminController::PAGER_LIMIT;
		$query = Settings::createQuery($_SESSION['settings']['criteria']);
		$models = $query->all();
		$count = $query->count();

		return $this->render('index', [
			'models'  => Settings::dataOutput($models),
			'count'   => $count,
			'columns' => Settings::columns(),
			'limit'   => $_SESSION['items']['criteria']['limit'],
		]);
	}

	/**
	 * Finds the Settings model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Settings the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Settings::findOne($id)) !== null) {
			return $model;
		}
		else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionSavechanges()
	{
		$request = Yii::$app->request->post();

		if (isset($request['data'])) {
			foreach ($request['data'] as $item) {
				$model = $this->findModel($item['id']);
				//из-за особенности таблицы true и false записывается как строка и vue.js всегда рисует checkbox
				if($item['type'] === '5'){
					if($item['value'] === 'true')
						$model->value = $item['value'];
					else
						$model->value = '';
				}
				else
					$model->value = $item['value'];

				if (!$model->save() && !empty($model->getErrors()))
				{
					return json_encode([
						'error' => KUseful::errorsToString($model->getErrors())
					]);
				}
				elseif (!$model->save()) {
					return json_encode([
						'error' => 'Ошибка при сохранении элемента с id = '.$item['id']
					]);
				}
			}
		}
		else {
			return json_encode([
				'error' => 'Недостаточно данных для выполнения данного действия'
			]);
		}

		return json_encode([
			'success' => 'Сохранено'
		]);
	}

	/*
	* С помощью этого метода обновляем представление таблицы.
	* На входе может быть пусто, либо передай параметры для изменения таблицы (колонок).
	* На выход обязательно отдать 'items' и 'count'.
	* */
	public function actionGetitemsbycriteria()
	{
		$request = Yii::$app->request->post();

		$query = Settings::createQuery($_SESSION['settings']['criteria']);
		$items = $query->all();
		$count = $query->count();

		$result['items'] = Settings::dataOutput($items);
		$result['count'] = $count;

		return json_encode($result);
	}
}
