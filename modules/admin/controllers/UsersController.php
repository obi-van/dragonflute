<?php

namespace app\modules\admin\controllers;

use app\components\DragonApi;
use app\helpers\Debug;
use app\helpers\KUseful;
use Yii;
use app\modules\admin\models\User;
use app\modules\admin\components\AdminController;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends AdminController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
		$_SESSION['users']['criteria']['orderBy'] = ['id' => SORT_DESC];
		if (!isset($_SESSION['users']['criteria']['limit']))
			$_SESSION['users']['criteria']['limit'] = AdminController::PAGER_LIMIT;
		$query = User::createQuery($_SESSION['users']['criteria']);
		if (isset($_SESSION['users']) && !empty($_SESSION['users']['searchQuery']))
			$searchQuery = $_SESSION['users']['searchQuery'];
		else
			$searchQuery = '';
		$models = $query->all();
		$count = $query->count();

		return $this->render('index', [
			'models'  => User::dataOutput($models),
			'count'   => $count,
			'columns' => User::columns(),
			'searchQuery' => $searchQuery,
			'limit'   => $_SESSION['users']['criteria']['limit']
		]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model = new User();
		$request = Yii::$app->request->post();
		$model->attributes = $request;
	    $model->password_hash = $model->setPassword($model->password);
	    $model->date_create = strtotime(date("Y-m-d h:i:s", time()));
	    $model->date_update = strtotime(date("Y-m-d h:i:s", time()));
	    $model->activate = 1;
	    if(empty($model->role))
	    {
		    $model->role = NULL;
	    }

	    //роли
	    $roles = Yii::$app->authManager->getRoles();

		// если в запросе пришел только параметр "onlyShow",
		// значит нужно просто отобразить форму
		if (isset($request['onlyShow']) && $request['onlyShow'] == 'true') {
			return $this->renderPartial('create', [
				'model'       => $model,
				'roles'       => $roles,
			]);
		}
		// если пришли данные с формы, мы загрузили их в модель и она сохранилась - отдаем "success"
		else if ($model->validate()) {
			//чекнув БД сайта - чекаем БД игры (на наличие email или account)
			$api = new DragonApi();
			$playerInfo = json_decode($api->checkIssetPlayer($model->account, $model->email));

			if($playerInfo->account){
				return json_encode([
					'error' => ['account' => ['0' => 'Имя аккаунта уже используется']]
				]);
			}

			if($playerInfo->email){
				return json_encode([
					'error' => ['email' => ['0' => 'Адрес электронной почты уже используется']]
				]);
			}

			if($model->save())
			{
				if(!empty($model->role))
				{
					//получаем роль
					$userRole = Yii::$app->authManager->getRole($model->role);
					//назначаем роль созданному юзеру в RBAC
					Yii::$app->authManager->assign($userRole, $model->id);
				}

				//создаем аккаунт в БД игры
				$api = new DragonApi();
				$request = json_decode($api->createPlayer($model->account, $model->email, $model->password));

				if($request->result == 'success')
				{
					return json_encode([
						'success' => 'Сохранено'
					]);
				}
				else
				{
					return json_encode([
						'error' => 'Произошла непредвиденная ошибка при сохранении в БД игры'
					]);
				}
			}
		}
		// если позицию не удалось сохранить, выводим сообщение с ошибками
		else {
			if (!empty($model->getErrors()))
			return json_encode([
				'error' => KUseful::errorsToString($model->getErrors())
			]);
		else
			return json_encode([
				'error' => 'Произошла непредвиденная ошибка при сохранении'
			]);
		}
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		$request = Yii::$app->request->post();
 
		$model->attributes = $request;
		
	    $model->scenario = 'update';
	    $model->date_update = strtotime(date("Y-m-d h:i:s", time()));

	    if(isset($request['password']))
	    {
		    $model->password = $request['password'];
	    }
		
	    if(empty($model->role))
	    {
		    $model->role = NULL;
	    }

		//роли
	    $roles = Yii::$app->authManager->getRoles();
	    $userRole = Yii::$app->authManager->getRolesByUser($id);
	    $currentRole = KUseful::getUserRoleName($userRole);

		// если в запросе пришел только параметр "onlyShow",
		// значит нужно просто отобразить форму
		if (isset($request['onlyShow']) && $request['onlyShow'] == 'true') {
			return $this->renderPartial('update', [
				'model'       => $model,
				'roles'       => $roles,
				'currentRole' => $currentRole
			]);
		}
		// если пришли данные с формы, мы загрузили их в модель и она сохранилась - отдаем "success"
		else if ($model->validate()) {
			//получаем новую роль в RBAC
			$userRole = Yii::$app->authManager->getRole($model->role);

			//если нет обновления пароля
			if(empty($model->password))
			{
				if($model->save())
				{
					//обнуляем роль для User
					Yii::$app->authManager->revokeAll($model->id);
					//обновляем роль
					if($userRole) Yii::$app->authManager->assign($userRole, $model->id);


					return json_encode([
						'success' => 'Сохранено'
					]);
				}
				else
				{
					return json_encode([
						'error' => KUseful::errorsToString($model->getErrors())
					]);
				}
			}
			else
			{
				$model->password_hash = $model->setPassword($model->password);

				$api = new DragonApi();
				$request = json_decode($api->updatePlayer($model->account, $model->email, $model->password));

				if($request->result == 'success'){
					$model->save();
					//обнуляем роль для User
					Yii::$app->authManager->revokeAll($model->id);
					//обновляем роль
					Yii::$app->authManager->assign($userRole, $model->id);

					return json_encode([
						'success' => 'Сохранено'
					]);
				}
				else
				{
					return json_encode([
						'error' => 'Произошла непредвиденная ошибка при сохранении в БД игры'
					]);
				}
			}
		}
		// если позицию не удалось сохранить, выводим сообщение с ошибками
		else {
			if (!empty($model->getErrors()))
				return json_encode([
					'error' => KUseful::errorsToString($model->getErrors())
				]);
			else
				return json_encode([
					'error' => 'Произошла непредвиденная ошибка при сохранении'
				]);
		}
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if ($this->findModel($id)->delete())
		{
			//удаляем роль из RBAC
			Yii::$app->authManager->revokeAll($id);
			return "1";
		}
		else
			return "0";
    }

	public function actionRemoveitems()
	{
		$request = Yii::$app->request->post();
		if (isset($request['ids']) && is_array($request['ids'])) {
			foreach ($request['ids'] as $id) {
				$this->findModel($id)->delete();
				//удаляем роль из RBAC
				Yii::$app->authManager->revokeAll($id);
			}
			return "1";
		}
		else {
			return "0";
		}
	}

	
	/*
	* С помощью этого метода обновляем представление таблицы.
	* На входе может быть пусто, либо передай параметры для изменения таблицы (колонок).
	* На выход обязательно отдать 'items' и 'count'.
	* */
	public function actionGetitemsbycriteria()
	{
		$request = Yii::$app->request->post();

		$query = User::createQuery($_SESSION['users']['criteria']);
		$items = $query->all();
		$count = $query->count();

		$result['items'] = User::dataOutput($items);
		$result['count'] = $count;

		return json_encode($result);
	}

	/*
	* В этот метод передаем то, что нужно прибавить к существующей критерии,
	* измени его в соответствии с требованиями, если это необходимо
	* */
	public function actionChangecriteria()
	{
		$request = Yii::$app->request->post();
		$query = User::createQuery($_SESSION['users']['criteria']);

		if (!isset($request['method']) || !isset($request['params']))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);

		$params = json_decode($request['params']);

		// limit, offset используются при пагинации
		if ($request['method'] == 'limit') {
			$query->limit = $params;
			$query->offset = -1;
		}
		if ($request['method'] == 'offset') {
			$query->offset = $params;
		}
		if ($request['method'] == 'orderBy') {
		if (!isset($params->field) || !isset($params->trend))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);
		if ($params->trend == 'DESC')
			$trend = SORT_DESC;
		else
			$trend = SORT_ASC;
			$query->orderBy = [
				$params->field => $trend
			];
		}
		// если нужно добавить еще условия запроса (например при добавлении фильтра), описываем всё здесь
		if ($request['method'] == 'where') {
			if (!isset($params->field) || !isset($params->value))
				return json_encode([
					'error' => 'Некорректно указаны параметры'
				]);
			$query->where = [$params->field => $params->value];
		}

		$_SESSION['users']['criteria'] = User::createCriteria($query);
		if ($request['method'] == 'search') {
		if (!isset($params->searchColumns) || !isset($params->searchQuery))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);
			$_SESSION['users']['searchQuery'] = $params->searchQuery;
			$whereArr = ['OR'];
			foreach ($params->searchColumns as $column) {
				array_push($whereArr, ['like', $column, $params->searchQuery]);
			}
			$_SESSION['users']['criteria']['where'] = $whereArr;
			$_SESSION['users']['criteria']['offset'] = -1;
		}

		return json_encode([
			'success' => 'Критерий запроса обновлен'
		]);
	}

	// сохраняет изменения в полях "visible" и "order"
	public function actionSavechanges()
	{
		$request = Yii::$app->request->post();

		if (isset($request['data'])) {
	    	if (!isset($request['visible']))
			    $request['visible'] = [];
			foreach ($request['data'] as $item) {
				$model = $this->findModel($item['id']);
				$model->order = $item['order'];
				if (in_array($item['id'], $request['visible'])) {
					$model->visible = 1;
				} else {
					$model->visible = 0;
				}
				if (!$model->save() && !empty($model->getErrors()))
				{
					return json_encode([
						'error' => KUseful::errorsToString($model->getErrors())
					]);
				}
				elseif (!$model->save()) {
					return json_encode([
						'error' => 'Ошибка при сохранении элемента с id = '.$item['id']
					]);
				}
			}
		}
		else {
			return json_encode([
				'error' => 'недостаточно данных для выполнения данного действия'
			]);
		}

		return json_encode([
			'success' => 'Сохранено'
		]);
	}


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
