<?php

namespace app\modules\admin\controllers;

use app\helpers\KDebug;
use app\helpers\KUseful;
use Yii;
use app\modules\admin\models\Textbloks;
use app\modules\admin\components\AdminController;
use yii\web\NotFoundHttpException;

/**
 * TextbloksController implements the CRUD actions for Textbloks model.
 */
class TextbloksController extends AdminController
{
    /**
     * Lists all Textbloks models.
     * @return mixed
     */
    public function actionIndex()
    {
		$_SESSION['textbloks']['criteria']['orderBy'] = ['id' => SORT_DESC];
		if (!isset($_SESSION['textbloks']['criteria']['limit']))
			$_SESSION['textbloks']['criteria']['limit'] = AdminController::PAGER_LIMIT;
		$query = Textbloks::createQuery($_SESSION['textbloks']['criteria']);
		if (isset($_SESSION['textbloks']) && !empty($_SESSION['textbloks']['searchQuery']))
			$searchQuery = $_SESSION['textbloks']['searchQuery'];
		else
			$searchQuery = '';
		$models = $query->all();
		$count = $query->count();

		return $this->render('index', [
			'models'  => Textbloks::dataOutput($models),
			'count'   => $count,
			'columns' => Textbloks::columns(),
			'searchQuery' => $searchQuery,
			'limit'   => $_SESSION['textbloks']['criteria']['limit']
		]);
    }

    /**
     * Creates a new Textbloks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model = new Textbloks();
		$request = Yii::$app->request->post();
		$model->attributes = $request;

		// если в запросе пришел только параметр "onlyShow",
		// значит нужно просто отобразить форму
		if (isset($request['onlyShow']) && $request['onlyShow'] == 'true') {
			return $this->renderPartial('create', [
				'model' => $model,
			]);
		}
		// если пришли данные с формы, мы загрузили их в модель и она сохранилась - отдаем "success"
		else if ($model->save()) {
			return json_encode([
				'success' => 'Сохранено'
			]);
		}
		// если позицию не удалось сохранить, выводим сообщение с ошибками
		else {
			if (!empty($model->getErrors()))
			return json_encode([
				'error' => KUseful::errorsToString($model->getErrors())
			]);
		else
			return json_encode([
				'error' => 'Произошла непредвиденная ошибка при сохранении'
			]);
		}
    }

    /**
     * Updates an existing Textbloks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		$request = Yii::$app->request->post();

		$model->attributes = $request;

		// если в запросе пришел только параметр "onlyShow",
		// значит нужно просто отобразить форму
		if (isset($request['onlyShow']) && $request['onlyShow'] == 'true') {
			return $this->renderPartial('update', [
				'model' => $model,
			]);
		}
		// если пришли данные с формы, мы загрузили их в модель и она сохранилась - отдаем "success"
		else if ($model->save()) {
			return json_encode([
				'success' => 'Сохранено'
			]);
		}
		// если позицию не удалось сохранить, выводим сообщение с ошибками
		else {
			if (!empty($model->getErrors()))
				return json_encode([
					'error' => KUseful::errorsToString($model->getErrors())
				]);
			else
				return json_encode([
					'error' => 'Произошла непредвиденная ошибка при сохранении'
				]);
		}
    }

    /**
     * Deletes an existing Textbloks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if ($this->findModel($id)->delete())
			return "1";
		else
			return "0";
    }

	public function actionRemoveitems()
	{
		$request = Yii::$app->request->post();
		if (isset($request['ids']) && is_array($request['ids'])) {
			foreach ($request['ids'] as $id) {
				$this->findModel($id)->delete();
			}
			return "1";
		}
		else {
			return "0";
		}
	}

	
	/*
	* С помощью этого метода обновляем представление таблицы.
	* На входе может быть пусто, либо передай параметры для изменения таблицы (колонок).
	* На выход обязательно отдать 'items' и 'count'.
	* */
	public function actionGetitemsbycriteria()
	{
		$request = Yii::$app->request->post();

		$query = Textbloks::createQuery($_SESSION['textbloks']['criteria']);
		$items = $query->all();
		$count = $query->count();

		$result['items'] = Textbloks::dataOutput($items);
		$result['count'] = $count;

		return json_encode($result);
	}

	/*
	* В этот метод передаем то, что нужно прибавить к существующей критерии,
	* измени его в соответствии с требованиями, если это необходимо
	* */
	public function actionChangecriteria()
	{
		$request = Yii::$app->request->post();
		$query = Textbloks::createQuery($_SESSION['textbloks']['criteria']);

		if (!isset($request['method']) || !isset($request['params']))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);

		$params = json_decode($request['params']);

		// limit, offset используются при пагинации
		if ($request['method'] == 'limit') {
			$query->limit = $params;
			$query->offset = -1;
		}
		if ($request['method'] == 'offset') {
			$query->offset = $params;
		}
		if ($request['method'] == 'orderBy') {
		if (!isset($params->field) || !isset($params->trend))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);
		if ($params->trend == 'DESC')
			$trend = SORT_DESC;
		else
			$trend = SORT_ASC;
			$query->orderBy = [
				$params->field => $trend
			];
		}
		// если нужно добавить еще условия запроса (например при добавлении фильтра), описываем всё здесь
		if ($request['method'] == 'where') {
			if (!isset($params->field) || !isset($params->value))
				return json_encode([
					'error' => 'Некорректно указаны параметры'
				]);
			$query->where = [$params->field => $params->value];
		}

		$_SESSION['textbloks']['criteria'] = Textbloks::createCriteria($query);
		if ($request['method'] == 'search') {
		if (!isset($params->searchColumns) || !isset($params->searchQuery))
			return json_encode([
				'error' => 'Некорректно указаны параметры'
			]);
			$_SESSION['textbloks']['searchQuery'] = $params->searchQuery;
			$whereArr = ['OR'];
			foreach ($params->searchColumns as $column) {
				array_push($whereArr, ['like', $column, $params->searchQuery]);
			}
			$_SESSION['textbloks']['criteria']['where'] = $whereArr;
			$_SESSION['textbloks']['criteria']['offset'] = -1;
		}

		return json_encode([
			'success' => 'Критерий запроса обновлен'
		]);
	}

	// сохраняет изменения в полях "visible" и "order"
	public function actionSavechanges()
	{
		$request = Yii::$app->request->post();

		if (isset($request['data'])) {
	    	if (!isset($request['visible']))
			    $request['visible'] = [];
			foreach ($request['data'] as $item) {
				$model = $this->findModel($item['id']);
				$model->order = $item['order'];
				if (in_array($item['id'], $request['visible'])) {
					$model->visible = 1;
				} else {
					$model->visible = 0;
				}
				if (!$model->save() && !empty($model->getErrors()))
				{
					return json_encode([
						'error' => KUseful::errorsToString($model->getErrors())
					]);
				}
				elseif (!$model->save()) {
					return json_encode([
						'error' => 'Ошибка при сохранении элемента с id = '.$item['id']
					]);
				}
			}
		}
		else {
			return json_encode([
				'error' => 'недостаточно данных для выполнения данного действия'
			]);
		}

		return json_encode([
			'success' => 'Сохранено'
		]);
	}


    /**
     * Finds the Textbloks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Textbloks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Textbloks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
