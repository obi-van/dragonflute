<?php

namespace app\modules\admin;

use yii\filters\AccessControl;
use Yii;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [[
					'allow' => true,
					'roles' => ['@'],
				]]
			]
		];
	}

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
	    Yii::$app->user->loginUrl = ['/auth/login'];
    }
}
