<?php


namespace app\helpers;
use app\components\ImageHandler;
use app\models\Settings;
use app\models\Textbloks;
use app\models\Statics;
use app\models\User;
use Yii;
/**
 * Public helpers.
 *
 * @package    Kohana
 * @category   Helpers
 * @author     Kohana Team
 * @copyright  (c) 2007-2012 Kohana Team
 * @license    http://kohanaframework.org/license
 */
class KUseful {

	/**
	 * Подгружает настройку по ключу
	 */
	static function cnf($key = NULL)
	{
		$model = Settings::find()->where(['key' => $key])->one();
		return isset($model->value) ? $model->value : false;
	}

	/**
	 * Подгружает текстовый блок по ключу
	 */
	static function textblock($key = NULL)
	{
		$model = Textbloks::find()->where(['key' => $key])->one();
		return $model->content;
	}

	/**
	 * глухарь для изображений
	 * 1 - путь до оригинала
	 * 2 - путь до глухаря
	 */
	static function imgpatch($img, $path = '/img/1px.png')
	{
		if (file_exists(Yii::getAlias('@webroot') . '/upload/' . $img) && is_file(Yii::getAlias('@webroot') . '/upload/' . $img)) {
			return '/upload/' . $img;
		}
		else {
			return Yii::getAlias('@web') . $path;
		}
	}

	/**
	 * Генерит превьюшку
	 */
	static function create_thumb($name, $thumb_name, $thumb_width, $thumb_height)
	{
		$filepath = Yii::getAlias('@webroot').Yii::$app->params['updir'].$name;
		$thumbpath = Yii::getAlias('@webroot').Yii::$app->params['updir'].$thumb_name;
		$sizes = getimagesize($filepath);

		// Подсчитываем соотношение сторон картинки
		$ratio = $sizes[0] / $sizes[1];
		// Соотношение сторон нужных размеров
		$original_ratio = $thumb_width / $thumb_height;
		// Размеры, до которых обрежем картинку до масштабирования
		$crop_width = $sizes[0];
		$crop_height = $sizes[1];
		// Смотрим соотношения
		if($ratio > $original_ratio)
		{
			$crop_width = round($original_ratio * $crop_height);
		}
		else
		{
			$crop_height = round($crop_width / $original_ratio);
		}

		$ih = new ImageHandler();
		$ih->load($filepath);

		// Обрезаем по высчитанным размерам до нужной пропорции
		$ih->crop($crop_width, $crop_height);
		// Масштабируем картинку до точных размеров
		$ih->resize($thumb_width, $thumb_height);

		// Сохраняем изображение в файл
		$ih->save($thumbpath, false, 100);

		return strtolower($thumb_name);
	}

	// превращает массив с ошибками в строку
	public static function errorsToString($array)
	{
		$result = '';
		foreach ($array as $key => $value) {
			$result .= $value[0].'<br>';
		}

		return $result;
	}

	/**
	 * Создает  альяс для урла
	 */
	static function create_alias($id = NULL, $incomming_alias = NULL, $prefix = 'i')
	{
		$alias = $prefix.$id.'-'.$incomming_alias;
		return $alias;
	}

	/**
	 * переводит строку в транслит
	 */
	static function translit($string)
	{
		$replace=[
			" "=>"-",
			"'"=>"",
			"`"=>"",
			"а"=>"a","А"=>"a",
			"б"=>"b","Б"=>"b",
			"в"=>"v","В"=>"v",
			"г"=>"g","Г"=>"g",
			"д"=>"d","Д"=>"d",
			"е"=>"e","Е"=>"e",
			"ж"=>"zh","Ж"=>"zh",
			"з"=>"z","З"=>"z",
			"и"=>"i","И"=>"i",
			"й"=>"y","Й"=>"y",
			"к"=>"k","К"=>"k",
			"л"=>"l","Л"=>"l",
			"м"=>"m","М"=>"m",
			"н"=>"n","Н"=>"n",
			"о"=>"o","О"=>"o",
			"п"=>"p","П"=>"p",
			"р"=>"r","Р"=>"r",
			"с"=>"s","С"=>"s",
			"т"=>"t","Т"=>"t",
			"у"=>"u","У"=>"u",
			"ф"=>"f","Ф"=>"f",
			"х"=>"h","Х"=>"h",
			"ц"=>"c","Ц"=>"c",
			"ч"=>"ch","Ч"=>"ch",
			"ш"=>"sh","Ш"=>"sh",
			"щ"=>"sch","Щ"=>"sch",
			"ъ"=>"","Ъ"=>"",
			"ы"=>"y","Ы"=>"y",
			"ь"=>"","Ь"=>"",
			"э"=>"e","Э"=>"e",
			"ю"=>"yu","Ю"=>"yu",
			"я"=>"ya","Я"=>"ya",
			"і"=>"i","І"=>"i",
			"ї"=>"yi","Ї"=>"yi",
			"є"=>"e","Є"=>"e"
		];
		$str = iconv("UTF-8","UTF-8//IGNORE",strtr($string,$replace));
		$str = strtolower($str);
		$str = str_replace('/', '-', $str);
		$str = preg_replace('~[^-a-z0-9_]+~u', '', $str);
		// удаляем начальные и конечные '-'
		$str = trim($str, "-");
		return $str;
	}

	/**
	 * Склонение числа $num
	 */
	static function declination($num, $one, $ed, $mn, $notnumber = false)
	{
		$num = $num % 100;
		if($num === "") print "";
		if(($num == "0") or (($num >= "5") and ($num <= "20")) or preg_match("|[056789]$|",$num))
			if(!$notnumber)
				return "$num $mn";
			else
				return $mn;
		if(preg_match("|[1]$|",$num))
			if(!$notnumber)
				return "$num $one";
			else
				return $one;
		if(preg_match("|[234]$|",$num))
			if(!$notnumber)
				return "$num $ed";
			else
				return $ed;
	}

	/**
	 * преобразует Y-m-d H:s:i в Y месяц d
	 */
	static function date_for_human($date = false, $time = false)
	{
		if(!$date) $date = date('Y-m-d');
		$formated = date('d', strtotime($date));
		$formated .= '&nbsp;';
		$formated .= self::change_month(date('m', strtotime($date)));
		$formated .= '&nbsp;';
		$formated .= date('Y', strtotime($date));
		if ($time) {
			$formated .= '&nbsp;';
			$formated .= date('H:i', strtotime($date));
		}
		return $formated;
	}

	/**
	 * преобразует Y-m-d H:s:i в Y месяц d из unix
	 */
	static function date_for_human_from_unix($date = false, $time = false)
	{
		if(!$date) $date = strtotime(date('Y-m-d'));
		$formated = date('d', $date);
		$formated .= '&nbsp;';
		$formated .= self::change_month(date('m', $date));
		$formated .= '&nbsp;';
		$formated .= date('Y', $date);
		if ($time) {
			$formated .= '&nbsp;';
			$formated .= date('H:i', $date);
		}
		return $formated;
	}

	/**
	 * заменяет число на название месяца
	 */
	static function change_month_nominative($month)
	{
		switch($month)
		{
			case 1:
				return $month = 'Январь';
				break;
			case 2:
				return $month = 'Февраль';
				break;
			case 3:
				return $month = 'Март';
				break;
			case 4:
				return $month = 'Апрель';
				break;
			case 5:
				return $month = 'Май';
				break;
			case 6:
				return $month = 'Июнь';
				break;
			case 7:
				return $month = 'Июль';
				break;
			case 8:
				return $month = 'Август';
				break;
			case 9:
				return $month = 'Сентябрь';
				break;
			case 10:
				return $month = 'Октябрь';
				break;
			case 11:
				return $month = 'Ноябрь';
				break;
			case 12:
				return $month = 'Декабрь';
				break;
		}

	}

	/**
	 * преобразует Y-m-d H:s:i в месяц d, Y H:s
	 */
	static function date_for_forum_category($date = false, $time = false)
	{
		if(!$date) $date = date('Y-m-d');
		$formated = self::change_month_nominative(date('m', strtotime($date)));
		$formated .= '&nbsp;';
		$formated .= date('d', strtotime($date));
		$formated .= '&nbsp;,';
		$formated .= date('Y', strtotime($date));
		if ($time) {
			$formated .= '&nbsp;';
			$formated .= date('H:i', strtotime($date));
		}
		return $formated;
	}

	/**
	 * преобразует Y-m-d H:s:i в Y-m-d
	 * либо в Y.m.d, если $robots_format = false
	 */
	static function date_for_robot($date, $robots_format = true)
	{
		if(!$date) $date = date('Y-m-d');
		if($robots_format === true)
		{
			return date('Y-m-d', strtotime($date));
		}
		else
		{
			return date('d.m.Y', strtotime($date));
		}
	}

	/**
	 * заменяет число на название месяца
	 */
	static function change_month($month)
	{
		switch($month)
		{
			case 1:
				return $month = 'января';
				break;
			case 2:
				return $month = 'февраля';
				break;
			case 3:
				return $month = 'марта';
				break;
			case 4:
				return $month = 'апреля';
				break;
			case 5:
				return $month = 'мая';
				break;
			case 6:
				return $month = 'июня';
				break;
			case 7:
				return $month = 'июля';
				break;
			case 8:
				return $month = 'августа';
				break;
			case 9:
				return $month = 'сентября';
				break;
			case 10:
				return $month = 'октября';
				break;
			case 11:
				return $month = 'ноября';
				break;
			case 12:
				return $month = 'декабря';
				break;
		}

	}

	/**
	 * Clean HTML and values.
	 * Useful when using _GET and _POST values
	 * @param string In value
	 * @return string Out value
	 */
	public static function parseCleanValue($val) {
		if ($val == '') {
			return '';
		}

		if (get_magic_quotes_gpc()) {
			$val = stripslashes($val);
			$val = preg_replace("/\\\(?!&amp;#|\?#)/", "&#092;", $val);
		}
		$val = str_replace("&#032;", " ", $val);
		$val = str_replace("&", "&amp;", $val);
		$val = str_replace("<!--", "&#60;&#33;--", $val);
		$val = str_replace("-->", "--&#62;", $val);
		$val = preg_replace("/<script/i", "&#60;sсript",  $val);
		$val = str_replace(">", "&gt;", $val);
		$val = str_replace("<", "&lt;", $val);
		$val = str_replace('"', "&quot;", $val);
		$val = str_replace("$", "&#036;", $val);
		$val = str_replace("\r", "", $val); // Remove tab chars
//		$val = str_replace("!", "&#33;", $val);
		$val = str_replace("'", "&#39;", $val); // for SQL injection security

		// Recover Unicode
		$val = preg_replace("/&amp;#([0-9]+);/s", "&#\\1;", $val);
		// Trying to fix HTML entities without ;
		$val = preg_replace("/&#(\d+?)([^\d;])/i", "&#\\1;\\2", $val);

		return $val;
	}

	/*
	 * отправка писем
	 * */

	public static function sendEmail($subject, $view, $params, $from, $fromName, $to)
	{
		Yii::$app->mailer->compose($view, $params)
			->setFrom([$from => $fromName])
			->setTo($to)
			->setSubject($subject)
			->send();
	}

	/*
	 * получаем название роли пользователя RBAC
	 */
	public static function getUserRoleName($userRole = [])
	{
		$roles = [];

		if(!empty($userRole))
		{
			foreach ($userRole as $role)
			{
				$roles[] = $role->name;
			}
		}

		if(!empty($roles))
		{
			return $roles[0];
		}
		else
		{
			return null;
		}
	}

	/*
	 * return string - account name
	 */
	public static function getUserAccount($id)
	{
		$user = User::findOne($id);
		return $user->nickname ? $user->nickname : $user->account;
	}

	/*
	 * Обрезает строку по кол-ву символов и добавяет ...
	 */
	public static function lineCutter($str, $count)
	{
		$line = trim($str);
		$q = strlen($str);

		if($q >= $count)
		{
			$line = mb_strimwidth($line, 0, $count, '...');
		}

		return $line;
	}
}