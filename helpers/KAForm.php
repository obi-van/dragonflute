<?php
namespace app\helpers;
use Yii;

class KAForm {
	/**
	 * Generate html-block select in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of select
	 * @param   string $title title of block
	 * @param   array $options array options
	 * @param	string $subname name of title row in related model
	 * @param   string $width width select
	 * @param   array $params array params text input
	 * @param   boolean $first default false add first null value
	 * @return  string
	 */
	public static function select($model, $name, $title, $options, $subname = 'title' , $width = 10, $params = [], $first = false, $firstTitle = '---')
	{
		$str_params = '';

		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
		}

		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'.$name.'" class="col-xl-2 col-sm-2 col-form-label text-right">'.$title.'</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		$r .= '<select autocomplete="off" class="form-control select" name="'.$name.'" id="'.$name.'" '.$str_params.'>';

		if($first) $r.= '<option value="">'.$firstTitle.'</option>';
		foreach ($options as $key => $val)
		{
			if(is_object($val))
			{
				$selected = $val->id == $model->{$name} ? 'selected="selected"' : NULL;
				$r .= '<option value="'.htmlspecialchars($val->id).'" '.$selected.'>'.htmlspecialchars($val->{$subname}).'</option>';
			}
			else
			{
				$selected = $key == $model->$name ? 'selected="selected"' : NULL;
				$r .= '<option value="'.htmlspecialchars($key).'" '.$selected.'>'.htmlspecialchars($val).'</option>';
			}
		}

		$r .= '</select>';
		$r .= '</div>';
		$r .= '</div>';

		return $r;
	}

	public static function checkbox($model, $name, $title, $width = 10, $params = [])
	{
		$str_params = '';

		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
		}
		$checked = $model->$name ? ' checked="checked"' : '';

		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'.$name.'" class="col-xl-2 col-sm-2 col-form-label text-right">'.$title;
		$r .= '</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		$r .= '<input type="checkbox" class="col-xl-'.$width.' form-control checkbox-edit-popup" name="'.$name.'" id="'.$name.'" '.$str_params.$checked.'>';
		$r .= '</div>';
		$r .= '</div>';

		return $r;
	}


	/**
	 * Generate html-block input text in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $title title of block
	 * @param   string $width width text input
	 * @param   array $params array params text input
	 * @return  string
	 */
	public static function textInput($model, $name, $title, $width = 10, $params = [])
	{
		$str_params = '';
		$str_class = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
			if($key == 'class')
			{
				$str_class .= ' '.$val;
			}
		}

		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'.$name.'" class="col-xl-2 col-sm-2 col-form-label text-right">'.$title.'</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		$r .= '<input class="form-control"  autocomplete="off" type="text" name="'.$name.'" id="'.$name.'" value="'
			.htmlspecialchars($model->$name).'" '.$str_params.'>';
		$r .= '</div>';
		$r .= '</div>';

		return $r;
	}

	/**
	 * Generate html-block input text in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $title title of block
	 * @param   string $width width text input
	 * @param   array $params array params text input
	 * @return  string
	 */
	public static function passInput($model, $name, $title, $width = 4, $params = [])
	{
		$str_params = '';
		$str_class = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
			if($key == 'class')
			{
				$str_class .= ' '.$val;
			}
		}

		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'.$name.'" class="col-xl-2 col-sm-2 col-form-label text-right">'.$title.'</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		$r .= '<input class="form-control"  autocomplete="off" type="password" autocomplete="off" name="'.$name.'" id="'.$name.'" value="'
			.htmlspecialchars($model->$name).'" '.$str_params.'>';
		$r .= '</div>';
		$r .= '</div>';

		return $r;
	}

	/**
	 * Generate html-block input link in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $title title of block
	 * @param   string $width width text input
	 * @param   array $params array params text input
	 * @return  string
	 */
	public static function linkInput($model, $name, $title, $width = 4, $params = [])
	{
		$str_params = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
		}

		$r = '';
		$r .= '<tr>';
		$r .= '<th>'.$title.'</th>';
		$r .= '<td>';
		$r .= '<div class="form-group col-lg-'.$width.'">';
		$r .= '<div class="input-group">';
		$r .= '<span class="input-group-addon">http://</span>';
		$r .= '<input autocomplete="off" class="form-control" type="text" name="'.get_class($model).'Form['.$name.']" value="'.htmlspecialchars($model->$name).'" '.$str_params.'>';
		$r .= '</div>';
		$r .= '</div>';
		$r .= '</td>';
		$r .= '</tr>';

		return $r;
	}

	/**
	 * Generate html-block input text with datepicker in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $title title of block
	 * @param   array $params array params text input
	 * @return  string
	 */
	/*public static function dateInput($model, $name, $title = 'Дата', $params = [])
	{
		$str_params = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
		}

		$r = '';
		$r .= '<tr>';
		$r .= '<th>'.$title.'</th>';
		$r .= '<td>';
		$r .= '<div class="form-group col-lg-2">';
		$r .= '<input autocomplete="off" class="form-control datepicker" type="text" data-validation="required" name="'.get_class($model).'Form['.$name.']" value="'.KUseful::date_for_robot($model->$name).'" '.$str_params.'>';
		$r .= '</div>';
		$r .= '</td>';
		$r .= '</tr>';

		return $r;
	}*/
	public static function dateInput($model, $name, $title, $width = 10, $params = [])
	{
		$str_params = '';
		$str_class = '';
		$date = date('Y-m-d');
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
			if($key == 'class')
			{
				$str_class .= ' '.$val;
			}
		}

		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'.$name.'" class="col-xl-2 col-sm-2 col-form-label text-right">'.$title.'</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		
		if(isset($model->$name))
		{
			$r .= '<input class="form-control datepicker-here"  autocomplete="off" type="text" name="'.$name.'" id="'.$name.'" value="'
			.htmlspecialchars($model->$name).'" '.$str_params.'>';
		}
		else
		{
			$r .= '<input class="form-control datepicker-here"  autocomplete="off" type="text" name="'.$name.'" id="'.$name.'" value="'
			.$date.'" '.$str_params.'>';
		}
		
		$r .= '</div>';
		$r .= '</div>';
		$r .= '<script>';
		$r .= '$(\'.datepicker-here\').datepicker({ modal: true, dateFormat: "yyyy-mm-dd"});';
		$r .= '</script>';

		return $r;
	}

	/**
	 * Generate html-block input hidden in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $value value of text input
	 * @param   array $params array params text input
	 * @return  string
	 */
	public static function hiddenInput($model, $name, $value = NULL, $params = [])
	{
		$str_params = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
		}

		$r = '';
		$r .= '<input type="hidden" name="'.get_class($model).'Form['.$name.']" autocomplete="off" value="'.htmlspecialchars($value).'" '.$str_params.'>';

		return $r;
	}


	/**
	 * Generate html-block textarea in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $title title of block
	 * @param   string $width width textarea
	 * @param   array $params array params textarea
	 * @return  string
	 */
	public static function textarea($model, $name, $title, $width = 10, $params = [])
	{
		$str_params = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
		}

		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'. $name. '" class="col-xl-2 col-sm-2 col-form-label text-right">'. $title.'</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		$r .= '<textarea class="form-control" autocomplete="off" name="'.$name.'" id="'.$name.'" rows="4" '.$str_params.'>'.$model->$name.'</textarea>';
		$r .= '</div>';
		$r .= '</div>';

		return $r;
	}

	/**
	 * Generate html-block textarea with wysiwyg in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $title title of block
	 * @param   string $width width textarea
	 * @return  string
	 */
	public static function wysiwyg($model, $name, $title, $width = 10, $params = [])
	{
		/*$r = '';
		$r .= '<tr>';
		$r .= '<th>'.$title.'</th>';
		$r .= '<td>';
		$r .= '<div class="form-group col-lg-'.$width.'">';
		$r .= '<textarea autocomplete="off" class="form-control wysiwyg ckeditor" name="'.get_class($model).'Form['.$name.']">'.$model->$name.'</textarea>';
		$r .= '</div>';
		$r .= '</td>';
		$r .= '</tr>';

		return $r;*/

		$str_params = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
		}

		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'. $name. '"class="col-xl-2 col-sm-2 col-form-label text-right">'. $title.'</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		$r .= '<textarea class="form-control jquery_ckeditor" autocomplete="off" name="'.$name.'" id="'.$name.'" rows="6" '.$str_params.'>'.$model->$name.'</textarea>';
		$r .= '</div>';
		$r .= '</div>';
		$r .= '<script>';
	    $r .= '$(\'.jquery_ckeditor\').ckeditor(function(){
					CKFinder.setupCKEditor( this, \'/ckfinder/\' );
			  });';
	    $r .= '</script>';
		return $r;
	}

	/**
	 * Generate html-block img in backend.
	 *
	 * @param   object  $model   model
	 * @param   string  $name    name of file input
	 * @param   string  $title   title of block
	 * @param   string  $action  this action
	 * @param   string  $subaction  this subaction (image or bgimage)
	 * @param   array   $thumb   width and height thumbnails
	 * @return  string
	 */
	public static function img($model, $name, $title, $subname='', $width = 10, $thumbs = [])
	{
		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'.$name.'" class="custom-file col-lg-2 text-right">'.$title.'</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		$path = Yii::getAlias('@web') . '/imgs/noimage.png';
		if($model->$name) {
		    if(preg_match("/.svg$/", $model->$name)){
                $img = $model->$name;
                $path = KUseful::imgpatch($model->$name);
            }
            else{
                $img = json_decode($model->$name);
                $path = isset($img->img) ? KUseful::imgpatch($img->img) : Yii::getAlias('@web') . '/imgs/noimage.png';
            }
		}

		$name = empty($subname) ? $name : $subname;

		$r .= '<img id="imgSrc" class="img-in-form-popup" src="'.$path.'" alt="Image" />';

		if(isset($img->img) || isset($img))
        {
            $r .= '<span class="vue-editer img-vue-editer" onclick="removeImg(\''.Yii::$app->controller->id.'\', '.$model->id.', \''.$name.'\')"><i class="fa fa-times"></i></span>';
        }
		$r .= '<div>';
		$r .= '<label class="custom-file">';
		$r .= '<input type="file" id="imgInput" name="'.$name.'" class="custom-file-input" accept="image/*" onchange="previewFile(this)">';
		$r .= '<span class="btn btn-secondary form-control-file">Выберите файл</span>';
		$r .= '</label>';
		$r .= '</div>';
		$r .= '</div>';
		foreach($thumbs as $key => $thumb)
		{
			$r .= '<input autocomplete="off" type="hidden" name="'.get_class($model).'Form['.$name.'-width]['.$key.']" value="'.$thumb[0].'">';
			$r .= '<input autocomplete="off" type="hidden" name="'.get_class($model).'Form['.$name.'-height]['.$key.']" value="'.$thumb[1].'">';
		}
		$r .= '</div>';
		return $r;
	}

	/**
	 * Generate html-block input text in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $title title of block
	 * @param   string $width width text input
	 * @param   array $params array params text input
	 * @return  string
	 */
	/*public static function fileInput($model, $name, $title, $action, $subaction = 'file', $params = [])
	{
		//echo KDebug::vars($_SERVER['DOCUMENT_ROOT'].Yii::$app->params['updir'].$model->$name); die;
		$str_params = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
		}

		$r = '';
		$r .= '<tr class="tr-' . $name . '">';
		$r .= '<th>'.$title.'</th>';
		$r .= '<td>';
		$r .= '<div class="form-group col-lg-8">';
		if(file_exists($_SERVER['DOCUMENT_ROOT'].Yii::$app->params['updir'].$model->$name) && is_file($_SERVER['DOCUMENT_ROOT'].Yii::$app->params['updir'].$model->$name)) {
			$r .= '<p>';
			$r .= '<span class="thumb thumb-big" style="padding-right: 25px">';
			$r .= '<a href='.KUseful::updir().$model->$name.' target="_blanck" download>Скачать '.$title.'</a>';
			$r .= '<a class="delete" href="'.KUseful::base().'/admin/'.$action.$subaction.'delete/id/'.$model->id.'/path/'.$model->$name.'"><i class="glyphicon glyphicon-remove"></i></a>';
			$r .= '</span>';
			$r .= '</p>';
		}
		$r .= '<input type="file" autocomplete="off" name="'.$name.'">';
		$r .= '</div>';
		$r .= '</td>';
		$r .= '</tr>';

		return $r;
	}*/


	public static function fileInput($model, $name, $title, $subname='', $width = 10, $params = [])
	{

		$r = '';
		$r .= '<div class="form-group row">';
		$r .= '<label for="'.$name.'" class="custom-file col-lg-2 text-right">'.$title.'</label>';
		$r .= '<div class="col-xl-'.$width.'">';
		if(file_exists($_SERVER['DOCUMENT_ROOT'].Yii::$app->params['updir'].$model->$name) && is_file($_SERVER['DOCUMENT_ROOT'].Yii::$app->params['updir'].$model->$name)) {
			$r .= '<p>';
			$r .= '<span class="thumb thumb-big" style="padding-right: 25px">';
			$r .= '<a href='.Yii::getAlias('@web').'/upload/'.$model->$name.' target="_blank" download>Download '.$model->$name.'</a>';
            $r .= '<span class="vue-editer img-vue-editer" onclick="removeImg(\''.Yii::$app->controller->id.'\', '.$model->id.', \''.$name.'\')"><i class="fa fa-times"></i></span>';
            $r .= '</span>';
			$r .= '</p>';
		}

		$name = empty($subname) ? $name : $subname;

		$r .= '<div class="custom-file">';
		$r .= '<input type="file" id="fileInput" name="'.$name.'" class="form-control-file">';
		$r .= '</div>';
		$r .= '</div>';
		$r .= '</div>';
		return $r;
	}

	/**
	 * Generate html-block imgs in backend.
	 *
	 * @param   object  $array   [{'img': $img, 'thumb': $thumb}]
	 * @param   string  $name    name of file input
	 * @param   string  $title   title of block
	 * @param   string  $action  this action
	 * @param   array   $thumb   width and height thumbnails
	 * @return  string
	 */
	public static function imgs($model, $name, $title, $action, $subaction = 'imgs', $thumbs = [])
	{
		$r = '';
		$r .= '<tr>';
		$r .= '<th>'.$title.'</th>';
		$r .= '<td>';
		$r .= '<div class="form-group col-lg-12 thumb-section sortable sortable-imgs" data-name="'.$name.'" data-model="'.get_class($model).'">';
		if($model->$name)
		{
			$imgs = json_decode($model->$name);

			if(!empty($imgs))
			{
				foreach ($imgs as $img)
				{
					$r .= '<span class="thumb thumb-middle sortable-item" data-img="'.$img->img.'" data-json=\''.json_encode($img).'\' data-id="'.$model->id.'" data-model="'.get_class($model).'">';
					$r .= '<img class="img-responsive" src="'.KUseful::updir().$img->thumb.'">';
					if($model->id)
					{
						$r .= '<a href="'.KUseful::base().'/admin/'.$action.$subaction.'delete/id/'.$model->id.'/path/'.$img->img.'" class="delete" href="#modalConfirm"><i class="glyphicon glyphicon-remove"></i></a>';
					}
					/*
					else
					{
						$r .= '<a class="delete" href="#" onclick="$(this).parents(\'.thumb\').remove()"><i class="glyphicon glyphicon-remove"></i></a>';
					}
					 *
					 */
					$r .= '</span>';
				}
			}
		}
		$r .= '</div>';

		$r .= '<div id="imgs" class="form-group col-lg-12 imgs">';
		foreach($thumbs as $key => $thumb)
		{
			$r .= '<input autocomplete="off" type="hidden" name="'.get_class($model).'Form['.$name.'-width]['.$key.']" value="'.$thumb[0].'">';
			$r .= '<input autocomplete="off" type="hidden" name="'.get_class($model).'Form['.$name.'-height]['.$key.']" value="'.$thumb[1].'">';
		}
		$r .= '<p>';
		$r .= '<input type="file" name="imgs-1-1" id="imgs-1-1">';
		$r .= '<i class="glyphicon glyphicon-plus-sign" data-next="2" data-gallery="1" onclick="addFileInput(this);"></i>';
		$r .= '</p>';
		$r .= '</div>';
		$r .= '</td>';
		$r .= '</tr>';

		return $r;
	}

	/**
	 * Generate html-block checkbox group.
	 *
	 * @param   object $model model
	 * @param   string $array array checkboxes: ['name_of_cell' => 'label_of_checkbox']
	 * @param   string $titlename name of related cell
	 * @return  string
	 */
	public static function checkboxArray($model, $array, $title)
	{
		$r = '';
		$r .= '<tr>';
		$r .= '<th>'.$title.'</th>';
		$r .= '<td>';

		$r .= '<div class="form-group col-lg-8">';
		foreach($array as $key => $value)
		{
			$checked = '';
			if($model->{$key} == 1)
			{
				$checked = 'checked="checked"';
			}
			$r .= '<label class="checkbox-inline" style="margin-left: 10px;">';
			$r .= '<input type="hidden" name="'.get_class($model).'Form['.$key.']" value="0">';
			$r .= '<input type="checkbox" '.$checked.' name="'.get_class($model).'Form['.$key.']"> '.$value;
			$r .= '</label>';
		}
		$r .= '</div>';
		$r .= '</td>';
		$r .= '</tr>';

		return $r;
	}

	/**
	 * Generate html-block checkbox group.
	 *
	 * @param   object $model model
	 * @param   string $name name of checkbox input
	 * @param   object $submodel submodel (object / array)
	 * @param   string $subname related key (if submodel is object or unused)
	 * @param   string $titlename name of related cell
	 * @param   string $title title of block
	 * @return  string
	 */
	public static function checkboxGroup($model, $name, $submodel, $subname, $titlename, $title)
	{
		$r = '';
		$r .= '<tr>';
		$r .= '<th>'.$title.'</th>';
		$r .= '<td>';

		$r .= '<div class="form-group col-lg-8">';
		foreach($submodel as $row)
		{
			$checked = '';
			foreach($model->$name as $i)
			{
				if($i->{$subname.'_id'} == $row->id)
				{
					$checked = 'checked="checked"';
				}
			}
			//echo KDebug::vars($subname, $row); die;
			$r .= '<label class="checkbox-inline" style="margin-left: 10px;">';
			$r .= '<input type="checkbox" '.$checked.' name="'.get_class($model).'Form['.$name.']['.$row->id.']"> '.$row->$titlename;
			$r .= '</label>';
		}
		$r .= '<input type="hidden" value="1" name="'.get_class($model).'Form['.$name.'][checkboxgroup]">';
		$r .= '</div>';
		$r .= '</td>';
		$r .= '</tr>';

		return $r;
	}

	/**
	 * Generate html-block separator in backend.
	 *
	 * @param   string $color class for colored:
	 * 'success' = green; 'info' = 'blue';
	 * 'warning' = yellow; 'danger' = red;
	 * @return  string
	 */
	public static function separator($color = 'info')
	{
		$r = '';
		$r .= '<tr class="tr-separator '.$color.'">';
		$r .= '<th></th>';
		$r .= '<td></td>';
		$r .= '</tr>';

		return $r;
	}

	/**
	 * Parce cell from row generated KAForm.
	 *
	 * @param   string $row html code table-row:
	 * @return  string
	 */
	public static function parseCellFromRow($row)
	{
		$r = preg_replace("'<th[^>]*?>.*?</th>'si","",str_replace(['<tr>', '</tr>'], '', $row));

		return $r;
	}

	/**
	 * Generate table cell block input text as part of array in backend.
	 * for example: ProductsInstancesForm[1][25][articul]
	 * as: $model.Form[$keyInArray][$model->id][$model->$name]
	 *
	 * @param   object $model  model
	 * @param   string $name name of text input
	 * @param   string $width width text input
	 * @param   array $params array params text input
	 * @param   int $keyInArray key in array
	 * @return  string
	 */
	public static function textInputCellInArray($model, $name, $width = 4, $params = [], $keyInArray = 0)
	{
		$str_params = '';
		$str_class = '';
		foreach ($params as $key => $val)
		{
			$str_params .= $key.'="'.$val.'" ';
			if($key == 'class')
			{
				$str_class .= ' '.$val;
			}
		}

		$r = '';
		$r .= '<td>';
		$r .= '<div class="form-group col-lg-'.$width.'">';
		$r .= '<input class="form-control'.$str_class.'" type="text" autocomplete="off" name="'.get_class($model).'Form['.$keyInArray.']['.($model->id ? $model->id : 0).']['.$name.']" value="'.htmlspecialchars($model->$name).'" '.$str_params.'>';
		$r .= '</div>';
		$r .= '</td>';

		return $r;
	}

	/**
	 * Generate table cell block checkbox group as part of array in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of checkbox input
	 * @param   object $submodel  submodel
	 * @param   string $titlename name of related cell
	 * @param   int $keyInArray key in array
	 * @return  string
	 */
	public static function checkboxGroupCellInArray($model, $name, $submodel, $titlename = 'title', $keyInArray = 0)
	{
		$r = '';
		$r .= '<td>';
		$r .= '<div class="form-group col-lg-12">';
		foreach($submodel as $row)
		{
			$checked = '';
			foreach($model->$name as $i)
			{
				if($i->id == $row->id)
				{
					$checked = 'checked="checked"';
				}
			}
			$r .= '<label class="checkbox-inline" style="margin-left: 10px;">';
			$r .= '<input type="checkbox" '.$checked.' name="'.get_class($model).'Form['.$keyInArray.']['.($model->id ? $model->id : 0).']['.$name.']['.$row->id.']"> '.$row->$titlename;
			$r .= '</label>';
		}
		$r .= '<input type="hidden" value="1" name="'.get_class($model).'Form['.$keyInArray.']['.($model->id ? $model->id : 0).']['.$name.'][checkboxgroup]">';
		$r .= '</div>';
		$r .= '</td>';

		return $r;
	}

	/**
	 * Generate table cell block multiple select as part of array in backend.
	 *
	 * @param   object $model  model
	 * @param   string $name name of checkbox input
	 * @param   object $submodel  submodel
	 * @param   string $titlename name of related cell
	 * @param   int $keyInArray key in array
	 * @return  string
	 */
	public static function selectMultipleCellInArray($model, $name, $submodel, $titlename = 'title', $keyInArray = 0)
	{
		$r = '';
		$r .= '<td>';
		$r .= '<div class="form-group col-lg-12">';
		$r .= '<select style="width: 100%;" multiple="multiple" class="chosen-select" name="'.get_class($model).'Form['.$keyInArray.']['.($model->id ? $model->id : 0).']['.$name.'][]">';
		foreach($submodel as $row)
		{
			$selected = '';
			foreach($model->$name as $i)
			{
				if($i->id == $row->id)
				{
					$selected = 'selected="selected"';
				}
			}
			$r .= '<option '.$selected.' value="'.htmlspecialchars($row->id).'">'.$row->$titlename.'</option>';
		}
		$r .= '</select>';
		$r .= '</div>';
		$r .= '</td>';

		return $r;
	}


	/**
	 * Generate html-block with title
	 *
	 * @param   string $title
	 * @return  string
	 */
	public static function title($title = '')
	{
		$r = '';
		$r .= '<tr><th colspan="2"><h3>';
		$r .= $title;
		$r .= '</h3></th></tr>';

		return $r;
	}

}