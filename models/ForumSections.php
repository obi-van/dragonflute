<?php

namespace app\models;

use app\helpers\KUseful;
use app\helpers\Debug;
use Yii;

/**
 * This is the model class for table "{{%forum_sections}}".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property integer $order
 * @property integer $visible
 * @property integer $for_admins
 * @property array $categories
 */
class ForumSections extends \yii\db\ActiveRecord
{
	public $categories;
	public $img;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%forum_sections}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['title'], 'required'],
            [['order', 'visible', 'for_admins'], 'integer'],
            [['alias', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'order' => 'Order',
            'visible' => 'Visible',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ForumSectionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ForumSectionsQuery(get_called_class());
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->alias = KUseful::create_alias($this->id, KUseful::translit($this->title), 'df');
		$this->updateAttributes(['alias']);
	}

	public function getImg()
	{
		switch ($this->id)
		{
			case 6:
				$img = 'diskette.svg';
				break;
			default:
				$img = 'rocket.svg';
		}

		return $img;
	}
}
