<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "df_textbloks".
 *
 * @property integer $id
 * @property string $key
 * @property string $title
 * @property string $content
 */
class Textbloks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'df_textbloks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'title'], 'required'],
            [['content'], 'string'],
            [['key', 'title'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'title' => 'Title',
            'content' => 'Content',
        ];
    }

    /**
     * @inheritdoc
     * @return TextbloksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\TextbloksQuery(get_called_class());
    }
}
