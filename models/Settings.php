<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "df_settings".
 *
 * @property int $id
 * @property string $key
 * @property int $type
 * @property string|null $value
 * @property string|null $title
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'type'], 'required'],
            [['type'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 45],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'type' => 'Type',
            'value' => 'Value',
            'title' => 'Title',
        ];
    }
}
