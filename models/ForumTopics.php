<?php

namespace app\models;

use app\helpers\KUseful;
use Yii;
use app\helpers\Debug;
use bupy7\bbcode\BBCodeBehavior;

/**
 * This is the model class for table "{{%forum_topics}}".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $message
 * @property string $date_create
 * @property string $forum_category_id
 * @property string $user_id
 *
 * @property ForumCategories $forumCategory
 * @property User $user
 */
class ForumTopics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%forum_topics}}';
    }

	public function behaviors()
	{
		return [
			[
				'class' => BBCodeBehavior::className(),
				'attribute' => 'message',
				'saveAttribute' => 'message',
				'codeDefinitionBuilder' => [
					function($builder) {
						$builder->setTagName('video');
						$builder->setReplacementText('<iframe allowfullscreen="" height="360" src="https://www.youtube.com/embed/{param}" width="540"></iframe>');
						return $builder->build();
					},
				]
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'message', 'type'], 'safe'],
            [['title', 'message'], 'required'],
            [['forum_category_id', 'user_id'], 'integer'],
            [['title'], 'string', 'max' => 65],
	        [['alias'], 'string', 'max' => 255],
//            [['forum_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ForumCategories::className(), 'targetAttribute' => ['forum_category_id' => 'id']],
//            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Название темы',
            'message' => 'Сообщение',
            'date_create' => 'Дата создания',
            'forum_category_id' => 'Forum Category ID',
            'user_id' => 'User ID',
	        'type' => 'Статус топика'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForumCategory()
    {
        return $this->hasOne(ForumCategories::className(), ['id' => 'forum_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getForumAnswers()
	{
		return $this->hasMany(ForumAnswers::className(), ['topic_id' => 'id']);
	}

    /**
     * @inheritdoc
     * @return \app\models\queries\ForumTopicsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ForumTopicsQuery(get_called_class());
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->alias = KUseful::create_alias($this->id, KUseful::translit($this->title), 'df-topic');
		$this->updateAttributes(['alias']);
	}

	public function getTopicTypeImg()
	{
		switch ($this->type)
		{
			case 'important':
				$img = 'lamp.svg';
				break;
			case 'info':
				$img = 'forum-info-icon.svg';
				break;
			case 'closed':
				$img = 'closed.svg';
				break;
			default:
				$img = 'forum-info-icon.svg';
		}

		return $img;
	}

	public function getTopicType()
	{
		switch ($this->type)
		{
			case 'important':
				$type = 'Важно';
				break;
			case 'info':
				$type = 'Инфо';
				break;
			case 'closed':
				$type = 'Закрыто';
				break;
			default:
				$type = 'Инфо';
		}

		return $type;
	}

	public function getTopicAuthorName()
	{
		$user = User::findOne($this->user_id);

		if($user)
		{
			return $user->nickname ? $user->nickname : $user->account;
		}
		else
		{
			return '&mdash;';
		}
	}

	public function getQuantityAnswers()
	{
		$answers = ForumAnswers::find()->where(['topic_id' => $this->id])->notDeleted()->all();

		return count($answers);
	}

	public function getLastMessageInfo()
	{
		$answers = ForumAnswers::find()->where(['topic_id' => $this->id])->notDeleted()->orderByDateDesc()->all();

		if(empty($answers))
		{
			$message = $this;
		}
		else
		{
			$message = $answers[0];
		}

		$date = KUseful::date_for_forum_category($message->date_create, true);

		$str = '';
		$str .= '<div class="forum__theme-message">'.$date.'</div>';
		$str .= '<div class="forum__theme-message">Автор: ';
		$str .= !empty($message->user->nickname) ? $message->user->nickname : $message->user->account;
		$str .= '</div>';

		return $str;
	}

	public function getQuantityMessages()
	{
		$user = $this->user;

		$topics = ForumTopics::find()->where(['user_id' => $user->id])->notDeleted()->all();
		$answers = ForumAnswers::find()->where(['user_id' => $user->id])->notDeleted()->all();

		$quantityMessage = count($topics) + count($answers);

		return $quantityMessage;
	}
}
