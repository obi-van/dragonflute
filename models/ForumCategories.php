<?php

namespace app\models;

use app\helpers\KUseful;
use Yii;
use app\helpers\Debug;

/**
 * This is the model class for table "{{%forum_categories}}".
 *
 * @property string $id
 * @property integer $section_id
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property integer $visible
 * @property integer $order
 *
 * @property ForumSections $section
 */
class ForumCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%forum_categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['title'], 'required'],
            [['section_id', 'visible', 'order', 'for_players'], 'integer'],
            [['alias', 'title', 'description'], 'string', 'max' => 255],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => ForumSections::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_id' => 'Раздел',
            'title' => 'Заголовок',
            'description' => 'Описание',
	        'for_players' => 'Разрешить игрокам создавать топики'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(ForumSections::className(), ['id' => 'section_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ForumCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ForumCategoriesQuery(get_called_class());
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->alias = KUseful::create_alias($this->id, KUseful::translit($this->title), 'df');
		$this->updateAttributes(['alias']);
	}

	public function getQuantityTopics()
	{
		//топики в категории
		$topics = ForumTopics::find()->where(['forum_category_id' => $this->id])->notDeleted()->all();

		return count($topics);
	}

	public function getQuantityAnswers()
	{
		$answers = 0;
		//топики в категории
		$topics = ForumTopics::find()->where(['forum_category_id' => $this->id])->notDeleted()->all();

		//перебираем топики и считаем кол-во ответов
		if($topics)
		{
			foreach ($topics as $topic)
			{
				$answers += 1;
				$topicAnswers = $topic->forumAnswers;
				if(!empty($topicAnswers))
				{
					foreach ($topicAnswers as $item)
					{
						$answers += 1;
					}
				}
			}
		}

		return $answers;
	}

	public function getLastMessage()
	{
		$lastMessage = null;
		$currentTopic = null;

		//свежий топик
		$topics = ForumTopics::find()->where(['forum_category_id' => $this->id])->notDeleted()->orderByDate()->all();
		if(!empty($topics))
		{
			$topic = $topics[0];

			$keys = [];
			foreach ($topics as $key=>$item)
			{
				$keys[] = $key;
			}

			$answers = ForumAnswers::find()->andWhere(['id'=>$keys])->orderByDateDesc()->all();

			if(!empty($answers))
			{
				$answer = $answers[0];
				if($topic->date_create > $answer->date_create)
				{
					$lastMessage = $topic;
					$currentTopic = $topic;
				}
				else
				{
					$lastMessage = $answer;
					$currentTopic = $answer->topic;
				}
			}
			else
			{
				$lastMessage = $topic;
				$currentTopic = $topic;
			}
		}

		if($lastMessage)
		{
			$str = '';
			$str .= '<div class="forum__category-message">';
			$str .= '<a href="/forum/'.$this->alias.'/'.$currentTopic->alias.'">['.KUseful::date_for_robot($currentTopic->date_create, false).'] '.KUseful::lineCutter($currentTopic->title, 25).'</a>';
			$str .= '</div>';

			$str .= '<div class="forum__category-message">Автор: ';
			$str .= !empty($lastMessage->user->nickname) ? $lastMessage->user->nickname : $lastMessage->user->account;
			$str .= '</div>';
			$str .= '<div class="forum__category-message">'.KUseful::date_for_forum_category($lastMessage->date_create, true).'</div>';
		}
		else
		{
			$str = '';
			$str .= '<div class="forum__category-message">';
			$str .= '&nbsp;';
			$str .= '</div>';

			$str .= '<div class="forum__category-message">&nbsp;</div>';
			$str .= '<div class="forum__category-message">&nbsp;</div>';
		}

		return $str;
	}
}
