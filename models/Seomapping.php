<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%seomapping}}".
 *
 * @property string $id
 * @property string $uri
 * @property string $title
 * @property string $keywords
 * @property string $description
 */
class Seomapping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%seomapping}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uri'], 'required'],
            [['uri', 'title'], 'string', 'max' => 255],
            [['keywords', 'description'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uri' => 'Uri',
            'title' => 'Title',
            'keywords' => 'Keywords',
            'description' => 'Description',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\SeomappingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\SeomappingQuery(get_called_class());
    }
}
