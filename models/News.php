<?php

namespace app\models;

use app\helpers\KUseful;
use Yii;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property string $id
 * @property string $alias
 * @property string $seotitle
 * @property string $seokeywords
 * @property string $seodescription
 * @property string $short_title
 * @property string $short_description
 * @property string $title
 * @property string $content
 * @property string $img
 * @property integer $visible
 * @property integer $top
 * @property string $date
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seotitle', 'seokeywords', 'seodescription', 'short_title', 'short_description', 'title', 'date'], 'required'],
            [['content', 'img'], 'string'],
            [['visible', 'top'], 'integer'],
            [['date'], 'safe'],
            [['alias', 'seotitle', 'seokeywords', 'title'], 'string', 'max' => 255],
            [['seodescription'], 'string', 'max' => 500],
            [['short_title'], 'string', 'max' => 23],
            [['short_description'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'seotitle' => 'Seotitle',
            'seokeywords' => 'Seokeywords',
            'seodescription' => 'Seodescription',
            'short_title' => 'Short Title',
            'short_description' => 'Short Description',
            'title' => 'Title',
            'content' => 'Content',
            'img' => 'Img',
            'visible' => 'Visible',
            'top' => 'Top',
            'date' => 'Date',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\NewsQuery(get_called_class());
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->alias = KUseful::create_alias($this->id, KUseful::translit($this->short_title), 'df-news');
		$this->updateAttributes(['alias']);
	}
}
