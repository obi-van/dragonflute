<?php

namespace app\models;

use app\helpers\KUseful;
use Yii;

/**
 * This is the model class for table "{{%events}}".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $description
 * @property string $link
 * @property integer $order
 * @property integer $visible
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%events}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['title'], 'required'],
            [['order', 'visible'], 'integer'],
            [['alias'], 'string', 'max' => 100],
	        [['img'], 'string'],
            [['title'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 180],
	        [['link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
	        'img' => 'Изображение',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'link' => 'Ссылка',
            'order' => 'Order',
            'visible' => 'Visible',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\EventsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\EventsQuery(get_called_class());
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->alias = KUseful::create_alias($this->id, KUseful::translit($this->title), 'df');
		$this->updateAttributes(['alias']);
	}
}
