<?php

namespace app\models;

use app\helpers\KUseful;
use app\helpers\Debug;
use Yii;

/**
 * This is the model class for table "{{%bosses}}".
 *
 * @property integer $id
 * @property string $alias
 * @property string $img
 * @property string $name
 * @property integer $deaths
 * @property integer $order
 * @property integer $visible
 */
class Bosses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bosses}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['name'], 'required'],
            [['img'], 'string'],
            [['deaths', 'order', 'visible'], 'integer'],
	        [['deaths'], 'default', 'value' => 0],
            [['alias', 'name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'alias',
            'img' => 'Изображение',
            'name' => 'Наименование',
            'deaths' => 'Количество смертей',
            'order' => 'Order',
            'visible' => 'Visible',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\BossesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\BossesQuery(get_called_class());
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->alias = KUseful::create_alias($this->id, KUseful::translit($this->name), 'df');
		$this->updateAttributes(['alias']);
	}
}
