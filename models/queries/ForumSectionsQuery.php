<?php

namespace app\models\queries;
use app\helpers\KUseful;

/**
 * This is the ActiveQuery class for [[\app\models\ForumSections]].
 *
 * @see \app\models\ForumSections
 */
class ForumSectionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\ForumSections[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\ForumSections|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function visibled()
	{
		return $this->andWhere('[[visible]]=1');
	}

	public function order()
	{
		return $this->orderBy(['order' => SORT_ASC]);
	}
}
