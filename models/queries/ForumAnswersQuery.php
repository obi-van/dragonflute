<?php

namespace app\models\queries;
use app\helpers\KUseful;

/**
 * This is the ActiveQuery class for [[ForumAnswers]].
 *
 * @see ForumAnswers
 */
class ForumAnswersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ForumAnswers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ForumAnswers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function visibled()
	{
		return $this->andWhere('[[visible]]=1');
	}

	public function order()
	{
		return $this->orderBy(['order' => SORT_ASC]);
	}

	public function notDeleted()
	{
		return $this->andWhere('[[deleted]]=0');
	}

	public function orderByDate()
	{
		return $this->orderBy(['date_create' => SORT_ASC]);
	}

	public function orderByDateDesc()
	{
		return $this->orderBy(['date_create' => SORT_DESC]);
	}
}
