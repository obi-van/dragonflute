<?php

namespace app\models\queries;
use app\helpers\KUseful;

/**
 * This is the ActiveQuery class for [[\app\models\ForumTopics]].
 *
 * @see \app\models\ForumTopics
 */
class ForumTopicsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\ForumTopics[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\ForumTopics|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	public function visibled()
	{
		return $this->andWhere('[[visible]]=1');
	}

	public function order()
	{
		return $this->orderBy(['order' => SORT_ASC]);
	}

	public function orderByDateAndType()
	{
		return $this->orderBy(['type' => SORT_ASC, 'date_create' => SORT_DESC]);
	}

	public function orderByDate()
	{
		return $this->orderBy(['date_create' => SORT_DESC]);
	}

	public function notDeleted()
	{
		return $this->andWhere('[[deleted]]=0');
	}
}
