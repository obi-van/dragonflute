<?php

namespace app\models;

use bupy7\bbcode\BBCodeBehavior;
use Yii;

/**
 * This is the model class for table "df_forum_answers".
 *
 * @property string $id
 * @property string $date_create
 * @property string $message
 * @property string $user_id
 * @property integer $topic_id
 * @property integer $deleted
 *
 * @property ForumTopics $topic
 * @property Users $user
 */
class ForumAnswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%forum_answers}}';
    }

	public function behaviors()
	{
		return [
			[
				'class' => BBCodeBehavior::className(),
				'attribute' => 'message',
				'saveAttribute' => 'message',
				'codeDefinitionBuilder' => [
					function($builder) {
						$builder->setTagName('video');
						$builder->setReplacementText('<iframe allowfullscreen="" height="360" src="https://www.youtube.com/embed/{param}" width="540"></iframe>');
						return $builder->build();
					},
				]
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'required'],
            [['date_create'], 'safe'],
            [['message'], 'string'],
            [['user_id', 'topic_id', 'deleted'], 'integer'],
//            [['topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => ForumTopics::className(), 'targetAttribute' => ['topic_id' => 'id']],
//            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата создания',
            'message' => 'Сообщение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(ForumTopics::className(), ['id' => 'topic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ForumAnswersQuery the active query used by this AR class.
     */
    public static function find()
    {
	    return new \app\models\queries\ForumAnswersQuery(get_called_class());
    }

	public function getQuantityMessages()
	{
		$user = $this->user;

		$topics = ForumTopics::find()->where(['user_id' => $user->id])->notDeleted()->all();
		$answers = ForumAnswers::find()->where(['user_id' => $user->id])->notDeleted()->all();

		$quantityMessage = count($topics) + count($answers);

		return $quantityMessage;
	}
}
