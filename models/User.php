<?php

namespace app\models;

use app\helpers\Debug;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use \Yii;

class User extends ActiveRecord implements IdentityInterface
{
	const ROLE_ADMIN = 'admin';
	const ROLE_WRITER = 'writer';
	const ROLE_PLAYER = 'player';
	const ROLE_MODERATOR = 'moder';

	public $password;

	public static function tableName()
	{
		return '{{%users}}';
	}

	public function rules()
	{
		return [
			[['account', 'email', 'password'], 'required', 'except' => 'update'],
			[['account', 'email'], 'required'],
			[['date_create', 'date_update', 'account', 'password_hash', 'password_reset_token', 'activate', 'key', 'role', 'img'], 'safe'],
			[['account'], 'match', 'pattern' => '/^[a-zA-Z0-9_\!]+$/', 'message' => 'Недопустимые символы'],
			[['password'], 'match', 'pattern' => '/^[a-zA-Z0-9_\!]+$/', 'message' => 'Недопустимые символы'],
			[['account', 'password'], 'string', 'length' => [5, 12]],
			[['nickname'], 'string', 'length' => [3, 15]],
			[['nickname'], 'match', 'pattern' => '/^[a-zA-Zа-яА-ЯёЁ0-9_]+$/', 'message' => 'Недопустимые символы'],
			[['email'], 'email'],
			[['email'], 'unique', 'message' => 'Адрес электронной почты уже используется'],
			[['account'], 'unique', 'message' => 'Имя аккаунта уже используется'],
		];
	}

	public function attributeLabels()
	{
		return [
			'account' => 'Аккаунта',
			'email' => 'E-mail',
			'password' => 'Пароль',
			'nickname' => 'Ник'
		];
	}

	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findOne(['access_token' => $token]);
	}

	public function getId()
	{
		return $this->id;
	}

	public function getRole()
	{
		return $this->role;
	}

	public function getAuthKey()
	{
		return $this->auth_key;
	}

	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	public function validatePassword($password)
	{
//		echo Debug::vars($password, $this->password_hash); die;
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	/**
	 * Generates password hash from password and sets it to the model
	 *
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$password = Yii::$app->security->generatePasswordHash($password);
		return $password;
	}

	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
		return $password_reset_token;
	}

	public function getQuantityMessages()
	{
		$topics = ForumTopics::find()->where(['user_id' => $this->id])->notDeleted()->all();
		$answers = ForumAnswers::find()->where(['user_id' => $this->id])->notDeleted()->all();

		$quantityMessage = count($topics) + count($answers);

		return $quantityMessage;
	}
}
