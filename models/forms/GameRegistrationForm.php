<?
namespace app\models\forms;

use yii\base\Model;
use Yii;

class GameRegistrationForm extends Model
{
	public $account;
	public $email;
	public $password;
	public $repassword;

	public function rules()
	{
		return [
			[['account', 'email', 'password', 'repassword'], 'required'],
			[['account'], 'match', 'pattern' => '/^[a-zA-Z0-9_\!]+$/', 'message' => 'Недопустимые символы'],
			[['password', 'repassword'], 'match', 'pattern' => '/^[a-zA-Z0-9_\!]+$/', 'message' => 'Недопустимые символы'],
			[['account', 'password', 'repassword'], 'string', 'length' => [6, 12]],
			[['email'], 'email'],
			['repassword', 'compare', 'compareAttribute'=>'password']
		];
	}

	public function attributeLabels()
	{
		return [
			'account' => 'Аккаунта',
			'email' => 'E-mail',
			'password' => 'Пароль',
			'repassword' => 'Повтороите пароль',
		];
	}
}