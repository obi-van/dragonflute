<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;
use app\helpers\Debug;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class AmnesiaRestoreForm extends Model
{
	public $password;
	public $repassword;

	public function rules()
	{
		return [
			[['password', 'repassword'], 'required'],
			[['password', 'repassword'], 'match', 'pattern' => '/^[a-zA-Z0-9_]+$/', 'message' => 'Русские буквы не допустимы'],
			[['password', 'repassword'], 'string', 'length' => [6, 12]],
			['repassword', 'compare', 'compareAttribute'=>'password']

		];
	}

	public function attributeLabels()
	{
		return [
			'password' => 'Новый пароль',
			'repassword' => 'Повтороите пароль',
		];
	}
}