<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use app\models\User;
use app\helpers\Debug;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class AmnesiaForm extends Model
{
    public $email;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
	        ['email', 'email', 'message' => 'E-mail не является правильным'],
        ];
    }

	public function attributeLabels()
	{
		return [
			'email' => 'E-mail',
		];
	}
}
