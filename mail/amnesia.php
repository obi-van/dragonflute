<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta name="viewport" content="width=device-width"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Запрос на смену пароля для аккаунта для <?= $_SERVER['SERVER_NAME'] ?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>
<div class="main-container">
	<h2>Запрос на смену пароля для аккаунта <?= $_SERVER['SERVER_NAME'] ?></h2>
	<table class="table table-striped table-bordered table-hover orders-table">
		<tr>
			<td>
				<p>Для смены пароля на <?= $_SERVER['SERVER_NAME'] ?> необходимо перейти по ссылке:</p>
				<p>
					<a href="http://<?= $_SERVER['HTTP_HOST'] ?>/auth/restore/<?= $code ?>">СМЕНИТЬ ПАРОЛЬ</a>
				</p>
			</td>
		</tr>
	</table>
</div>
</body>
</html>