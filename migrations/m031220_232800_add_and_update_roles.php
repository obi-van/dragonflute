<?php

use yii\db\Migration;

class m031220_232800_add_and_update_roles extends Migration
{
    public function safeUp()
    {
        $this->execute("INSERT INTO `df_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
					('manageForum', 2, 'Право на управление форумом - видеть админ раздел, удалять посты, блокировать пользователей', NULL, NULL, 1604252446, 1604252446);");
        $this->execute("INSERT INTO `df_auth_item_child` (`parent`, `child`) VALUES
					('admin', 'manageForum'),
					('moder', 'manageForum');");
    }

    public function safeDown()
    {
    	$this->execute("DELETE FROM `df_auth_item` WHERE  `name`='manageForum';");
	    $this->execute("DELETE FROM `df_auth_item_child` WHERE  `parent`='admin';");
	    $this->execute("DELETE FROM `df_auth_item_child` WHERE  `parent`='moder';");
    }
}
