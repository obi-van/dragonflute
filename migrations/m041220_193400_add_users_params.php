<?php

use yii\db\Migration;

class m041220_193400_add_users_params extends Migration
{
    public function safeUp()
    {
	    $this->execute("ALTER TABLE {{%users}} ADD COLUMN `img` TEXT NULL DEFAULT NULL AFTER `role`;");

	    $this->execute("ALTER TABLE {{%users}} ADD COLUMN `forum_banned` SMALLINT(1) NOT NULL DEFAULT 0 AFTER `img`;");
    }

    public function safeDown()
    {
	    $this->execute("ALTER TABLE {{%users}} DROP COLUMN `forum_banned`;");

	    $this->execute("ALTER TABLE {{%users}} DROP COLUMN `img`;");
    }
}
