<?php

use yii\db\Migration;

class m091220_231100_add_textblocks_table extends Migration
{
    public function safeUp()
    {
	    $this->execute("CREATE TABLE {{%textbloks}} (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`key` VARCHAR(100) NOT NULL COMMENT 'Ключ' COLLATE 'utf8_general_ci',
			`title` VARCHAR(100) NOT NULL COMMENT 'Заголовок' COLLATE 'utf8_general_ci',
			`content` TEXT(65535) NOT NULL COMMENT 'Текст' COLLATE 'utf8_general_ci',
			PRIMARY KEY (`id`) USING BTREE
		)
		COMMENT='текстовые блоки'
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB
		AUTO_INCREMENT=0
		;");

	    $this->execute("INSERT INTO {{%textbloks}} (`id`, `key`, `title`, `content`) VALUES
			(1, 'df_forum_rules', 'Правила форума', '<p>Тут будут правила форума</p>\r\n'),
			(2, 'df_about', 'О сервере', '<h1>О сервере</h1>\r\n		<p>\r\n			Проект <strong>DragonFlute</strong> стартовал 10 апреля 2020 г. благодаря группе энтузиастов и ценителей\r\n			Lineage 2. Целью проекта является воссоздание атмосферы официальных серверов времён The Chaotic Chronicle\r\n			<strong>без внутриигрового доната и премиум-подписок</strong>. Для нас важно обеспечить глубокое погружение\r\n			в уникальную обстановку игры с самых первых уровней.\r\n		</p>\r\n		<p>\r\n			Наш мир Lineage 2 живет своей собственной жизнью, на которую не могут повлиять ни рубль, ни доллар,\r\n			ни любые другие бумажки.\r\n		</p>\r\n		<p>\r\n			Мы очень ценим обратную связь с игроками и примем любую помощь в развитии проекта. Свои идеи и предложения\r\n			по улучшению сайта и сервера вы можете отправлять на почту <a href=\"mailto:l2dragonflute@gmail.com\">l2dragonflute@gmail.com</a>.\r\n		</p>\r\n\r\n		<h2>Техническая сторона</h2>\r\n		<h3>Uptime</h3>\r\n		<p>\r\n			Сервер поддерживает постоянную стабильную доступность на протяжении всего времени. Возможны кратковременные\r\n			отключения (не более чем на 15 минут) без предварительного оповещения игроков (статистически - не чаще 1-2\r\n			раз в месяц). О проведении длительных технических работ игроки предупреждаются заранее, не менее чем за сутки\r\n			до их начала.\r\n		</p>\r\n\r\n		<h2>Версия игры</h2>\r\n		<p>\r\n			<strong>Dragon Flute</strong> реализует хроники High Five PTS, максимально приближенные к оригинальной версии\r\n			европейского официального сервера, с небольшими изменениями.\r\n		</p>\r\n\r\n		<h2>Правила</h2>\r\n		<p>\r\n			<strong>DragonFlute</strong> позиционирует себя, как лайф-сервер с живыми игроками, но принимает допустимой игру с использованием\r\n			трех окон. Администрация постоянно отслеживает (живым присутствием и с привлечением\r\n			аналитических средств) стабильность игровой экономики и общее состояние баланса, оставляя за собой право\r\n			предупреждать и ограничивать игроков за явное игнорирование ценностей сервера.\r\n		</p>\r\n		<h3>Боты и окна</h3>\r\n		<p>\r\n			Разрешена работа <strong>не более 3 окон</strong> клиента одновременно для каждого игрока.\r\n		</p>\r\n		<div class=\"about-1__special-alert-info\">\r\n			Строго запрещено использование ботов, а также любых других программных средств автоматизации, эмулирующих присутствие человека в\r\n			игре.\r\n		</div>\r\n\r\n		<h2>Экономика и баланс</h2>\r\n		<p>\r\n			Сервер поддерживает полностью закрытую экономику без возможности наращивания внутриигрового капитала за\r\n			счет внешних вливаний денежных средств, идентичную корейскому официальному серверу времён Chaotic Chronicle.\r\n		</p>\r\n		<p>\r\n			В игре нет и категорически не будет предметов, модифицирующих получение опыта или дропа (а также изменяющих\r\n			любые другие параметры, влияющие на игровой баланс), покупаемых за деньги.\r\n		</p>\r\n\r\n		<h2>Связь и общение</h2>\r\n		<p>\r\n			Темы, посвященные торговле, организации прокачки и неформальному общению, в настоящее время можно вести в группе\r\n			<a href=\"https://vk.com/club133040637\" target=\"_blank\">Вконтакте</a>\r\n		</p>\r\n\r\n		<div class=\"about-1__special-info\">\r\n			Администрация оставляет за собой право редактировать действующие правила, уведомляя игроков о вносимых\r\n			изменениях.\r\n		</div>');");
    }

    public function safeDown()
    {
	    $this->dropTable("{{%textbloks}}");
    }
}
