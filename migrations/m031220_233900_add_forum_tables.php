<?php

use yii\db\Migration;

class m031220_233900_add_forum_tables extends Migration
{
    public function safeUp()
    {
	    $this->execute("CREATE TABLE IF NOT EXISTS {{%forum_sections}} (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `order` int(11) NOT NULL DEFAULT '0',
				  `visible` smallint(1) NOT NULL DEFAULT '1',
				  `for_admins` smallint(1) NOT NULL DEFAULT '0',
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Секции форума';");

	    $this->execute("CREATE TABLE IF NOT EXISTS {{%forum_categories}} (
				  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `section_id` int(11) DEFAULT NULL,
				  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `for_players` smallint(1) NOT NULL DEFAULT '0' COMMENT 'Разрешить добавлять топики пользователям',
				  `visible` smallint(1) NOT NULL DEFAULT '1',
				  `order` int(11) NOT NULL DEFAULT '0',
				  PRIMARY KEY (`id`),
				  KEY `FK_df_forum_categories_df_forum_sections` (`section_id`),
				  CONSTRAINT `FK_df_forum_categories_df_forum_sections` FOREIGN KEY (`section_id`) REFERENCES `df_forum_sections` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
				) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Категории форума (находятся в разделах)';");

	    $this->execute("CREATE TABLE IF NOT EXISTS {{%forum_topics}} (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
				  `message` longtext COLLATE utf8mb4_unicode_ci,
				  `date_create` datetime DEFAULT NULL,
				  `type` enum('important','info','closed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'info',
				  `forum_category_id` int(11) unsigned NOT NULL,
				  `user_id` int(11) unsigned NOT NULL,
				  `deleted` smallint(1) unsigned NOT NULL DEFAULT '0',
				  PRIMARY KEY (`id`),
				  KEY `FK_df_forum_topics_df_forum_categories` (`forum_category_id`),
				  KEY `FK_df_forum_topics_df_users` (`user_id`),
				  CONSTRAINT `FK_df_forum_topics_df_forum_categories` FOREIGN KEY (`forum_category_id`) REFERENCES `df_forum_categories` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
				  CONSTRAINT `FK_df_forum_topics_df_users` FOREIGN KEY (`user_id`) REFERENCES `df_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
				) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Темы форума';");

        $this->execute("CREATE TABLE IF NOT EXISTS {{%forum_answers}} (
					  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
					  `date_create` datetime NOT NULL,
					  `message` longtext COLLATE utf8mb4_unicode_ci,
					  `user_id` int(11) unsigned NOT NULL,
					  `topic_id` int(11) NOT NULL,
					  `deleted` smallint(1) unsigned NOT NULL DEFAULT '0',
					  PRIMARY KEY (`id`),
					  KEY `FK_df_forum_answers_df_users` (`user_id`),
					  KEY `FK_df_forum_answers_df_forum_topics` (`topic_id`),
					  CONSTRAINT `FK_df_forum_answers_df_forum_topics` FOREIGN KEY (`topic_id`) REFERENCES `df_forum_topics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
					  CONSTRAINT `FK_df_forum_answers_df_users` FOREIGN KEY (`user_id`) REFERENCES `df_users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
					) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Ответы к топикам';");

	    $this->execute("INSERT INTO {{%forum_sections}} (`id`, `alias`, `title`, `order`, `visible`, `for_admins`) VALUES
				(4, 'df4-osnovnoy-razdel', 'Основной раздел', 1, 1, 0),
				(5, 'df5-administrativnyy-razdel', 'Административный раздел', 2, 1, 1),
				(6, 'df6-arhiv', 'Архив', 3, 1, 0);");

        $this->execute("INSERT INTO {{%forum_categories}} (`id`, `section_id`, `alias`, `title`, `description`, `for_players`, `visible`, `order`) VALUES
				(1, 4, 'df1-novosti-i-obyavleniya', 'Новости и объявления', 'Новости от администрации проекта и разработчиков игры.', 0, 1, 1),
				(2, 4, 'df2-obsuzhdenie-igry', 'Обсуждение игры', 'Общение на тему игровых вопросов', 1, 1, 3),
				(3, 4, 'df3-tehnicheskie-voprosy', 'Технические вопросы', 'Обсуждение технических вопросов, связанных с  игрой', 0, 1, 2),
				(4, 4, 'df4-tvorchestvo', 'Творчество', '', 0, 1, 4),
				(5, 4, 'df5-rekruting', 'Рекрутинг', 'Раздел для сообщений о наборе в клан или постоянную группу', 0, 1, 5),
				(6, 5, 'df6-taynoe-massonskoe-lozhe', 'Тайное массонское ложе', 'Раздел для администраторов и модераторв', 0, 1, 0),
				(7, 6, 'df7-starye-kumeki', 'Старые кумеки', 'Тестовый раздел', 0, 1, 0);");

    }

    public function safeDown()
    {
	    $this->dropTable("{{%forum_answers}}");
	    $this->dropTable("{{%forum_topics}}");
	    $this->dropTable("{{%forum_categories}}");
	    $this->dropTable("{{%forum_sections}}");
    }
}
