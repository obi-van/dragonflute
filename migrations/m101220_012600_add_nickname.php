<?php

use yii\db\Migration;

class m101220_012600_add_nickname extends Migration
{
    public function safeUp()
    {
	    $this->execute("ALTER TABLE {{%users}} ADD COLUMN `nickname` VARCHAR(50) NULL DEFAULT NULL AFTER `account`;");
    }

    public function safeDown()
    {
	    $this->execute("ALTER TABLE {{%users}} DROP COLUMN `nickname`;");
    }
}
