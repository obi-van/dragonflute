<?php

use yii\db\Migration;

class m181220_211300_add_textblock_for_files extends Migration
{
    public function safeUp()
    {
	    $this->execute("INSERT INTO {{%textbloks}} (`id`, `key`, `title`, `content`) VALUES ('3', 'df_files_patch', 'Патч', '1');");
    }

    public function safeDown()
    {
	    $this->execute("DELETE FROM {{%textbloks}} WHERE  `id`=3;");
    }
}
